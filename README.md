# YARM SoC designs

## Requirements

- GNU **make**
- **gcc** >= 9.1.0
	* target `riscv*-unknown-elf`
	* arch `rv32i`
	* abi `ilp32`
- **ghdl** >= `aaa25b2`
- [**LiteX**](https://github.com/enjoy-digital/litex)
- [**LiteEth**](https://github.com/enjoy-digital/liteeth)

### Simulation

- **gtkwave** - to view simulation wave form

### Synthesis

- [**ghdl-yosys-plugin**](https://github.com/ghdl/ghdl-yosys-plugin)
- [**yosys**](https://github.com/YosysHQ/yosys)
- [**nextpnr-xilinx**](https://github.com/daveshah1/nextpnr-xilinx)
- [**Project Xray**](https://github.com/SymbiFlow/prjxray)
- [**openFPGALoader**](https://github.com/trabucayre/openFPGALoader)

## Project directory structure

- `external/`: External dependencies as submodules
- `firmware/`: See `firmware/README.md`
- `sim/`: GHDL wave options and GTKWave save files for test bench traces
- `tests/`: High-level tests
  - `formal/`: SymbiYosys formal verification tests
  - `riscv-compliance-target/`: YARM target for RISC-V compliance test suite
- `vhdl/`: All VHDL code
  - `components/`: Various SoC components
  - `core/`: The YARM processor core
  - `memories/`: Various memory primitives, tweaked to work with yosys
  - `simulation/`: Simulation models for primitives and external IP
  - `tests/`: Test benches

## Building

Before simulating or synthesizing, the VHDL sources have to be imported:

```
make ghdl_import
```

### Simulation

To run the simulation of the SoC and show the resulting waveforms:

```
make wave
```

The target entity can be changed with `SIM_ENTITY=` in `Makefile`

### Synthesis

The project targets the Digilent Arty A7-35T board. Synthesis requires several software components whose paths have to be specified in the Makefile:

```make
NEXTPNR_DIR = /path/to/nextpnr-xilinx
XRAY_DIR = /path/to/prjxray
GHDL_YOSYS_PLUGIN = /path/to/ghdl.so
```

To synthesize the design, create the bitstream, and flash it to the device:

```sh
make flash
```

## Firmware

Split into two parts:

- loader:
	gets initialized into FPGA block RAM, takes data from UART and boots it
- payload:
	sent via UART

```
make firmware
```

- creates binaries
- stitches loader and payload together into a single image for simulation
- hex-dumps loader bytecode to `firmware/bin/loader.hex` for block RAM initialization in synthesis
- creates payload file `firmware/payload_uart_data.bin` to be sent directly via UART

### Loader protocol

entry point is `void payload_entry()` at `0x1000`

example code: `00008067` - single `ret` instruction

1. leader sequence: `BOOT\n`
2. number of data bytes (big endian 16-bit int) - e.g. `0004`
3. data as it will appear in memory (little-endian 32-bit words) - e.g. `67800000`

complete blob as created by `build_uart_data.py`:

```
$ xxd payload_uart_data.bin
00000000: 424f 4f54 0a00 0467 8000 00              BOOT...g...
```
