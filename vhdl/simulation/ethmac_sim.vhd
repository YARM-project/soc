library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity ethmac_sim is
	port (
		sys_clock         : in std_logic;
		sys_reset         : in std_logic;
		mii_eth_clocks_tx : in std_logic;
		mii_eth_clocks_rx : in std_logic;
		mii_eth_rst_n     : out std_logic;
		mii_eth_mdio      : inout std_logic;
		mii_eth_mdc       : out std_logic;
		mii_eth_rx_dv     : in std_logic;
		mii_eth_rx_er     : in std_logic;
		mii_eth_rx_data   : in std_logic_vector(3 downto 0);
		mii_eth_tx_en     : out std_logic;
		mii_eth_tx_data   : out std_logic_vector(3 downto 0);
		mii_eth_col       : in std_logic;
		mii_eth_crs       : in std_logic;

		wishbone_adr   : in std_logic_vector(29 downto 0);
		wishbone_dat_r : out std_logic_vector(31 downto 0);
		wishbone_dat_w : in std_logic_vector(31 downto 0);
		wishbone_sel   : in std_logic_vector(3 downto 0);
		wishbone_cyc   : in std_logic;
		wishbone_stb   : in std_logic;
		wishbone_ack   : out std_logic;
		wishbone_we    : in std_logic;
		wishbone_cti   : in std_logic_vector(2 downto 0);
		wishbone_bte   : in std_logic_vector(1 downto 0);
		wishbone_err   : out std_logic;

		interrupt : out std_logic
	);
end ethmac_sim;

architecture rtl of ethmac_sim is
	signal adr_full : std_logic_vector(31 downto 0);
begin
	mii_eth_rst_n   <= '0';
	mii_eth_tx_en   <= '0';
	mii_eth_tx_data <= (others => '0');

	adr_full <= wishbone_adr & "00";

	process(sys_clock)
	begin
		if rising_edge(sys_clock) then
			wishbone_dat_r <= (others => '0');
			wishbone_ack <= '0';

			wishbone_err <= '0';

			if wishbone_cyc and wishbone_stb then
				case adr_full is
					when x"f018_0800" =>
						-- ethphy_crg_reset
						wishbone_ack <= '1';

					when x"f018_0804" =>
						-- ethphy_mdio_w
						if wishbone_we and wishbone_sel(0) then
							mii_eth_mdc <= wishbone_dat_w(0);
							if wishbone_dat_w(1) then
								mii_eth_mdio <= wishbone_dat_w(2);
							else
								mii_eth_mdio <= 'Z';
							end if;
						end if;
						wishbone_ack <= '1';

					when x"f018_0808" =>
						-- ethphy_mdio_r
						wishbone_dat_r <= (0 => mii_eth_mdio, others => '0');
						wishbone_ack <= '1';

					when x"f018_1000" =>
						-- ethmac_sram_writer_slot
						wishbone_ack <= '1';

					when x"f018_1004" =>
						-- ethmac_sram_writer_length
						wishbone_ack <= '1';

					when x"f018_1008" =>
						-- ethmac_sram_writer_errors
						wishbone_ack <= '1';

					when x"f018_100c" =>
						-- ethmac_sram_writer_ev_status
						wishbone_ack <= '1';

					when x"f018_1010" =>
						-- ethmac_sram_writer_ev_pending
						wishbone_ack <= '1';

					when x"f018_1014" =>
						-- ethmac_sram_writer_ev_enable
						wishbone_ack <= '1';

					when x"f018_1018" =>
						-- ethmac_sram_reader_start
						wishbone_ack <= '1';

					when x"f018_101c" =>
						-- ethmac_sram_reader_ready
						wishbone_ack <= '1';

					when x"f018_1020" =>
						-- ethmac_sram_reader_level
						wishbone_ack <= '1';

					when x"f018_1024" =>
						-- ethmac_sram_reader_slot
						wishbone_ack <= '1';

					when x"f018_1028" =>
						-- ethmac_sram_reader_length
						wishbone_ack <= '1';

					when x"f018_102c" =>
						-- ethmac_sram_reader_ev_status
						wishbone_ack <= '1';

					when x"f018_1030" =>
						-- ethmac_sram_reader_ev_pending
						wishbone_ack <= '1';

					when x"f018_1034" =>
						-- ethmac_sram_reader_ev_enable
						wishbone_ack <= '1';

					when x"f018_1038" =>
						-- ethmac_preamble_crc
						wishbone_ack <= '1';

					when x"f018_103c" =>
						-- ethmac_preamble_errors
						wishbone_ack <= '1';

					when x"f018_1040" =>
						-- ethmac_crc_errors
						wishbone_ack <= '1';

					when others =>
						if adr_full ?= x"f010" & "000-" & x"---" then
							wishbone_ack <= '1';
						else
							wishbone_err <= '1';
						end if;
				end case;
			end if;
		end if;
	end process;

	interrupt <= '0';
end rtl;
