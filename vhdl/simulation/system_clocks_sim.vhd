library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity system_clocks_sim is
	generic (
		CLK_IN_FREQ : natural
	);
	port (
		clk_in : in std_logic;

		clk_core   : out std_logic;
		clk_uart   : out std_logic;
		clk_ddr3   : out std_logic;
		clk_pixel  : out std_logic;
		clk_tmds   : out std_logic;
		clk_ethphy : out std_logic
	);
end system_clocks_sim;

architecture rtl of system_clocks_sim is
	signal stop : std_logic;
begin
	assert CLK_IN_FREQ = 100_000_000
		report "Simulation clock generator is currently only designed for 100MHz board clock"
		severity error;

	-- 18.432 MHz UART clock (actually 18.518..., which is within 0.4%)
	clkgen_uart: entity work.clock_gen
		generic map (
			T_pulse => 27 ns,
			T_period => 54 ns
		)
		port map (
			clk => clk_uart,
			stop => stop
		);

	-- 200MHz DDR3 clock
	clkgen_ddr3: entity work.clock_gen
		generic map (
			T_pulse => 2.5 ns,
			T_period => 5 ns
		)
		port map (
			clk => clk_ddr3,
			stop => stop
		);

	-- 25MHz pixel clock
	clkgen_pixel: entity work.clock_gen
		generic map (
			T_pulse => 20 ns,
			T_period => 40 ns
		)
		port map (
			clk => clk_pixel,
			stop => stop
		);

	-- 250MHz TMDS clock
	clkgen_tmds: entity work.clock_gen
		generic map (
			T_pulse => 2 ns,
			T_period => 4 ns
		)
		port map (
			clk => clk_tmds,
			stop => stop
		);

	-- 25MHz ethernet PHY clock
	clkgen_ethphy: entity work.clock_gen
		generic map (
			T_pulse => 20 ns,
			T_period => 40 ns
		)
		port map (
			clk => clk_ethphy,
			stop => stop
		);

	-- 50MHz core/bus clock
	clkgen_core: entity work.clock_gen
		generic map (
			T_pulse => 10 ns,
			T_period => 20 ns
		)
		port map (
			clk => clk_core,
			stop => stop
		);

	process
	begin
		stop <= '0';
		wait until clk_in'quiet(20 ns);

		stop <= '1';
		wait;
	end process;
end rtl;
