library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity OBUFDS_sim is
	port (
		I  : in std_logic;
		O  : out std_logic;
		OB : out std_logic
	);
end OBUFDS_sim;

architecture rtl of OBUFDS_sim is
begin
	O  <= I;
	OB <= not I;
end rtl;
