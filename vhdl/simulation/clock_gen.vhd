library ieee;
use ieee.std_logic_1164.all;

entity clock_gen is
	generic(
		T_pulse  : time;
		T_period : time;
		T_reset  : time := 2.5 * T_period
	);
	port(
		stop  : in std_logic;
		clk   : out std_logic;
		reset : out std_logic
	);
end clock_gen;

architecture behaviour of clock_gen is
begin
	clock_driver: process
		begin
			if stop = '1' then
				wait;
			else
				clk <= '1', '0' after T_pulse;
				wait for T_period;
			end if;
		end process;

	reset_driver: process
		begin
			reset <= '1', 'Z' after T_reset;
			wait;
		end process;
end behaviour;
