library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.util_pkg.all;

entity io is
	generic (
		IS_SIMULATION : std_logic;
		WS2812_NUM_LEDS : natural range 0 to 1024
	);
	port (
		clk_bus   : in std_logic;
		clk_uart  : in std_logic;
		-- 25.175MHz for 640x480@60
		clk_pixel : in std_logic;
		-- 10x pixel clock
		clk_tmds  : in std_logic;
		clk_100mhz : in std_logic;
		reset     : in std_logic;

		IO_addr        : in yarm_word;
		IO_read        : in std_logic;
		IO_write       : in std_logic;
		IO_done        : out std_logic;
		IO_byte_enable : in std_logic_vector(3 downto 0);
		IO_data_read   : out yarm_word;
		IO_data_write  : in yarm_word;

		TRAM_vga_addr : out std_logic_vector(11 downto 0);
		TRAM_vga_data : in std_logic_vector(15 downto 0);

		buttons     : in std_logic_vector(3 downto 0);
		switches    : in std_logic_vector(3 downto 0);
		leds_simple : out std_logic_vector(3 downto 0);
		rgb_led0    : out std_logic_vector(2 downto 0);
		rgb_led1    : out std_logic_vector(2 downto 0);
		rgb_led2    : out std_logic_vector(2 downto 0);
		rgb_led3    : out std_logic_vector(2 downto 0);

		vga_r     : out std_logic_vector(7 downto 0);
		vga_g     : out std_logic_vector(7 downto 0);
		vga_b     : out std_logic_vector(7 downto 0);
		vga_hsync : out std_logic;
		vga_vsync : out std_logic;

		dvi_red   : out std_logic;
		dvi_green : out std_logic;
		dvi_blue  : out std_logic;
		dvi_clk   : out std_logic;

		uart_rx : in std_logic;
		uart_tx : out std_logic;
		uart_interrupt : out std_logic;

		ws2812_data : out std_logic;

		mii_n_reset : out std_logic;
		mii_mdio    : inout std_logic;
		mii_mdc     : out std_logic;
		mii_rx_clk  : in std_logic;
		mii_rx_er   : in std_logic;
		mii_rx_dv   : in std_logic;
		mii_rx_data : in std_logic_vector(3 downto 0);
		mii_tx_clk  : in std_logic;
		mii_tx_en   : out std_logic;
		mii_tx_data : out std_logic_vector(3 downto 0);
		mii_col     : in std_logic;
		mii_crs     : in std_logic;

		external_addr       : out std_logic_vector(7 downto 0);
		external_data_read  : in std_logic_vector(7 downto 0);
		external_data_write : out std_logic_vector(7 downto 0);
		external_read       : out std_logic;
		external_write      : out std_logic;
		external_cs_uart    : out std_logic;
		external_cs_dac     : out std_logic;

		mtime    : in unsigned(63 downto 0);
		mtimecmp : out unsigned(63 downto 0) := (others => '0')
	);
end io;

architecture rtl of io is
	constant CHAR_WIDTH  : integer := 8;
	constant CHAR_HEIGHT : integer := 16;

	constant H_FRONT : integer := 16;
	constant H_SYNC  : integer := 96;
	constant H_BACK  : integer := 48;
	constant H_DATA  : integer := 640;

	constant V_FRONT : integer := 10;
	constant V_SYNC  : integer := 2;
	constant V_BACK  : integer := 33;
	constant V_DATA  : integer := 480;

	signal inprogress_cycles : integer := 0;

	signal external_tick   : std_logic;
	signal external_cycles : integer := 0;

	-- RGB LEDs
	type rgb_leds_t is array (3 downto 0) of std_logic_vector(2 downto 0);
	signal rgb_leds : rgb_leds_t;

	-- UART
	signal uart_read        : std_logic;
	signal uart_write       : std_logic;
	signal uart_data_read   : std_logic_vector(7 downto 0);
	signal uart_data_write  : std_logic_vector(7 downto 0);

	-- WS2812 driver
	type ws2812_mem_t is
		array(natural range 0 to WS2812_NUM_LEDS-1) of std_logic_vector(23 downto 0);

	constant WS2812_ADDR_WIDTH : natural := addr_bits_req(WS2812_NUM_LEDS);

	signal ws2812_addr : std_logic_vector(6 downto 0);
	signal ws2812_word : std_logic_vector(23 downto 0);
	signal ws2812_mem  : ws2812_mem_t := (others => (others => '0'));

	component liteeth_core is
		port (
			sys_clock         : in std_logic;
			sys_reset         : in std_logic;
			mii_eth_clocks_tx : in std_logic;
			mii_eth_clocks_rx : in std_logic;
			mii_eth_rst_n     : out std_logic;
			mii_eth_mdio      : inout std_logic;
			mii_eth_mdc       : out std_logic;
			mii_eth_rx_dv     : in std_logic;
			mii_eth_rx_er     : in std_logic;
			mii_eth_rx_data   : in std_logic_vector(3 downto 0);
			mii_eth_tx_en     : out std_logic;
			mii_eth_tx_data   : out std_logic_vector(3 downto 0);
			mii_eth_col       : in std_logic;
			mii_eth_crs       : in std_logic;

			wishbone_adr   : in std_logic_vector(29 downto 0);
			wishbone_dat_r : out std_logic_vector(31 downto 0);
			wishbone_dat_w : in std_logic_vector(31 downto 0);
			wishbone_sel   : in std_logic_vector(3 downto 0);
			wishbone_cyc   : in std_logic;
			wishbone_stb   : in std_logic;
			wishbone_ack   : out std_logic;
			wishbone_we    : in std_logic;
			wishbone_cti   : in std_logic_vector(2 downto 0);
			wishbone_bte   : in std_logic_vector(1 downto 0);
			wishbone_err   : out std_logic;

			interrupt : out std_logic
		);
	end component;

	-- wishbone bus is big-endian
	signal wishbone_dat_w : std_logic_vector(31 downto 0);
	signal wishbone_sel : std_logic_vector(3 downto 0);

	signal ethernet_dat_r : yarm_word;
	signal ethernet_stb : std_logic;
	signal ethernet_ack : std_logic;
	signal ethernet_interrupt : std_logic;
begin
	gfx_module: entity work.gfx_module
		generic map (
			H_FRONT => H_FRONT,
			H_SYNC  => H_SYNC,
			H_BACK  => H_BACK,
			H_DATA  => H_DATA,
			V_FRONT => V_FRONT,
			V_SYNC  => V_SYNC,
			V_BACK  => V_BACK,
			V_DATA  => V_DATA,

			CHAR_WIDTH  => CHAR_WIDTH,
			CHAR_HEIGHT => CHAR_HEIGHT
		)
		port map (
			clk_pixel => clk_pixel,
			clk_tmds  => clk_tmds,
			reset     => reset,

			tram_addr => TRAM_vga_addr,
			tram_data => TRAM_vga_data,

			hsync => vga_hsync,
			vsync => vga_vsync,
			blank => open,

			analog_red   => vga_r,
			analog_green => vga_g,
			analog_blue  => vga_b,

			dvi_red   => dvi_red,
			dvi_green => dvi_green,
			dvi_blue  => dvi_blue,
			dvi_clk   => dvi_clk
		);


	uart: entity work.uart_16550
		port map (
			bus_clock  => clk_bus,
			uart_clock => clk_uart,
			reset      => reset,

			address    => IO_addr(4 downto 2),
			read       => uart_read,
			write      => uart_write,
			data_read  => uart_data_read,
			data_write => uart_data_write,

			interrupt => uart_interrupt,

			tx => uart_tx,
			rx => uart_rx,

			n_rts => open,
			n_dtr => open,
			n_dsr => '1',
			n_dcd => '1',
			n_cts => '1',
			n_ri  => '1'
		);

	ws2812: entity work.ws2812
		generic map (
			NUM_LEDS => WS2812_NUM_LEDS,
			COLOR_ORDER => "GRB",
			T_CLK => 10 ns,

			T0H => 0.35 us,
			T1H => 0.7 us,
			T0L => 0.8 us,
			T1L => 0.6 us,

			T_RES => 80 us
		)
		port map (
			n_reset => not reset,
			clk     => clk_100mhz,

			led_addr  => ws2812_addr,
			led_red   => ws2812_word(23 downto 16),
			led_green => ws2812_word(15 downto 8),
			led_blue  => ws2812_word(7 downto 0),

			dout => ws2812_data
		);

	ethernet: liteeth_core
		port map (
			sys_clock         => clk_bus,
			sys_reset         => reset,
			mii_eth_clocks_tx => mii_tx_clk,
			mii_eth_clocks_rx => mii_rx_clk,
			mii_eth_rst_n     => mii_n_reset,
			mii_eth_mdio      => mii_mdio,
			mii_eth_mdc       => mii_mdc,
			mii_eth_rx_dv     => mii_rx_dv,
			mii_eth_rx_er     => mii_rx_er,
			mii_eth_rx_data   => mii_rx_data,
			mii_eth_tx_en     => mii_tx_en,
			mii_eth_tx_data   => mii_tx_data,
			mii_eth_col       => mii_col,
			mii_eth_crs       => mii_crs,

			wishbone_adr   => IO_addr(31 downto 2),
			wishbone_dat_r => ethernet_dat_r,
			wishbone_dat_w => wishbone_dat_w,
			wishbone_sel   => wishbone_sel,
			wishbone_cyc   => IO_read or IO_write,
			wishbone_stb   => ethernet_stb,
			wishbone_ack   => ethernet_ack,
			wishbone_we    => IO_write,
			wishbone_cti   => (others => '0'),
			wishbone_bte   => (others => '0'),
			wishbone_err   => open,

			interrupt => ethernet_interrupt
		);

	external_clkdiv: entity work.clock_divider
		generic map (
			DIVIDE_FACTOR_WIDTH => 6
		)
		port map (
			clk_in  => clk_bus,
			reset   => reset,
			divisor => (others => '0'),
			tick    => external_tick
		);

	run_proc: process(clk_bus)
		variable ws2812_mem_addr : integer range 0 to WS2812_NUM_LEDS-1;
	begin
		if rising_edge(clk_bus) then
			if reset or IO_done then
				-- automatically go from done back to idle after one cycle
				IO_done <= '0';
				inprogress_cycles <= 0;
				external_cycles   <= 0;
			elsif IO_read or IO_write then
				inprogress_cycles <= inprogress_cycles + 1;
				if external_tick then
					external_cycles <= external_cycles + 1;
				end if;

				if IO_addr ?= x"f000_0000" and IO_write then
					-- LEDs
					IO_done <= '1';
					leds_simple <= endian_switch(IO_data_write)(3 downto 0);
				end if;

				if IO_addr ?= x"f000_0004" and IO_read then
					-- switches
					IO_done <= '1';
					IO_data_read <= endian_switch(x"0000_000" & switches);
				end if;

				if IO_addr ?= x"f000_0008" and IO_read then
					-- buttons
					IO_done <= '1';
					IO_data_read <= endian_switch(x"0000_000" & buttons);
				end if;

				if IO_addr ?= x"f000_001-" and IO_write then
					-- RGB LEDs (first: 0xf000_0010, second: 0xf000_0014)
					IO_done <= '1';
					--rgb_leds(to_integer(unsigned(IO_addr(3 downto 2)))) <=
					--	endian_switch(IO_data_write)(2 downto 0);
				end if;

				if IO_addr ?= x"f000_01" & "000-----" then
					-- UART base: 0xf000_0100
					-- reg 1: f000_0104
					-- reg 7: f000_011c

					if inprogress_cycles = 0 then
						-- on first memory cycle, set read/write bits
						uart_write <= IO_write;
						uart_read  <= IO_read;
					else
						-- by default, only pass output
						uart_write <= '0';
						uart_read  <= '0';

						-- if we're either only writing or we're on the third
						-- cycle, mark as done
						if IO_read = '0' or inprogress_cycles >= 2 then
							IO_done <= '1';
						end if;
					end if;

					uart_data_write <= endian_switch(IO_data_write)(7 downto 0);

					if IO_read then
						IO_data_read <= endian_switch(x"0000_00" & uart_data_read);
					end if;
				end if;

				if IO_addr ?= x"f000_1---" then
					-- ws2812 base (LED 0): 0xf000_1000
					-- LED 1:    0xf000_1004
					-- LED 1023: 0xf000_1ffc
					IO_done <= '1';
					ws2812_mem_addr := to_integer(unsigned(IO_addr(2+WS2812_ADDR_WIDTH-1 downto 2)));

					if IO_write then
						ws2812_mem(ws2812_mem_addr) <= endian_switch(IO_data_write)(23 downto 0);
					end if;
					if IO_read then
						IO_data_read <= endian_switch(x"00" & ws2812_mem(ws2812_mem_addr));
					end if;
				end if;

				-- mtime[h]
				if IO_addr = x"8000_0000" then
					IO_done <= '1';
					if IO_read then
						IO_data_read <= endian_switch(std_logic_vector(mtime(31 downto 0)));
					end if;
				end if;
				if IO_addr = x"8000_0004" then
					IO_done <= '1';
					if IO_read then
						IO_data_read <= endian_switch(std_logic_vector(mtime(63 downto 32)));
					end if;
				end if;

				-- mtimecmp[h]
				if IO_addr = x"8000_0010" then
					IO_done <= '1';

					if IO_write then
						mtimecmp(31 downto 0) <= unsigned(endian_switch(IO_data_write));
					end if;
					if IO_read then
						IO_data_read <= endian_switch(std_logic_vector(mtimecmp(31 downto 0)));
					end if;
				end if;
				if IO_addr = x"8000_0014" then
					IO_done <= '1';

					if IO_write then
						mtimecmp(63 downto 32) <= unsigned(endian_switch(IO_data_write));
					end if;
					if IO_read then
						IO_data_read <= endian_switch(std_logic_vector(mtimecmp(63 downto 32)));
					end if;
				end if;

				-- simulation detection
				if IO_addr = x"8000_0020" then
					IO_done <= '1';

					if IO_read then
						IO_data_read <= endian_switch((0 => IS_SIMULATION, others => '0'));
					end if;
				end if;

				if ethernet_stb then
					if ethernet_ack then
						IO_data_read <= endian_switch(ethernet_dat_r);
						IO_done <= '1';
					end if;
				end if;

				external_cs_uart <= '0';
				external_cs_dac  <= '0';

				external_read  <= '0';
				external_write <= '0';

				if IO_addr ?= x"f000_20--" then
					-- 3 address bits (0xf000_2000, 0xf000_2004, ..., 0xf000_201c)
					external_cs_uart <= IO_addr ?= x"f000_20" & "000---" & "--";
					-- 1 address bit (0xf000_2100, 0xf000_2104)
					external_cs_dac  <= IO_addr ?= x"f000_21" & "00000-" & "--";

					-- wait until the clock divider ticks once
					if external_cycles = 1 then
						-- on first memory cycle, set read/write bits
						external_read  <= IO_read;
						external_write <= IO_write;

						external_addr <= IO_addr(9 downto 2);

						external_data_write <= endian_switch(IO_data_write)(7 downto 0);
					elsif external_cycles > 1 then
						-- if we're either only writing or we're on the third
						-- cycle, mark as done
						IO_done <= '1';

						IO_data_read <= endian_switch(x"0000_00" & external_data_read);
					end if;
				end if;
			end if;
		end if;
	end process;

	rgb_led0 <= rgb_leds(0);
	rgb_led1 <= rgb_leds(1);
	rgb_led2 <= rgb_leds(2);
	rgb_led3 <= rgb_leds(3);

	wishbone_dat_w <= endian_switch(IO_data_write);
	wishbone_sel <= IO_byte_enable(0) & IO_byte_enable(1) & IO_byte_enable(2) & IO_byte_enable(3);

	ethernet_stb <= (IO_read or IO_write) and IO_addr ?= x"f01-_----";
end rtl;
