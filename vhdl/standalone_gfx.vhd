library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity standalone_gfx is
	port (
		clk_100mhz : in std_logic;
		reset : in std_logic;

		dvi_clk_p   : out std_logic;
		dvi_clk_n   : out std_logic;
		dvi_red_p   : out std_logic;
		dvi_red_n   : out std_logic;
		dvi_green_p : out std_logic;
		dvi_green_n : out std_logic;
		dvi_blue_p  : out std_logic;
		dvi_blue_n  : out std_logic
	);
end standalone_gfx;

architecture behaviour of standalone_gfx is
	constant SCREEN_WIDTH  : integer := 640;
	constant SCREEN_HEIGHT : integer := 480;

	constant CHAR_WIDTH    : integer := 8;
	constant CHAR_HEIGHT   : integer := 16;

	constant H_FRONT : integer := 16;
	constant H_SYNC  : integer := 96;
	constant H_BACK  : integer := 48;

	constant V_FRONT : integer := 10;
	constant V_SYNC  : integer := 2;
	constant V_BACK  : integer := 33;

	signal clk_pixel, clk_tmds : std_logic;
	signal tram_addr : std_logic_vector(11 downto 0);
	signal tram_data : std_logic_vector(15 downto 0);

	signal dvi_red, dvi_green, dvi_blue, dvi_clk : std_logic;

	component system_clocks is
		port (
			clk_in : in std_logic;

			clk_core   : out std_logic;
			clk_uart   : out std_logic;
			clk_ddr3   : out std_logic;
			clk_pixel  : out std_logic;
			clk_tmds   : out std_logic;
			clk_ethphy : out std_logic
		);
	end component;

	component OBUFDS is
		port (
			I  : in std_logic;
			O  : out std_logic;
			OB : out std_logic
		);
	end component;
begin
	uut: entity work.gfx_module
		generic map (
			H_FRONT => H_FRONT,
			H_SYNC  => H_SYNC,
			H_BACK  => H_BACK,
			H_DATA  => SCREEN_WIDTH,

			V_FRONT => V_FRONT,
			V_SYNC  => V_SYNC,
			V_BACK  => V_BACK,
			V_DATA  => SCREEN_HEIGHT,

			CHAR_WIDTH  => CHAR_WIDTH,
			CHAR_HEIGHT => CHAR_HEIGHT
		)
		port map (
			clk_pixel => clk_pixel,
			clk_tmds  => clk_tmds,
			reset     => reset,

			tram_addr => tram_addr,
			tram_data => tram_data,

			hsync => open,
			vsync => open,
			blank => open,

			analog_red   => open,
			analog_green => open,
			analog_blue  => open,

			dvi_red   => dvi_red,
			dvi_green => dvi_green,
			dvi_blue  => dvi_blue,
			dvi_clk   => dvi_clk
		);

	system_clocks_inst: system_clocks
		port map (
			clk_in => clk_100mhz,

			clk_core   => open,
			clk_uart   => open,
			clk_ddr3   => open,
			clk_pixel  => clk_pixel,
			clk_tmds   => clk_tmds,
			clk_ethphy => open
		);

	text_gen: process(clk_pixel)
	begin
		if rising_edge(clk_pixel) then
			tram_data <= "0001" & "1111" & x"78";
		end if;
	end process;

	OBUFDS_dvi_red:   OBUFDS port map (I => dvi_red,   O => dvi_red_p,   OB => dvi_red_n);
	OBUFDS_dvi_green: OBUFDS port map (I => dvi_green, O => dvi_green_p, OB => dvi_green_n);
	OBUFDS_dvi_blue:  OBUFDS port map (I => dvi_blue,  O => dvi_blue_p,  OB => dvi_blue_n);
	OBUFDS_dvi_clk:   OBUFDS port map (I => dvi_clk,   O => dvi_clk_p,   OB => dvi_clk_n);
end behaviour;
