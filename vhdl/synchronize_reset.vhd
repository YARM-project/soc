library ieee;
use ieee.std_logic_1164.all;

-- synchronize a reset - any pulse on data_in results in at least LEVELS
-- cycles of active data_out

entity synchronize_reset is
	generic (
		LEVELS : integer range 2 to integer'high := 2
	);
	port (
		clk      : in std_logic;
		data_in  : in std_logic;
		data_out : out std_logic
	);
end synchronize_reset;

architecture rtl of synchronize_reset is
	attribute SHREG_EXTRACT : string;

	signal sync_chain : std_logic_vector(LEVELS downto 0);
	attribute SHREG_EXTRACT of sync_chain : signal is "NO";
begin
	process(clk)
	begin
		if data_in then
			sync_chain <= (others => '1');
		elsif rising_edge(clk) then
			sync_chain <= '0' & sync_chain(sync_chain'high downto 1);
		end if;
	end process;

	data_out <= sync_chain(0);
end rtl;
