library ieee;
use ieee.std_logic_1164.all;

entity synchronize_bit is
	generic (
		LEVELS : integer range 2 to integer'high := 2
	);
	port (
		clk      : in std_logic;
		data_in  : in std_logic;
		data_out : out std_logic
	);
end synchronize_bit;

architecture rtl of synchronize_bit is
	attribute SHREG_EXTRACT : string;

	signal data_meta  : std_logic;
	signal sync_chain : std_logic_vector(LEVELS downto 0);

	attribute SHREG_EXTRACT of data_meta  : signal is "NO";
	attribute SHREG_EXTRACT of sync_chain : signal is "NO";
begin
	sync: process(clk)
	begin
		if rising_edge(clk) then
			data_meta <= data_in;
			sync_chain <= data_meta & sync_chain(sync_chain'high downto 1);
		end if;
	end process;

	data_out <= sync_chain(0);
end rtl;
