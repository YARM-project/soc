library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants.all;

entity uart_16550_tb is
end uart_16550_tb;

architecture behaviour of uart_16550_tb is
	signal bus_clk, uart_clk : std_logic;
	signal reset, stop       : std_logic;

	signal reg_address : std_logic_vector(2 downto 0);
	signal read, write : std_logic;
	signal data_read, data_write : std_logic_vector(7 downto 0);
	signal interrupt   : std_logic;
	signal rx, tx      : std_logic;
	signal rts, dtr    : std_logic;
	signal dsr, dcd, cts, ri : std_logic;
begin
	uut: entity work.uart_16550
		port map (
			bus_clock => bus_clk,
			uart_clock => uart_clk,
			reset => reset,
			address => reg_address,
			read => read,
			write => write,

			data_read => data_read,
			data_write => data_write,

			interrupt => interrupt,

			tx => tx,
			rx => rx,

			rts => rts,
			dtr => dtr,

			dsr => dsr,
			dcd => dcd,
			cts => cts,
			ri => ri
		);

	clkgen_bus: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => bus_clk,
			reset => reset,
			stop => stop
		);

	clkgen_uart: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => uart_clk,
			reset => reset,
			stop => stop
		);

	stimulus: process
		begin
			reset <= 'L';
			stop <= '0';

			reg_address <= "000";
			read <= '0';
			write <= '0';
			rx <= '1';

			wait for 50 ns;

			-- set DLAB
			reg_address <= "011";
			data_write <= b"1000_0000";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- divisor MSB
			reg_address <= "001";
			data_write <= b"0000_0000";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- divisor LSB
			reg_address <= "000";
			data_write <= b"0000_0010";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- clear DLAB, set word length (LCR7, LCR0-1)
			reg_address <= "011";
			data_write <= b"0000_0011";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- enable FIFO mode (FCR0)
			reg_address <= "010";
			data_write <= b"0000_0001";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- enable loopback (MCR4)
			reg_address <= "100";
			data_write <= b"0001_0000";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- write a byte to THE
			reg_address <= "000";
			data_write <= x"AB";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- write another byte to THE
			reg_address <= "000";
			data_write <= x"39";
			write <= '1';
			wait for 10 ns;
			write <= '0';

			-- wait for first byte tx
			wait for 7 us;

			reg_address <= "000";
			read <= '1';
			wait for 10 ns;
			read <= '0';


			wait for 10000 ns;
			stop <= '1';
			wait;
		end process;
end behaviour;
