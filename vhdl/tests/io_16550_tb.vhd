library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.uart_16550_pkg.all;

entity io_16550_tb is
end io_16550_tb;

architecture behaviour of io_16550_tb is
	signal clk, reset, stop : std_logic;
	signal xmit_data : std_logic_vector(7 downto 0);
	signal xmit_data_ready : std_logic;
	signal rcvr_data : std_logic_vector(7 downto 0);
	signal rcvr_data_ready : std_logic;
	signal xmit_busy : std_logic;
	signal rx, tx    : std_logic;
begin
	uut: entity work.io_16550
		port map (
			rclk => clk,
			reset => reset,
			num_data_bits => 8,
			stop_bits_clk_count => 16,
			parity => NONE,
			break => '0',
			xmit_busy => xmit_busy,
			rcvr_data_ready => rcvr_data_ready,
			rcvr_data_out => rcvr_data,
			xmit_data_ready => xmit_data_ready,
			xmit_data_in => xmit_data,
			tx => tx,
			rx => rx
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	rx <= tx;
	stimulus: process
		begin
			stop <= '0';
			reset <= 'L';
			xmit_data_ready <= '0';
			wait until reset = 'L' and falling_edge(clk);

			xmit_data <= x"AB";
			wait for 30 ns;

			xmit_data_ready <= '1';
			wait for 10 ns;

			xmit_data_ready <= '0';
			wait until xmit_busy = '0';

			xmit_data <= x"F0";
			xmit_data_ready <= '1';


			wait for 1000 ns;
			stop <= '1';
			wait;
		end process;
end behaviour;
