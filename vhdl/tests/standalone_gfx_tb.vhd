library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity standalone_gfx_tb is
end standalone_gfx_tb;

architecture behaviour of standalone_gfx_tb is
	signal clk_100mhz, reset, stop : std_logic;

	component standalone_gfx is
		port (
			clk_100mhz : in std_logic;
			reset : in std_logic;

			dvi_clk_p   : out std_logic;
			dvi_clk_n   : out std_logic;
			dvi_red_p   : out std_logic;
			dvi_red_n   : out std_logic;
			dvi_green_p : out std_logic;
			dvi_green_n : out std_logic;
			dvi_blue_p  : out std_logic;
			dvi_blue_n  : out std_logic
		);
	end component;
begin
	uut: standalone_gfx
		port map (
			clk_100mhz => clk_100mhz,
			reset => reset
		);

	clkgen: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk_100mhz,
			stop => stop
		);

	stimulus: process
	begin
		reset <= 'L';
		stop <= '0';
		wait for 5000000 ns;
		stop <= '1';
		wait;
	end process;
end behaviour;
