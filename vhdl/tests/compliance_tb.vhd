library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use std.textio.all;

use work.yarm_types_pkg.all,
    work.util_pkg.all;

entity compliance_tb is
	generic (
		IMAGE_SIZE : positive;

		TEST_BINARY_FILE : string;

		TOHOST_ADDR_STR     : string;

		SIGNATURE_START_STR : string;
		SIGNATURE_END_STR   : string;
		SIGNATURE_FILE_PATH : string
	);
end compliance_tb;

architecture behavior of compliance_tb is
	subtype addr_t is unsigned(31 downto 0);

	function string_to_addr(addr : string) return addr_t is
		variable addr_line : line;
		variable result    : addr_t;
	begin
		write(addr_line, addr);
		hex_read(addr_line, result);

		return result;
	end function;

	constant TOHOST_ADDR     : addr_t := string_to_addr(TOHOST_ADDR_STR);
	constant SIGNATURE_START : addr_t := string_to_addr(SIGNATURE_START_STR);
	constant SIGNATURE_END   : addr_t := string_to_addr(SIGNATURE_END_STR);

	signal clk, reset, stop : std_logic;

	signal MEM_addr  : yarm_word;
	signal MEM_read  : std_logic;
	signal MEM_write : std_logic;
	signal MEM_byte_enable : std_logic_vector(3 downto 0);
	signal MEM_data_read   : yarm_word;
	signal MEM_data_write  : yarm_word;

	signal core_mem_addr  : yarm_word;
	signal core_mem_read  : std_logic;
	signal core_mem_write : std_logic;
	signal core_mem_ready : std_logic;
	signal core_mem_byte_enable : std_logic_vector(3 downto 0);
	signal core_mem_data_read   : yarm_word;
	signal core_mem_data_write  : yarm_word;

	signal sysmem_active : std_logic;

	signal readback_started : std_logic := '0';
	signal readback_done    : std_logic;
	signal readback_addr : unsigned(31 downto 0) := SIGNATURE_START;

	file signature_file : text open write_mode is SIGNATURE_FILE_PATH;

	constant MEMORY_ADDR_BITS : positive := addr_bits_req(IMAGE_SIZE);
begin
	uut: entity work.core
		generic map (
			HART_ID => 0,
			RESET_VECTOR => x"8000_0000"
		)
		port map (
			clk   => clk,
			reset => reset,

			MEM_addr        => core_mem_addr,
			MEM_read        => core_mem_read,
			MEM_write       => core_mem_write,
			MEM_ready       => core_mem_ready,
			MEM_byte_enable => core_mem_byte_enable,
			MEM_data_read   => core_mem_data_read,
			MEM_data_write  => core_mem_data_write,

			external_int => '0',
			timer_int    => '0',
			software_int => '0'
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	core_mem_ready <= not readback_started;
	core_mem_data_read <= MEM_data_read;

	sysmem_active <= MEM_addr(31) ?= '1' and not (or MEM_addr(30 downto MEMORY_ADDR_BITS));

	mem: entity work.single_port_ram
		generic map (
			ADDR_WIDTH => MEMORY_ADDR_BITS-2,
			DATA_WIDTH => 32,

			COL_WIDTH => 8,

			INIT_FILE => TEST_BINARY_FILE
		)
		port map (
			clk        => clk,
			read       => sysmem_active and MEM_read,
			write      => sysmem_active and MEM_write,
			byte_en    => MEM_byte_enable,
			addr       => MEM_addr(MEMORY_ADDR_BITS-1 downto 2),
			data_read  => MEM_data_read,
			data_write => MEM_data_write
		);

	process(all)
	begin
		if readback_started then
			MEM_addr        <= std_logic_vector(readback_addr);
			MEM_read        <= '1';
			MEM_write       <= '0';
			MEM_byte_enable <= (others => '0');
			MEM_data_write  <= (others => '0');
		else
			MEM_addr        <= core_mem_addr;
			MEM_read        <= core_mem_read;
			MEM_write       <= core_mem_write;
			MEM_byte_enable <= core_mem_byte_enable;
			MEM_data_write  <= core_mem_data_write;
		end if;
	end process;

	readback: process(clk)
		variable sig_word : line;
		variable return_code : unsigned(31 downto 0);
	begin
		if rising_edge(clk) then
			-- less-than/equal: poor man's "implies"
			assert (MEM_read or MEM_write) <= sysmem_active
				report "Memory access outside system memory bounds: " & to_hstring(MEM_addr)
				severity failure;

			if MEM_write = '1' and MEM_addr = std_logic_vector(TOHOST_ADDR) then
				return_code := unsigned(endian_switch(MEM_data_write));

				assert return_code(0)
					report "Unknown syscall: " & to_hstring(return_code)
					severity failure;

				assert return_code(31 downto 1) = 0
					report "*** Test failure (ret = " & natural'image(to_integer(return_code(31 downto 1))) & ") ***"
					severity failure;

				readback_started <= '1';
			end if;

			if readback_started then
				if readback_addr > SIGNATURE_START then
					hwrite(sig_word, endian_switch(MEM_data_read));
					writeline(signature_file, sig_word);
				end if;

				readback_addr <= readback_addr + 4;
			end if;
		end if;
	end process;

	readback_done <= readback_addr ?> SIGNATURE_END;

	stimulus: process
	begin
		stop <= '0';

		wait until readback_done for 1 ms;

		assert readback_done
			report "*** Test timed out ***"
			severity failure;

		report "*** Test finished ***" severity note;

		stop <= '1';
		wait;
	end process;
end behavior;
