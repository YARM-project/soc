library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.uart_16550_pkg.all;

entity fifo_16550_tb is
end fifo_16550_tb;

architecture behaviour of fifo_16550_tb is
	signal clk, reset, stop    : std_logic;
	signal enable_fifo         : std_logic;
	signal frame_in, frame_out : fifo_frame_t := FIFO_FRAME_EMPTY;
	signal add, remove         : std_logic := '0';
	signal full, empty         : std_logic;
	signal has_error           : std_logic;
	signal elements            : integer;
begin
	uut: entity work.fifo_16550
		generic map (
			FIFO_SIZE => 4
		)
		port map (
			clock => clk,
			enable_fifo => enable_fifo,
			reset => reset,
			frame_in => frame_in,
			frame_out => frame_out,
			add => add,
			remove => remove,
			full => full,
			empty => empty,
			has_error => has_error,
			elements => elements
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	stimulus: process
		begin
			reset <= 'L';
			stop <= '0';
			enable_fifo <= '1';
			wait until reset = 'L' and falling_edge(clk);

			assert elements = 0 and frame_out.data = x"00" and empty = '1' and full = '0';

			frame_in.data <= x"FF";
			wait for 10 ns;
			assert elements = 0 and frame_out.data = x"00" and empty = '1' and full = '0';

			add <= '1';
			-- FF
			wait for 10 ns;
			assert elements = 1 and frame_out.data = x"FF" and empty = '0' and full = '0';

			frame_in.data <= x"A1";
			wait for 10 ns;
			-- A1 FF
			assert elements = 2 and frame_out.data = x"FF" and empty = '0' and full = '0';

			add <= '0';
			remove <= '1';
			wait for 10 ns;
			-- A1
			assert elements = 1 and frame_out.data = x"A1" and empty = '0' and full = '0';

			add <= '0';
			remove <= '0';
			wait for 10 ns;
			-- A1
			assert elements = 1 and frame_out.data = x"A1" and empty = '0' and full = '0';

			add <= '1';
			frame_in.data <= x"A2";
			wait for 10 ns;
			-- A2 A1
			assert elements = 2 and frame_out.data = x"A1" and empty = '0' and full = '0';
			frame_in.data <= x"A3";
			frame_in.err_parity <= '1';
			wait for 10 ns;
			-- A3p A2 A1
			assert elements = 3 and frame_out.data = x"A1" and empty = '0' and full = '0';
			frame_in.data <= x"A4";
			frame_in.err_parity <= '0';
			wait for 10 ns;
			-- A4 A3p A2 A1
			assert elements = 4 and frame_out.data = x"A1" and empty = '0' and full = '1';
			frame_in.data <= x"A5";
			wait for 10 ns;
			-- A5 A3p A2 A1
			assert elements = 4 and frame_out.data = x"A1" and empty = '0' and full = '1';

			add <= '1';
			remove <= '1';
			frame_in.data <= x"BA";
			wait for 10 ns;
			add <= '0';
			-- BA A5 A3p A2
			assert elements = 4 and frame_out.data = x"A2" and empty = '0' and full = '1';
			wait for 10 ns;
			-- BA A5 A3p
			assert elements = 3 and frame_out.data = x"A3" and empty = '0' and full = '0';
			wait for 10 ns;
			-- BA A5
			assert elements = 2 and frame_out.data = x"A5" and empty = '0' and full = '0';
			wait for 10 ns;
			-- BA
			assert elements = 1 and frame_out.data = x"BA" and empty = '0' and full = '0';
			wait for 10 ns;
			-- [empty]
			assert elements = 0 and frame_out.data = x"00" and empty = '1' and full = '0';
			wait for 10 ns;
			-- read zeroes from empty queue
			assert elements = 0 and frame_out.data = x"00" and empty = '1' and full = '0';

			add <= '1';
			remove <= '0';
			frame_in.data <= x"EF";
			wait for 10 ns;
			-- EF
			assert elements = 1 and frame_out.data = x"EF" and empty = '0' and full = '0';

			add <= '0';
			reset <= '1';
			wait for 10 ns;
			-- [empty]
			assert elements = 0 and frame_out.data = x"00" and empty = '1' and full = '0';


			wait for 50 ns;
			stop <= '1';
			wait;
		end process;
end behaviour;
