library ieee;
use ieee.std_logic_1164.all;

entity tmds_gen_tb is
end tmds_gen_tb;

architecture behaviour of tmds_gen_tb is
	signal clk, reset, stop : std_logic;
	signal n_reset : std_logic;
	signal blank   : std_logic;
	signal control : std_logic_vector(1 downto 0);

	signal data_in : std_logic_vector(7 downto 0);
	signal encoded : std_logic_vector(9 downto 0);

	signal bias_total : integer := 0;

	function tmds_decode(encoded_in : std_logic_vector(9 downto 0))
	return std_logic_vector is
		variable data_encoded : std_logic_vector(7 downto 0);
		variable data_real    : std_logic_vector(7 downto 0);
	begin
		data_encoded := encoded_in(7 downto 0);

		if encoded_in(9) then
			data_encoded := not data_encoded;
		end if;

		data_real(0) := data_encoded(0);
		if encoded_in(8) then
			for i in 1 to 7 loop
				data_real(i) := data_encoded(i) xnor data_encoded(i-1);
			end loop;
		else
			for i in 1 to 7 loop
				data_real(i) := data_encoded(i) xor data_encoded(i-1);
			end loop;
		end if;

		return data_real;
	end function;
begin
	n_reset <= not reset;

	uut: entity work.tmds_gen
		port map (
			clk     => clk,
			n_reset => n_reset,
			blank   => blank,

			control     => control,
			data_in     => data_in,
			encoded_out => encoded
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	-- add up the output bias on every clock cycle and make sure it doesn't go out of range
	bias_check: process(clk)
		variable bias_delta : integer;
	begin
		-- ignore bias in blanks, the control encodings are fixed to biased values
		if falling_edge(clk) and n_reset = '1' and blank = '0' then
			assert abs(bias_total) <= 8;

			bias_delta := 0;
			for i in encoded'range loop
				if encoded(i) then
					bias_delta := bias_delta + 1;
				else
					bias_delta := bias_delta - 1;
				end if;
			end loop;

			bias_total <= bias_total + bias_delta;
		end if;
	end process;

	stimulus: process
	begin
		reset <= 'L';
		blank <= '1';
		data_in <= x"00";
		control <= "00";
		wait for 38 ns;

		assert encoded = "11" & "01010100";

		blank <= '0';
		wait for 10 ns;
		assert tmds_decode(encoded) = data_in;

		wait for 10 ns;
		assert tmds_decode(encoded) = data_in;

		blank <= '1';
		control <= "01";
		wait for 10 ns;
		assert encoded = "00" & "10101011";

		control <= "10";
		wait for 10 ns;
		assert encoded = "01" & "01010100";

		control <= "11";
		wait for 10 ns;
		assert encoded = "10" & "10101011";

		blank <= '0';
		data_in <= "00010001";
		-- encoded as "0001010101", only bias is in overhead bits -> check if they
		-- are accounted for in bias calculation
		for i in 0 to 20 loop
			wait for 10 ns;
			assert tmds_decode(encoded) = data_in;
		end loop;

		data_in <= "11101110";
		-- encoded as "1100010000", same thing as before but in case we're negating
		for i in 0 to 20 loop
			wait for 10 ns;
			assert tmds_decode(encoded) = data_in;
		end loop;

		stop <= '1';
		wait;
	end process;
end behaviour;
