library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity vga_tb is
end vga_tb;

architecture behaviour of vga_tb is
	constant SCREEN_WIDTH  : integer := 640;
	constant SCREEN_HEIGHT : integer := 480;

	constant CHAR_WIDTH    : integer := 8;
	constant CHAR_HEIGHT   : integer := 16;

	constant H_FRONT : integer := 16;
	constant H_SYNC  : integer := 96;
	constant H_BACK  : integer := 48;

	constant V_FRONT : integer := 10;
	constant V_SYNC  : integer := 2;
	constant V_BACK  : integer := 33;

	signal clk, reset, stop : std_logic;
	signal red, green, blue : std_logic_vector(7 downto 0);
	signal hsync, vsync, blank : std_logic;
begin
	uut: entity work.vga
		generic map (
			PREFETCH_LENGTH => CHAR_WIDTH,
			H_FRONT => H_FRONT,
			H_SYNC  => H_SYNC,
			H_BACK  => H_BACK,
			H_DATA  => SCREEN_WIDTH,
			V_FRONT => V_FRONT,
			V_SYNC  => V_SYNC,
			V_BACK  => V_BACK,
			V_DATA  => SCREEN_HEIGHT
		)
		port map (
			pixel_clk => clk,
			reset => reset,
			hsync => hsync,
			vsync => vsync,
			blank => blank
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	stimulus: process
		begin
			reset <= 'L';
			stop <= '0';
			wait for 1000000 ns;
			stop <= '1';
			wait;
		end process;

	red   <= (others => '0') when blank else "00100100";
	green <= (others => '0') when blank else "01101101";
	blue  <= (others => '0') when blank else "11111111";
end behaviour;
