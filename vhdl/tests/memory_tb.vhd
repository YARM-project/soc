library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.rom.all;

entity memory_tb is
end memory_tb;

architecture rtl of memory_tb is
	signal clk, reset, stop : std_logic;

	signal MEM_addr  : yarm_word;
	signal MEM_read  : std_logic;
	signal MEM_write : std_logic;
	signal MEM_byte_enable : std_logic_vector(3 downto 0);
	signal MEM_data_read   : yarm_word;
	signal MEM_data_write  : yarm_word;
begin
	uut: entity work.memory
		generic map (
			DATA_WIDTH => 32,
			ADDR_WIDTH => 13,
			INIT_DATA  => (x"1234_5678", x"90ab_cdef", others => x"0000_0000")
		)
		port map (
			clk_a        => clk,
			enable_a     => MEM_read or MEM_write,
			write_en_a   => MEM_write,
			byte_en_a    => MEM_byte_enable,
			address_a    => MEM_addr(12 downto 0),
			data_in_a    => MEM_data_write,
			data_out_a   => MEM_data_read,

			clk_b        => '0',
			enable_b     => '0',
			write_en_b   => '0',
			byte_en_b    => (others => '0'),
			address_b    => (others => '0'),
			data_in_b    => (others => '0'),
			data_out_b   => open
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	stimulus: process
	begin
		MEM_read <= '0';
		MEM_write <= '0';
		MEM_byte_enable <= "1111";
		reset <= 'L';
		wait for 42 ns;

		MEM_addr <= x"0000_0000";
		MEM_read <= '1';
		wait for 10 ns;
		MEM_read <= '0';

		MEM_addr <= x"0000_0001";
		MEM_read <= '1';
		wait for 10 ns;
		MEM_read <= '0';

		MEM_addr <= x"0000_0004";
		MEM_read <= '1';
		wait for 10 ns;
		MEM_read <= '0';

		MEM_addr <= x"0000_0008";
		MEM_read <= '1';
		wait for 10 ns;
		MEM_read <= '0';

		wait for 50 ns;
		stop <= '1';
		wait;
	end process;
end rtl;
