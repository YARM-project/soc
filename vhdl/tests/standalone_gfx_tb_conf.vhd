configuration standalone_gfx_tb_conf of standalone_gfx_tb is
	for behaviour
		for uut : standalone_gfx
			use entity work.standalone_gfx;

			for behaviour
				for system_clocks_inst : system_clocks
					use entity work.system_clocks_sim
						generic map (
							CLK_IN_FREQ => 100_000_000
						);
				end for;

				for all : OBUFDS
					use entity work.OBUFDS_sim;
				end for;
			end for;
		end for;
	end for;
end standalone_gfx_tb_conf;
