library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity text_render_tb is
end text_render_tb;

architecture behaviour of text_render_tb is
	constant SCREEN_WIDTH  : integer := 640;
	constant SCREEN_HEIGHT : integer := 480;

	constant CHAR_WIDTH    : integer := 8;
	constant CHAR_HEIGHT   : integer := 16;

	signal clk, reset, stop : std_logic;

	signal px_row, px_col : unsigned(9 downto 0) := (others => '0');

	signal tram_addr : std_logic_vector(11 downto 0);
	signal tram_data : std_logic_vector(7 downto 0);

	signal font_addr : std_logic_vector(10 downto 0);
	signal font_data : std_logic_vector(7 downto 0);

	type fifo_entry_t is record
		row, col : unsigned(9 downto 0);
	end record;
	type fifo_t is array(7 downto 0) of fifo_entry_t;

	signal fifo : fifo_t := (others => ((others => '0'), (others => '0')));
begin
	uut: entity work.text_render
		generic map (
			SCREEN_WIDTH  => SCREEN_WIDTH,
			SCREEN_HEIGHT => SCREEN_HEIGHT,
			CHAR_WIDTH    => CHAR_WIDTH,
			CHAR_HEIGHT   => CHAR_HEIGHT
		)
		port map (
			clk => clk,
			tram_addr => tram_addr,
			tram_data => tram_data,
			font_addr => font_addr,
			font_data => font_data,

			pref_row => px_row,
			pref_col => px_col,

			next_row => fifo(6).row,
			next_col => fifo(6).col
		);

	font_rom: entity work.font_rom
		generic map (
			CHAR_WIDTH  => CHAR_WIDTH,
			CHAR_HEIGHT => CHAR_HEIGHT
		)
		port map (
			clk => clk,
			address => font_addr,
			data => font_data
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	-- imaginary 5x3 screen
	px_counter: process(reset, clk)
		begin
			if reset = '1' then
				px_col <= (others => '0');
				px_row <= (others => '0');
			elsif rising_edge(clk) then
				if px_col = 5 * 8 - 1 then
					px_col <= (others => '0');
					if px_row = 3 * 16 - 1 then
						px_row <= (others => '0');
					else
						px_row <= px_row + 1;
					end if;
				else
					px_col <= px_col + 1;
				end if;
			end if;
		end process;

	coord_fifo: process(clk)
		begin
			if rising_edge(clk) then
				fifo(7 downto 1) <= fifo(6 downto 0);
				fifo(0) <= (px_row, px_col);
			end if;
		end process;

	tram_stub: process(clk)
		begin
			if rising_edge(clk) then
				tram_data <= x"4" & tram_addr(3 downto 0);
			end if;
		end process;

	stimulus: process
		begin
			reset <= 'L';
			stop <= '0';
			wait for 10000 ns;

			stop <= '1';
			wait;
		end process;
end behaviour;
