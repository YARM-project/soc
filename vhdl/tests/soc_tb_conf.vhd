configuration soc_tb_conf of soc_tb is
	for behaviour
		for uut : soc
			use entity work.soc;

			for behavior
				for system_clocks_inst : system_clocks
					use entity work.system_clocks_sim
						generic map (
							CLK_IN_FREQ => 100_000_000
						);
				end for;

				for block_ram_inst : block_ram
					use entity work.single_port_ram
						generic map (
							ADDR_WIDTH => 13,
							DATA_WIDTH => 32,

							COL_WIDTH  => 8,

							INIT_FILE  => "firmware/bin/simulation.hex"
						);
				end for;

				for text_ram_inst : text_ram
					use entity work.sdp_asymm_wrapper
						generic map (
							ADDR_WIDTH_READ => 12,
							DATA_WIDTH_READ => 16,

							ADDR_WIDTH_WRITE => 11,
							DATA_WIDTH_WRITE => 32,

							COL_WIDTH => 8
						);
				end for;

				for io_inst : io
					use entity work.io;

					for rtl
						for ethernet : liteeth_core
							use entity work.ethmac_sim;
						end for;
					end for;
				end for;

				for all : OBUFDS
					use entity work.OBUFDS_sim;
				end for;
			end for;
		end for;
	end for;
end soc_tb_conf;
