library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.util_pkg.all;

entity soc_tb is
end soc_tb;

architecture behaviour of soc_tb is
	signal reset, stop : std_logic;
	signal clk_100mhz  : std_logic;
	signal n_reset     : std_logic;

	component soc is
		generic (
			IS_SIMULATION : std_logic
		);
		port (
			n_reset     : in std_logic;
			buttons     : in std_logic_vector(3 downto 0);
			switches    : in std_logic_vector(3 downto 0);
			leds_simple : out std_logic_vector(3 downto 0);
			led0, led1, led2, led3 : out std_logic_vector(2 downto 0);

			-- 7-segment displays: 7 => decimal point, 6 => g, 0 => a
			--n_sseg_0, n_sseg_1, n_sseg_2, n_sseg_3 : out std_logic_vector(7 downto 0);

			-- Pmod connectors - A+D standard, B+C high-speed.
			-- Defined as inputs by default for safety, change
			-- when necessary
			pmod_a : out std_logic_vector(7 downto 0);
			pmod_b : out std_logic_vector(7 downto 0);
			pmod_c : in std_logic_vector(7 downto 0);
			pmod_d : inout std_logic_vector(7 downto 0);

			clock_100mhz : in std_logic;

			--vga_r, vga_g, vga_b : out std_logic_vector(3 downto 0);
			--vga_hs, vga_vs      : out std_logic;

			uart_rx : in std_logic;
			uart_tx : out std_logic;

			mii_clk_25mhz : out std_logic;
			mii_n_reset   : out std_logic;
			mii_mdio      : inout std_logic;
			mii_mdc       : out std_logic;
			mii_rx_clk    : in std_logic;
			mii_rx_er     : in std_logic;
			mii_rx_dv     : in std_logic;
			mii_rx_data   : in std_logic_vector(3 downto 0);
			mii_tx_clk    : in std_logic;
			mii_tx_en     : out std_logic;
			mii_tx_data   : out std_logic_vector(3 downto 0);
			mii_col       : in std_logic;
			mii_crs       : in std_logic;

			ck_dig_l : inout std_logic_vector(13 downto 0);
			ck_dig_h : inout std_logic_vector(41 downto 26)
		);
	end component;

	signal external_data : std_logic_vector(7 downto 0);
	signal ck_dig_l : std_logic_vector(13 downto 0);
	signal ck_dig_h : std_logic_vector(41 downto 26);
begin
	core_clkgen: entity work.clock_gen
		generic map (
			T_pulse  => 5 ns,
			T_period => 10 ns,
			T_reset  => 105 ns
		)
		port map (
			clk => clk_100mhz,
			reset => reset,
			stop => stop
		);

	uut: soc
		generic map (
			IS_SIMULATION => '1'
		)
		port map (
			n_reset  => n_reset,
			buttons  => (others => '0'),
			switches => (others => '0'),

			pmod_c   => (others => '0'),
			pmod_d   => external_data,

			clock_100mhz => clk_100mhz,
			uart_rx      => '1',

			mii_rx_clk  => '0',
			mii_rx_er   => '0',
			mii_rx_dv   => '0',
			mii_rx_data => (others => '0'),
			mii_tx_clk  => '0',
			mii_col     => '0',
			mii_crs     => '0',

			ck_dig_l => ck_dig_l,
			ck_dig_h => ck_dig_h
		);

	stimulus: process
	begin
		stop <= '0';
		reset <= 'L';
		wait for 250000 ns;
		stop <= '1';
		wait;
	end process;

	n_reset <= not reset;

	external_data <= x"55" when not ck_dig_l(2) else (others => 'Z');
end behaviour;
