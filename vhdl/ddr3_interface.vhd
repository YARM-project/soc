library ieee;
use ieee.std_logic_1164.all;

use work.yarm_types_pkg.all;

-- signal naming conventions:
-- m_: safe in memory clock domain
-- b_: safe in bus clock domain
-- _ms_: metastable, first flip-flop set in the destination domain

-- communication using 2 bits (arrow denotes next required condition):
--    bus_state | bus_waiting         || ddr3_state | ddr3_done
--    ----------+---------------------|+------------+--------------------------
-- 0: IDLE      | 0 -> bus enable     || IDLE       | 0 -> high bus_waiting
-- [...]
-- 1: WAITING   | 1 -> high ddr3_done || IDLE       | 0 -> high bus_waiting
-- 2: WAITING   | 1 -> high ddr3_done || WAITING    | 0 -> mem interfacing done
-- [...]
-- 3: WAITING   | 1 -> high ddr3_done || DONE       | 1 -> low bus_waiting
-- 4: DONE      | 0 -> low ddr3_done  || DONE       | 1 -> low bus_waiting
-- 5: DONE      | 0 -> low ddr3_done  || IDLE       | 0 -> high bus_waiting
-- 6: IDLE      | 0 -> bus enable     || IDLE       | 0 -> high bus_waiting

entity ddr3_interface is
	port (
		clk_ddr3      : in    std_logic;

		ddr3_dq       : inout std_logic_vector(15 downto 0);
		ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
		ddr3_dqs_n    : inout std_logic_vector(1 downto 0);

		ddr3_addr     : out   std_logic_vector(13 downto 0);
		ddr3_ba       : out   std_logic_vector(2 downto 0);
		ddr3_ras_n    : out   std_logic;
		ddr3_cas_n    : out   std_logic;
		ddr3_we_n     : out   std_logic;
		ddr3_reset_n  : out   std_logic;
		ddr3_ck_p     : out   std_logic_vector(0 downto 0);
		ddr3_ck_n     : out   std_logic_vector(0 downto 0);
		ddr3_cke      : out   std_logic_vector(0 downto 0);
		ddr3_cs_n     : out   std_logic_vector(0 downto 0);
		ddr3_dm       : out   std_logic_vector(1 downto 0);
		ddr3_odt      : out   std_logic_vector(0 downto 0);

		clk_bus    : in std_logic;
		n_reset    : in std_logic;
		enable     : in std_logic;
		ready      : out std_logic;
		addr       : in std_logic_vector(27 downto 0);
		data_valid : out std_logic;
		byte_en    : in std_logic_vector(3 downto 0);
		we         : in std_logic;
		d_in       : in yarm_word;
		d_out      : out yarm_word
	);
end ddr3_interface;

architecture behaviour of ddr3_interface is
	component memory_controller
		port (
			ddr3_dq       : inout std_logic_vector(15 downto 0);
			ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
			ddr3_dqs_n    : inout std_logic_vector(1 downto 0);

			ddr3_addr     : out   std_logic_vector(13 downto 0);
			ddr3_ba       : out   std_logic_vector(2 downto 0);
			ddr3_ras_n    : out   std_logic;
			ddr3_cas_n    : out   std_logic;
			ddr3_we_n     : out   std_logic;
			ddr3_reset_n  : out   std_logic;
			ddr3_ck_p     : out   std_logic_vector(0 downto 0);
			ddr3_ck_n     : out   std_logic_vector(0 downto 0);
			ddr3_cke      : out   std_logic_vector(0 downto 0);
			ddr3_cs_n     : out   std_logic_vector(0 downto 0);
			ddr3_dm       : out   std_logic_vector(1 downto 0);
			ddr3_odt      : out   std_logic_vector(0 downto 0);

			-- Application ports
			app_addr            : in  std_logic_vector(27 downto 0);
			app_cmd             : in  std_logic_vector(2 downto 0);
			app_en              : in  std_logic;
			app_wdf_data        : in  std_logic_vector(127 downto 0);
			app_wdf_end         : in  std_logic;
			app_wdf_mask        : in  std_logic_vector(15 downto 0);
			app_wdf_wren        : in  std_logic;
			app_rd_data         : out std_logic_vector(127 downto 0);
			app_rd_data_end     : out std_logic;
			app_rd_data_valid   : out std_logic;
			app_rdy             : out std_logic;
			app_wdf_rdy         : out std_logic;
			app_sr_req          : in  std_logic;
			app_ref_req         : in  std_logic;
			app_zq_req          : in  std_logic;
			app_sr_active       : out std_logic;
			app_ref_ack         : out std_logic;
			app_zq_ack          : out std_logic;
			ui_clk              : out std_logic;
			ui_clk_sync_rst     : out std_logic;
			init_calib_complete : out std_logic;

			-- System Clock Ports
			sys_clk_i     : in std_logic;
			device_temp_i : in std_logic_vector(11 downto 0);
			sys_rst       : in std_logic
		);
	end component memory_controller;

	signal app_addr            : std_logic_vector(27 downto 0) := (others => '0');
	signal app_cmd             : std_logic_vector(2 downto 0) := "001";
	signal app_en              : std_logic := '0';
	signal app_wdf_data        : std_logic_vector(127 downto 0) := (others => '0');
	signal app_wdf_end         : std_logic := '0';
	signal app_wdf_mask        : std_logic_vector(15 downto 0) := (others => '0');
	signal app_wdf_wren        : std_logic := '0';
	signal app_rd_data         : std_logic_vector(127 downto 0);
	signal app_rd_data_end     : std_logic;
	signal app_rd_data_valid   : std_logic;
	signal app_rdy             : std_logic;
	signal app_wdf_rdy         : std_logic;
	--signal app_ref_req         : std_logic;
	--signal app_zq_req          : std_logic;
	--signal app_ref_ack         : std_logic;
	--signal app_zq_ack          : std_logic;
	signal ui_clk              : std_logic;
	signal ui_clk_sync_rst     : std_logic;
	signal init_calib_complete : std_logic;
	signal device_temp_i       : std_logic_vector(11 downto 0) := (others => '1');

	type state_t is (
		IDLE,
		READ_WAITING, READ_DONE,
		WRITE_WAITING, WRITE_DONE
	);

	signal bus_state  : state_t := IDLE;
	signal ddr3_state : state_t := IDLE;

	-- synchronization signals
	signal bus_waiting : std_logic;
	signal ddr3_done   : std_logic;

	-- clock domain crossing
	signal m_ms_bus_waiting : std_logic;
	signal m_bus_waiting    : std_logic;
	signal b_ms_ddr3_done : std_logic;
	signal b_ddr3_done    : std_logic;

	-- bus => ddr3
	signal latched_we      : std_logic;
	-- byte address, need to cut off LSB for (16-bit word) memory address
	signal latched_addr    : std_logic_vector(27 downto 0);
	signal latched_d_in    : yarm_word;
	signal latched_byte_en : std_logic_vector(3 downto 0);

	-- ddr3 => bus
	signal latched_d_out : std_logic_vector(127 downto 0);

	constant CMD_READ  : std_logic_vector(2 downto 0) := "001";
	constant CMD_WRITE : std_logic_vector(2 downto 0) := "000";
begin
	controller: memory_controller
		port map (
			-- Memory interface ports
			ddr3_dq                        => ddr3_dq,
			ddr3_dqs_p                     => ddr3_dqs_p,
			ddr3_dqs_n                     => ddr3_dqs_n,
			ddr3_addr                      => ddr3_addr,
			ddr3_ba                        => ddr3_ba,
			ddr3_ras_n                     => ddr3_ras_n,
			ddr3_cas_n                     => ddr3_cas_n,
			ddr3_we_n                      => ddr3_we_n,
			ddr3_reset_n                   => ddr3_reset_n,
			ddr3_ck_p                      => ddr3_ck_p,
			ddr3_ck_n                      => ddr3_ck_n,
			ddr3_cke                       => ddr3_cke,
			ddr3_cs_n                      => ddr3_cs_n,
			ddr3_dm                        => ddr3_dm,
			ddr3_odt                       => ddr3_odt,
			-- Application interface ports
			app_addr                       => app_addr,
			app_cmd                        => app_cmd,
			app_en                         => app_en,
			app_wdf_data                   => app_wdf_data,
			app_wdf_end                    => app_wdf_end,
			app_wdf_mask                   => app_wdf_mask,
			app_wdf_wren                   => app_wdf_wren,
			app_rd_data                    => app_rd_data,
			app_rd_data_end                => app_rd_data_end,
			app_rd_data_valid              => app_rd_data_valid,
			app_rdy                        => app_rdy,
			app_wdf_rdy                    => app_wdf_rdy,
			app_sr_req                     => '0',
			app_ref_req                    => '0',
			app_zq_req                     => '0',
			app_sr_active                  => open,
			app_ref_ack                    => open,
			app_zq_ack                     => open,
			ui_clk                         => ui_clk,
			ui_clk_sync_rst                => ui_clk_sync_rst,
			init_calib_complete            => init_calib_complete,
			-- System Clock Ports
			sys_clk_i                      => clk_ddr3,
			device_temp_i                  => device_temp_i,
			sys_rst                        => n_reset
		);

	bus_fsm: process(clk_bus)
	begin
		if rising_edge(clk_bus) then
			data_valid <= '0';

			bus_waiting <= '0';

			case bus_state is
				when IDLE =>
					if enable then
						latched_we <= we;
						latched_addr <= addr;
						latched_d_in <= d_in;
						latched_byte_en <= byte_en;

						if we then
							bus_state <= WRITE_WAITING;
						else
							bus_state <= READ_WAITING;
						end if;
					end if;
				when READ_WAITING =>
					bus_waiting <= '1';
					if b_ddr3_done then
						bus_state <= READ_DONE;
						d_out <= latched_d_out(127 downto 96);
						data_valid <= '1';
					end if;
				when READ_DONE =>
					if not b_ddr3_done then
						bus_state <= IDLE;
					end if;
				when WRITE_WAITING =>
					bus_waiting <= '1';
					if b_ddr3_done then
						bus_state <= WRITE_DONE;
					end if;
				when WRITE_DONE =>
					if not b_ddr3_done then
						bus_state <= IDLE;
					end if;
			end case;
		end if;
	end process;

	ddr3_fsm: process(ui_clk)
	begin
		if rising_edge(ui_clk) then
			app_en <= '0';
			app_wdf_wren <= '0';

			ddr3_done <= '0';

			case ddr3_state is
				when IDLE =>
					if m_bus_waiting then
						if app_en and app_rdy then
							if latched_we then
								ddr3_state <= WRITE_WAITING;
							else
								ddr3_state <= READ_WAITING;
							end if;
						else
							app_en <= '1';
							-- workaround: memory uses 16-bit words, address should
							-- thus only be (26 downto 0) - instead, MSB is ignored
							app_addr <= '0' & latched_addr(27 downto 1);

							if latched_we then
								app_cmd <= CMD_WRITE;
							else
								app_cmd <= CMD_READ;
							end if;
						end if;
					end if;
				when READ_WAITING =>
					if app_rd_data_valid then
						ddr3_state <= READ_DONE;
						latched_d_out <= app_rd_data;
					end if;
				when READ_DONE =>
					ddr3_done <= '1';
					if not m_bus_waiting then
						ddr3_state <= IDLE;
					end if;
				when WRITE_WAITING =>
					app_wdf_wren <= '1';
					app_wdf_end <= '1';

					-- writes completely ignore the lower address bits, always
					-- writing a complete BL8 block from 0 to 7, meaning we need to
					-- shift our data and mask to the correct 32-bit word manually.
					app_wdf_data <= (others => '0');
					app_wdf_mask <= x"FFFF";
					case latched_addr(3 downto 2) is
						when "00" =>
							app_wdf_data(127 downto 96) <= latched_d_in;
							app_wdf_mask(15 downto 12)  <= not latched_byte_en;
						when "01" =>
							app_wdf_data(95 downto 64) <= latched_d_in;
							app_wdf_mask(11 downto 8)  <= not latched_byte_en;
						when "10" =>
							app_wdf_data(63 downto 32) <= latched_d_in;
							app_wdf_mask(7 downto 4)   <= not latched_byte_en;
						when "11" =>
							app_wdf_data(31 downto 0) <= latched_d_in;
							app_wdf_mask(3 downto 0)  <= not latched_byte_en;
						when others =>
							report "bad address" severity failure;
					end case;

					if app_wdf_rdy then
						ddr3_state <= WRITE_DONE;
					end if;
				when WRITE_DONE =>
					ddr3_done <= '1';
					if not m_bus_waiting then
						ddr3_state <= IDLE;
					end if;
			end case;
		end if;
	end process;

	crossing_to_bus: process(clk_bus)
	begin
		if rising_edge(clk_bus) then
			b_ms_ddr3_done <= ddr3_done;
			b_ddr3_done <= b_ms_ddr3_done;
		end if;
	end process;

	crossing_to_ddr3: process(ui_clk)
	begin
		if rising_edge(ui_clk) then
			m_ms_bus_waiting <= bus_waiting;
			m_bus_waiting <= m_ms_bus_waiting;
		end if;
	end process;

	ready <= '1' when bus_state = IDLE else '0';
end behaviour;
