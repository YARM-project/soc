library ieee;
use ieee.std_logic_1164.all;

use work.uart_16550_pkg.all;

entity parity_16550 is
	port (
		clk   : in std_logic;
		add   : in std_logic;
		reset : in std_logic;

		parity_type : in parity_t;
		data_in     : in std_logic;
		parity_out  : out std_logic
	);
end parity_16550;

architecture rtl of parity_16550 is
	signal curr_parity : std_logic;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				if parity_type = EVEN then
					curr_parity <= '0';
				else
					curr_parity <= '1';
				end if;
			elsif add = '1' then
				case parity_type is
					when EVEN | ODD =>
						curr_parity <= curr_parity xor data_in;
					when FORCE_0 =>
						curr_parity <= '0';
					when FORCE_1 =>
						curr_parity <= '1';
					when NONE =>
				end case;
			end if;
		end if;
	end process;

	parity_out <= curr_parity;
end rtl;
