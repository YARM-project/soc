library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

use work.uart_16550_pkg.all;

-- Implementation of the FIFO in the 16550 UART
-- data_out contains current last element
-- overwrites most recent element when full
-- returns zeroes when empty

entity fifo_16550 is
	generic (
		FIFO_SIZE : integer
	);
	port (
		clock     : in std_logic;
		-- enable FIFO functionality - otherwise just use a single buffer register
		enable_fifo : in std_logic;
		reset     : in std_logic;
		frame_in  : in fifo_frame_t;
		frame_out : out fifo_frame_t;

		-- Operation
		add, remove : in std_logic;
		-- Status indication
		full, empty : out std_logic;
		-- indicates at least one of the FIFO elements has an error condition set
		has_error   : out std_logic;
		-- number of elements
		elements    : out natural
	);
end fifo_16550;

architecture behaviour of fifo_16550 is
	type fifo_t is array(0 to FIFO_SIZE-1) of fifo_frame_t;

	signal num_elements : integer := 0;

	signal memory : fifo_t := (others => FIFO_FRAME_EMPTY);
	signal last_fifo_mode : std_logic;

	function is_err_frame(frame : fifo_frame_t) return boolean is
	begin
		return (frame.err_parity or frame.err_framing or frame.err_break) = '1';
	end is_err_frame;

	function has_err_frame(fifo : fifo_t) return boolean is
	begin
		for i in fifo'range loop
			if is_err_frame(fifo(i)) then
				return true;
			end if;
		end loop;

		return false;
	end has_err_frame;
begin
	elements <= num_elements;
	empty <= '1' when num_elements = 0 else '0';
	full  <= '1' when
	         (enable_fifo = '1' and num_elements = FIFO_SIZE) or
	         (enable_fifo = '0' and num_elements = 1) else '0';

	frame_out <= memory(0);

	has_error <= '1' when enable_fifo = '1' and has_err_frame(memory) else '0';

	process(clock, reset, enable_fifo)
	begin
		if rising_edge(clock) then
			last_fifo_mode <= enable_fifo;

			if reset = '1' or enable_fifo /= last_fifo_mode then
				memory <= (others => FIFO_FRAME_EMPTY);
				num_elements <= 0;
			end if;

			if add = '1' and remove = '0' then
				if enable_fifo = '0' then
					-- just use element 0 when not in FIFO mode
					memory(0) <= frame_in;
					num_elements <= 1;
				elsif num_elements = FIFO_SIZE then
					-- overwrite most recent (highest) element when FIFO full
					memory(num_elements - 1) <= frame_in;
				else
					-- otherwise, append element
					memory(num_elements) <= frame_in;
					num_elements <= num_elements + 1;
				end if;
			elsif add = '0' and remove = '1' then
				if num_elements /= 0 then
					num_elements <= num_elements - 1;
				end if;

				-- shift down and zero last element
				memory <= memory(1 to memory'high) & FIFO_FRAME_EMPTY;
			elsif add = '1' and remove = '1' then
				memory <= memory(1 to memory'high) & FIFO_FRAME_EMPTY;

				if num_elements = 0 then
					num_elements <= 1;
					memory(0) <= frame_in;
				else
					memory(num_elements - 1) <= frame_in;
				end if;
			end if;
		end if;
	end process;
end behaviour;
