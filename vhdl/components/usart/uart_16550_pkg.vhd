library ieee;
use ieee.std_logic_1164.all;

package uart_16550_pkg is
	type fifo_frame_t is record
		data        : std_logic_vector(7 downto 0);
		err_parity  : std_logic;
		err_framing : std_logic;
		err_break   : std_logic;
	end record;

	constant FIFO_FRAME_EMPTY : fifo_frame_t := (x"00", '0', '0', '0');

	type parity_t is (NONE, EVEN, ODD, FORCE_0, FORCE_1);

	constant CYCLES_PER_BIT : integer := 16;

	constant STOP_CYCLES_ONE        : integer := 1 * CYCLES_PER_BIT;
	constant STOP_CYCLES_ONEANDHALF : integer := CYCLES_PER_BIT + CYCLES_PER_BIT/2;
	constant STOP_CYCLES_TWO        : integer := 2 * CYCLES_PER_BIT;

	type modem_config_t is record
		num_data_bits : integer range 5 to 8;
		-- how many clock pulses to send a stop bit for - 1 bit = 16 pulses
		stop_bits_clk_count : integer range STOP_CYCLES_ONE to STOP_CYCLES_TWO;
		parity        : parity_t;
	end record;
end package;
