library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.uart_16550_pkg.all;

entity io_16550 is
	port (
		-- receiver clock - 16x baud rate
		rclk  : in std_logic;
		reset : in std_logic;

		modem_config    : in modem_config_t;

		rcvr_data_ready  : out std_logic := '0';
		rcvr_data_out    : out std_logic_vector(7 downto 0);
		rcvr_err_parity  : out std_logic;
		rcvr_err_framing : out std_logic;
		rcvr_break       : out std_logic;


		-- indicates the TX line should be forced low
		break           : in std_logic;
		xmit_data_ready : in std_logic;
		xmit_busy       : out std_logic;
		xmit_data_in    : in std_logic_vector(7 downto 0);

		rx_raw : in std_logic;
		tx_out : out std_logic
	);
end io_16550;

architecture behaviour of io_16550 is
	type modem_state_section_t is (IDLE, START, DATA, PARITY_BIT, STOP);
	type modem_state_t is record
		section   : modem_state_section_t;
		clk_count : integer;
	end record;

	constant MODEM_STATE_INIT : modem_state_t := (IDLE, 0);

	signal rx_synced : std_logic;

	signal rx_state : modem_state_t := MODEM_STATE_INIT;
	signal tx_state : modem_state_t := MODEM_STATE_INIT;

	signal latched_tx_data   : std_logic_vector(7 downto 0);
	signal rx_breaking       : std_logic;

	signal tx_par_add    : std_logic;
	signal tx_par_parity : parity_t;
	signal tx_par_reset  : std_logic;
	signal tx_par_result : std_logic;

	signal rx_par_add    : std_logic;
	signal rx_par_parity : parity_t;
	signal rx_par_reset  : std_logic;
	signal rx_par_result : std_logic;

	signal curr_rcvr_data : std_logic_vector(7 downto 0);

	-- internal TX line, without breaking
	signal tx : std_logic;

	procedure set_section(
		signal state : inout modem_state_t;
		constant section : modem_state_section_t) is
	begin
		state.section <= section;
		state.clk_count <= 0;
	end procedure;
begin
	rx_syncer: entity work.synchronize_bit
		generic map (
			LEVELS => 2
		)
		port map (
			clk => rclk,
			data_in  => rx_raw,
			data_out => rx_synced
		);

	tx_parity: entity work.parity_16550
		port map (
			clk   => rclk,
			add   => tx_par_add,
			reset => tx_par_reset,

			parity_type => tx_par_parity,
			data_in     => latched_tx_data(0),
			parity_out  => tx_par_result
		);

	rx_parity: entity work.parity_16550
		port map (
			clk   => rclk,
			add   => rx_par_add,
			reset => rx_par_reset,

			parity_type => rx_par_parity,
			data_in     => curr_rcvr_data(7),
			parity_out  => rx_par_result
		);

	tx_proc: process(rclk, reset)
		variable tx_config : modem_config_t;
	begin
		if rising_edge(rclk) then
			tx_par_add <= '0';
			tx_par_reset <= '0';

			if reset = '1' then
				set_section(tx_state, IDLE);
			else
				tx_state.clk_count <= tx_state.clk_count + 1;

				case tx_state.section is
					when IDLE =>
						tx <= '1';
						if xmit_data_ready = '1' then
							latched_tx_data <= xmit_data_in;
							tx_par_reset <= '1';
							tx_config := modem_config;
							tx_par_parity <= tx_config.parity;
							set_section(tx_state, START);
						end if;

					when START =>
						tx <= '0';
						if tx_state.clk_count = CYCLES_PER_BIT - 1 then
							set_section(tx_state, DATA);
						end if;

					when DATA =>
						tx <= latched_tx_data(0);

						if (tx_state.clk_count + 1) mod CYCLES_PER_BIT = 0 then
							-- next bit coming up, shift from MSB toward LSB
							latched_tx_data <= '0' & latched_tx_data(7 downto 1);
						end if;

						if tx_state.clk_count mod CYCLES_PER_BIT = 0 then
							-- add current bit to parity sum
							tx_par_add <= '1';
						end if;

						if (tx_state.clk_count + 1) / CYCLES_PER_BIT = tx_config.num_data_bits then
							if tx_config.parity /= NONE then
								set_section(tx_state, PARITY_BIT);
							else
								set_section(tx_state, STOP);
							end if;
						end if;

					when PARITY_BIT =>
						tx <= tx_par_result;
						if tx_state.clk_count = CYCLES_PER_BIT - 1 then
							set_section(tx_state, STOP);
						end if;

					when STOP =>
						tx <= '1';
						-- allow 2 early idle clock cycles to fetch the next character
						if tx_state.clk_count = tx_config.stop_bits_clk_count - 1 - 2 then
							set_section(tx_state, IDLE);
						end if;
				end case;
			end if;
		end if;
	end process;

	tx_out <= tx and not break;

	rx_proc: process(rclk, rx_synced, reset)
		variable rx_config : modem_config_t;
	begin
		if rising_edge(rclk) then
			if rx_synced = '1' then
				-- reset RX break marker
				rx_breaking <= '0';
			end if;

			rx_par_add <= '0';
			rx_par_reset <= '0';

			if reset = '1' then
				set_section(rx_state, IDLE);
			else
				rx_state.clk_count <= rx_state.clk_count + 1;

				case rx_state.section is
					when IDLE =>
						rcvr_data_ready <= '0';
						if rx_synced = '0' then
							rx_breaking <= '1';
							rx_par_reset <= '1';

							rx_config := modem_config;
							rx_par_parity <= rx_config.parity;

							rcvr_err_framing <= '0';
							rcvr_err_parity <= '0';
							rcvr_break <= '0';

							set_section(rx_state, START);
						end if;

					when START =>
						-- wait an extra half bit so sampling happens in the middle of a period
						if rx_state.clk_count = CYCLES_PER_BIT + CYCLES_PER_BIT/2 - 1 then
							set_section(rx_state, DATA);
						end if;

					when DATA =>
						if rx_state.clk_count mod CYCLES_PER_BIT = 0 then
							-- shift in next bit (insert at MSB, so first bit ends up as LSB)
							curr_rcvr_data <= rx_synced & curr_rcvr_data(7 downto 1);

							rx_par_add <= '1';
						end if;

						if (rx_state.clk_count + 1) / CYCLES_PER_BIT = rx_config.num_data_bits then
							if rx_config.parity /= NONE then
								set_section(rx_state, PARITY_BIT);
							else
								set_section(rx_state, STOP);
							end if;
						end if;

					when PARITY_BIT =>
						if rx_state.clk_count = 0 then
							if rx_synced /= rx_par_result then
								rcvr_err_parity <= '1';
							end if;
						elsif rx_state.clk_count = CYCLES_PER_BIT - 1 then
							set_section(rx_state, STOP);
						end if;

					when STOP =>
						if rx_synced = '1' then
							rcvr_data_ready <= '1';
							set_section(rx_state, IDLE);
						else
							if (rx_state.clk_count = rx_config.stop_bits_clk_count and
							  rx_breaking = '1') then
								rcvr_data_ready <= '1';
								rcvr_break <= '1';
							end if;
							-- framing error
							rcvr_err_framing <= '1';
						end if;
				end case;
			end if;
		end if;
	end process;

	xmit_busy <= '0' when tx_state.section = IDLE else '1';
	rcvr_data_out <= curr_rcvr_data;
end behaviour;
