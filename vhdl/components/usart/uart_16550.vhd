library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.uart_16550_pkg.all;

-- Implementation of a 16550 compatiable UART

entity uart_16550 is
	port (
		-- Control Lines
		bus_clock    : in std_logic;
		uart_clock   : in std_logic;
		reset        : in std_logic;

		-- bus interface
		address      : in std_logic_vector(2 downto 0);
		read         : in std_logic;
		write        : in std_logic;
		data_read    : out std_logic_vector(7 downto 0);
		data_write   : in std_logic_vector(7 downto 0);

		interrupt    : out std_logic;

		-- UART Interface
		tx           : out std_logic;
		rx           : in std_logic;
		-- flow control
		n_rts, n_dtr, n_out1, n_out2 : out std_logic;
		n_cts, n_dsr, n_ri,   n_dcd  : in std_logic
	);
end uart_16550;

architecture behaviour of uart_16550 is
	subtype register_t is std_logic_vector(7 downto 0);

	signal interrupt_enable_reg  : register_t;
	signal interrupt_identification_reg : register_t;
	signal fifo_ctrl_reg         : register_t;
	signal line_ctrl_reg         : register_t;
	signal modem_ctrl_reg        : register_t;
	signal line_status_reg       : register_t;
	signal modem_status_reg      : register_t;
	signal scratch_reg           : register_t;
	signal divisor_latch_reg     : std_logic_vector(15 downto 0);

	signal divisor_synced  : std_logic_vector(15 downto 0);
	signal divisor_changed : std_logic;

	signal prev_modem_status     : register_t;

	-- transmit holding register empty
	alias THRE     : std_logic is line_status_reg(5);

	-- divisor latch access bit
	alias DLAB     : std_logic is line_ctrl_reg(7);
	alias loopback : std_logic is modem_ctrl_reg(4);

	signal rx_buffer        : fifo_frame_t := FIFO_FRAME_EMPTY;

	signal rx_fifo_reset    : std_logic;
	signal rx_fifo_in       : fifo_frame_t;
	signal rx_fifo_add      : std_logic;
	signal rx_fifo_remove   : std_logic;
	signal rx_fifo_full     : std_logic;
	signal rx_fifo_empty    : std_logic;
	signal rx_fifo_elements : natural;

	-- used to mask bits 2-4 in line status register after it's been read
	signal mask_rx_character_errors : std_logic;

	signal tx_fifo_data     : std_logic_vector(7 downto 0);

	signal tx_fifo_reset    : std_logic;
	signal tx_fifo_out      : fifo_frame_t;
	signal tx_fifo_add      : std_logic;
	signal tx_fifo_remove   : std_logic := '0';

	signal num_data_bits    : integer range 5 to 8;
	signal stop_bits_clk_count : integer range 16 to 32;
	signal parity           : parity_t;
	signal modem_rx_ready   : std_logic;
	signal prev_rx_ready    : std_logic;
	signal modem_tx_send    : std_logic;
	signal modem_tx_busy    : std_logic;
	signal prev_tx_busy    : std_logic;
	signal modem_rx, modem_tx : std_logic;

	-- receiver clock
	signal rclk             : std_logic;

	-- similar layout to interrupt enable register (with additional timeout)
	-- 0 => data available
	-- 1 => THRE
	-- 2 => line status error
	-- 3 => modem status changed
	-- 4 => timeout
	signal interrupts_pending : std_logic_vector(4 downto 0);
	signal rx_data_interrupt  : std_logic;
	signal rx_threshold       : natural range 1 to 14;

	signal reset_rx_timeout : std_logic;
	signal rx_timeout_triggered : std_logic;
	-- init to approximately 4 character times
	constant RX_TIMEOUT_MAX : natural := 4 * (16 * 10 + 32);
	signal rx_timeout_ticks : natural range 0 to RX_TIMEOUT_MAX := RX_TIMEOUT_MAX;
	signal rx_timeout_left  : natural range 0 to RX_TIMEOUT_MAX := RX_TIMEOUT_MAX;

	-- why is this not in std_logic_1164?
	function to_std_logic(b : boolean) return std_logic is
	begin
		if b then
			return '1';
		else
			return '0';
		end if;
	end function;
begin
	rx_fifo: entity work.fifo_16550
		generic map (
			FIFO_SIZE => 16
		)
		port map (
			clock => bus_clock,
			enable_fifo => fifo_ctrl_reg(0),
			reset => rx_fifo_reset,
			frame_in => rx_fifo_in,
			frame_out => rx_buffer,
			add => rx_fifo_add,
			remove => rx_fifo_remove,
			full => rx_fifo_full,
			empty => rx_fifo_empty,
			has_error => line_status_reg(7),
			elements => rx_fifo_elements
		);

	tx_fifo: entity work.fifo_16550
		generic map (
			FIFO_SIZE => 16
		)
		port map (
			clock => bus_clock,
			enable_fifo => fifo_ctrl_reg(0),
			reset => tx_fifo_reset,
			frame_in => (
				data => tx_fifo_data,
				err_parity  => '0',
				err_framing => '0',
				err_break   => '0'
			),
			frame_out => tx_fifo_out,
			add => tx_fifo_add,
			remove => tx_fifo_remove,
			full => open,
			empty => THRE,
			elements => open
		);

	io: entity work.io_16550
		port map (
			rclk => rclk,
			reset => reset,

			modem_config => (
				num_data_bits => num_data_bits,
				stop_bits_clk_count => stop_bits_clk_count,
				parity => parity
			),

			break => line_ctrl_reg(6),

			rcvr_data_ready => modem_rx_ready,
			rcvr_data_out => rx_fifo_in.data,
			rcvr_err_parity => rx_fifo_in.err_parity,
			rcvr_err_framing => rx_fifo_in.err_framing,
			rcvr_break => rx_fifo_in.err_break,

			xmit_data_ready => modem_tx_send,
			xmit_busy => modem_tx_busy,
			xmit_data_in => tx_fifo_out.data,

			rx_raw => modem_rx,
			tx_out => modem_tx
		);

	divisor_sync: entity work.synchronize_vector
		generic map (
			DATA_WIDTH => 16
		)
		port map (
			clk_master => bus_clock,
			clk_slave  => uart_clock,

			data_in  => divisor_latch_reg,
			data_out => divisor_synced,

			changed => divisor_changed
		);

	baud_gen: entity work.clock_divider
		generic map (
			DIVIDE_FACTOR_WIDTH => 16,
			AUTO_RESET => false
		)
		port map (
			clk_in => uart_clock,
			reset  => divisor_changed,

			divisor => unsigned(divisor_latch_reg),
			clk_out => rclk
		);

	registers: process(bus_clock, reset, modem_status_reg)
	begin
		if reset = '1' then
			-- Reset all registers to sane values
			interrupt_enable_reg         <= b"0000_0000";
			fifo_ctrl_reg                <= b"0000_0000";
			line_ctrl_reg                <= b"0000_0000";
			modem_ctrl_reg               <= b"0000_0000";
			--line_status_reg(4 downto 1)  <=    b"0_000";
			line_status_reg(1 downto 1)  <=        b"0";
			modem_status_reg(3 downto 0) <=      b"0000";
			--prev_modem_status            <= modem_status_reg;
			divisor_latch_reg            <= x"FFFF";
		elsif rising_edge(bus_clock) then
			-- FIFO clear bits, self-resetting
			fifo_ctrl_reg(1) <= '0';
			fifo_ctrl_reg(2) <= '0';

			tx_fifo_add <= '0';
			rx_fifo_remove <= '0';

			-- ============================
			-- FIFO interface on modem side
			-- ============================
			-- remove from TX FIFO when transmitter goes busy
			tx_fifo_remove <= modem_tx_busy and not prev_tx_busy;
			prev_tx_busy <= modem_tx_busy;
			-- add to RX FIFO when receiver data becomes ready, overflow is ignored
			rx_fifo_add <= modem_rx_ready and not prev_rx_ready;
			prev_rx_ready <= modem_rx_ready;


			-- =============
			-- rx error bits
			-- =============
			-- if we just removed a frame, or a new frame is added to an
			-- empty FIFO, reveal line status error bits again
			if rx_fifo_remove or (rx_fifo_empty and rx_fifo_add) then
				mask_rx_character_errors <= '0';
			end if;

			-- if the modem has data ready, but the FIFO is full
			if rx_fifo_add and rx_fifo_full then
				-- set overrun error
				line_status_reg(1) <= '1';
			end if;

			-- ================
			-- modem delta bits
			-- ================
			-- DCD, DSR, CTS changed - RI will be overwritten in next line
			modem_status_reg(3 downto 0) <=
				-- already marked...
				modem_status_reg(3 downto 0) or (
					-- ...or inputs changed
					modem_status_reg(7 downto 4) xor
					prev_modem_status(7 downto 4)
				);

			-- RI rising edge
			modem_status_reg(2) <=
				-- already marked...
				modem_status_reg(2) or
				-- ...or rising edge
				(modem_status_reg(6) and not prev_modem_status(6));


			-- =========
			-- registers
			-- =========
			if write = '1' then
				case address is
					when "000" =>
						if DLAB = '1' then
							-- LSB of divisor latch
							divisor_latch_reg(7 downto 0) <= data_write;
						else
							tx_fifo_data <= data_write;
							tx_fifo_add <= '1';
						end if;

					when "001" =>
						if DLAB = '1' then
							-- MSB of divisor latch
							divisor_latch_reg(15 downto 8) <= data_write;
						else
							interrupt_enable_reg <= data_write;
						end if;

					when "010" =>
						-- only write FCR when FCR0 = 1
						fifo_ctrl_reg(0) <= data_write(0);

						if fifo_ctrl_reg(0) = '1' then
							fifo_ctrl_reg <= data_write;
						end if;

					when "011" =>
						line_ctrl_reg <= data_write;

					when "100" =>
						modem_ctrl_reg <= data_write;

					when "101" =>
						-- "intended for read operations only"
						-- line_status_reg <= data_write;

					when "110" =>
						-- I guess this is read only too?
						-- modem_status_reg <= data_write;

					when "111" =>
						scratch_reg <= data_write;

					when others =>
				end case;
			end if;

			if read = '1' then
				case address is
					when "000" =>
						if DLAB = '1' then
							-- LSB of divisor latch
							data_read <= divisor_latch_reg(7 downto 0);
						else
							data_read <= rx_buffer.data;
							rx_fifo_remove <= '1';
						end if;

					when "001" =>
						if DLAB = '1' then
							-- MSB of divisor latch
							data_read <= divisor_latch_reg(15 downto 8);
						else
							data_read <= interrupt_enable_reg;
						end if;

					when "010" =>
						data_read <= interrupt_identification_reg;

					when "011" =>
						data_read <= line_ctrl_reg;

					when "100" =>
						data_read <= modem_ctrl_reg;

					when "101" =>
						data_read <= line_status_reg;
						-- reset error fields
						line_status_reg(1) <= '0';
						mask_rx_character_errors <= '1';

					when "110" =>
						data_read <= modem_status_reg;
						-- reset delta fields
						modem_status_reg(3 downto 0) <= "0000";
						prev_modem_status <= modem_status_reg;

					when "111" =>
						data_read <= scratch_reg;

					when others =>
						data_read <= (others => 'X');
				end case;
			end if;
		end if;
	end process;

	-- run TX modem when ready and data is available
	tx_ctrl: process(rclk)
	begin
		if rising_edge(rclk) then
			modem_tx_send <= '0';

			if modem_tx_busy = '0' and THRE = '0' then
				modem_tx_send <= '1';
			end if;
		end if;
	end process;


	-- ==========
	-- interrupts
	-- ==========

	interrupt_fire: process(bus_clock)
	begin
		if rising_edge(bus_clock) then
			interrupt <= not interrupt_identification_reg(0);
		end if;
	end process;

	interrupts_pending <=
		-- duplicate "data available" enable bit for timeout interrupt
		(interrupt_enable_reg(0) & interrupt_enable_reg(3 downto 0)) and (
			rx_timeout_triggered &
			to_std_logic(modem_status_reg(3 downto 0) /= "0000") &
			to_std_logic(line_status_reg(4 downto 1) /= "0000") &
			THRE &
			rx_data_interrupt
		);

	interrupt_identification_reg(3 downto 0) <=
		"0110" when interrupts_pending(2) else -- line status error
		"1100" when interrupts_pending(4) else -- timeout
		"0100" when interrupts_pending(0) else -- data available
		"0010" when interrupts_pending(1) else -- THRE
		"0000" when interrupts_pending(3) else -- modem status changed
		"0001";
	-- bit 7/6 set to FIFO enable
	interrupt_identification_reg(7 downto 6) <= (others => fifo_ctrl_reg(0));
	-- bit 5/4 always 0
	interrupt_identification_reg(5 downto 4) <= (others => '0');

	-- FIFO trigger level when in FIFO mode, otherwise plain data available
	rx_data_interrupt <=
		to_std_logic(rx_fifo_elements >= rx_threshold) when fifo_ctrl_reg(0) else
		not rx_fifo_empty;

	-- calculate number of ticks needed for RX timeout
	rx_timeout_calc: process(bus_clock)
		-- number of bits needed for one character, excluding stop bits
		variable char_bits : integer;
	begin
		if rising_edge(bus_clock) then
			-- start bit, data
			char_bits := 1 + num_data_bits;
			if parity /= NONE then
				char_bits := char_bits + 1;
			end if;

			-- 4 characters, 16 cycles per bit
			rx_timeout_ticks <= 4 * (16 * char_bits + stop_bits_clk_count);
		end if;
	end process;

	rx_timeout_counter: process(rclk, rx_fifo_add, rx_fifo_remove)
	begin
		if rx_fifo_add or rx_fifo_remove then
			-- reset timeout
			reset_rx_timeout <= '1';
		elsif rising_edge(rclk) then
			reset_rx_timeout <= '0';

			if reset_rx_timeout then
				rx_timeout_left <= rx_timeout_ticks;
			elsif rx_timeout_left > 0 then
				rx_timeout_left <= rx_timeout_left - 1;
			end if;

			rx_timeout_triggered <= '0';

			if rx_timeout_left = 0 then
				-- trigger timeout when FIFO is active and filled
				rx_timeout_triggered <= fifo_ctrl_reg(0) and not rx_fifo_empty;
			end if;
		end if;
	end process;


	-- ========
	-- plumbing
	-- ========

	stop_bits_clk_count <= STOP_CYCLES_ONE when line_ctrl_reg(2) = '0' else
	                       STOP_CYCLES_ONEANDHALF when line_ctrl_reg(1 downto 0) = "00" else
	                       STOP_CYCLES_TWO;

	with line_ctrl_reg(1 downto 0) select num_data_bits <=
		5 when "00",
		6 when "01",
		7 when "10",
		8 when "11",
		8 when others;

	with fifo_ctrl_reg(7 downto 6) select rx_threshold <=
		1 when "00",
		4 when "01",
		8 when "10",
		14 when "11",
		1 when others;

	parity <= NONE when line_ctrl_reg(3) = '0' else
			  ODD when line_ctrl_reg(5 downto 4) = "00" else
			  EVEN when line_ctrl_reg(5 downto 4) = "01" else
			  FORCE_1 when line_ctrl_reg(5 downto 4) = "10" else
			  FORCE_0 when line_ctrl_reg(5 downto 4) = "11" else
			  NONE;

	rx_fifo_reset <= reset or fifo_ctrl_reg(1);
	tx_fifo_reset <= reset or fifo_ctrl_reg(2);

	-- Data Ready
	line_status_reg(0) <= not rx_fifo_empty;

	-- LSR1 (overflow) is set synchronously

	-- Receive errors
	line_status_reg(2) <= rx_buffer.err_parity  and not mask_rx_character_errors;
	line_status_reg(3) <= rx_buffer.err_framing and not mask_rx_character_errors;
	line_status_reg(4) <= rx_buffer.err_break   and not mask_rx_character_errors;

	-- LSR5 is THRE (set directly by FIFO)

	-- Transmitter Empty
	line_status_reg(6) <= THRE and not modem_tx_busy;


	-- ================
	-- Loopback control
	-- ================

	-- internal rx <= internal tx / external rx
	modem_rx            <= modem_tx when loopback else rx;
	-- reg CTS <= reg RTS / not n_CTS
	modem_status_reg(4) <= modem_ctrl_reg(1) when loopback else not n_cts;
	-- reg DSR <= reg DTR / not n_DSR
	modem_status_reg(5) <= modem_ctrl_reg(0) when loopback else not n_dsr;
	-- reg RI  <= reg OUT1 / not n_RI
	modem_status_reg(6) <= modem_ctrl_reg(2) when loopback else not n_ri;
	-- reg DCD <= reg OUT2 / not n_DCD
	modem_status_reg(7) <= modem_ctrl_reg(3) when loopback else not n_dcd;

	tx     <= '1' when loopback else modem_tx;
	n_dtr  <= '1' when loopback else modem_ctrl_reg(0);
	n_rts  <= '1' when loopback else modem_ctrl_reg(1);
	n_out1 <= '1' when loopback else modem_ctrl_reg(2);
	n_out2 <= '1' when loopback else modem_ctrl_reg(3);
end behaviour;
