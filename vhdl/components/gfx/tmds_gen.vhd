library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity tmds_gen is
	port (
		clk     : in std_logic;

		blank   : in std_logic;
		control : in std_logic_vector(1 downto 0);

		data_in     : in std_logic_vector(7 downto 0);
		encoded_out : out std_logic_vector(9 downto 0)
	);
end tmds_gen;

architecture behaviour of tmds_gen is
	signal data_xored  : std_logic_vector(7 downto 0);
	signal data_xnored : std_logic_vector(7 downto 0);

	-- the number of bit flips using XOR would cause
	signal xor_flips : natural range 0 to 7;

	signal data_transformed : std_logic_vector(7 downto 0);
	signal use_xor : std_logic;

	signal current_bias : integer range -5 to 5;
	signal tf_data_bias : integer range -5 to 5;

	function count_ones(vec : std_logic_vector) return natural is
		variable temp : natural := 0;
	begin
		for i in vec'range loop
			if vec(i) then
				temp := temp + 1;
			end if;
		end loop;

		return temp;
	end count_ones;
begin
	transform: process(data_in)
		variable var_xored  : std_logic_vector(7 downto 0);
		variable var_xnored : std_logic_vector(7 downto 0);
	begin
		var_xored(0) := data_in(0);
		for i in 1 to 7 loop
			var_xored(i) := var_xored(i-1) xor data_in(i);
		end loop;
		data_xored <= var_xored;

		var_xnored(0) := data_in(0);
		for i in 1 to 7 loop
			var_xnored(i) := var_xnored(i-1) xnor data_in(i);
		end loop;
		data_xnored <= var_xnored;
	end process;

	-- XOR flips the output every time a 1 is encountered in the input, we
	-- use this to decide which transformation has the least transitions
	xor_flips <= count_ones(data_in(7 downto 1));

	tf_choose: process(data_xored, data_xnored, xor_flips)
	begin
		if xor_flips <= 3 then
			use_xor <= '1';
			data_transformed <= data_xored;
		else
			use_xor <= '0';
			data_transformed <= data_xnored;
		end if;
	end process;

	-- 4 ones would be balanced
	tf_data_bias <= count_ones(data_transformed) - 4;

	stage_two: process(clk)
	begin
		if rising_edge(clk) then
			if blank then
				current_bias <= 0;
				case control is
					when "00" =>
						encoded_out <= "1101010100";
					when "01" =>
						encoded_out <= "0010101011";
					when "10" =>
						encoded_out <= "0101010100";
					when "11" =>
						encoded_out <= "1010101011";
					when others =>
						report "Invalid control input" severity error;
						encoded_out <= "1101010100";
				end case;
			elsif (current_bias > 0) = (tf_data_bias > 0) then
				-- would increase bias even further, so invert it
				encoded_out <= '1' & use_xor & not data_transformed;

				-- subtract the data's bias (it's inverted)
				if use_xor then
					-- invert and xor bit both set -> an extra one
					current_bias <= current_bias - tf_data_bias + 1;
				else
					current_bias <= current_bias - tf_data_bias;
				end if;
			else
				-- keep data as is, it will make the bias better
				encoded_out <= '0' & use_xor & data_transformed;

				if not use_xor then
					-- invert and xor bits both unset -> a missing one
					current_bias <= current_bias + tf_data_bias - 1;
				else
					current_bias <= current_bias + tf_data_bias;
				end if;
			end if;
		end if;
	end process;
end behaviour;
