library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity vga is
	generic (
		PREFETCH_LENGTH : integer;
		-- horizontal timing (number of pixels)
		H_FRONT : integer;
		H_SYNC  : integer;
		H_BACK  : integer;
		H_DATA  : integer;
		-- vertical timing (number of pixels)
		V_FRONT : integer;
		V_SYNC  : integer;
		V_BACK  : integer;
		V_DATA  : integer
	);
	port (
		-- 25.175MHz for 640x480@60
		pixel_clk : in std_logic;
		reset     : in std_logic;

		-- prefetched positions
		pref_img_row, pref_img_col      : out unsigned(9 downto 0);

		-- row and column of the next pixel
		next_img_row, next_img_col      : out unsigned(9 downto 0);

		hsync : out std_logic;
		vsync : out std_logic;
		blank : out std_logic
	);
end vga;

architecture behaviour of vga is
	constant TOTAL_COLUMNS : integer := H_FRONT+H_SYNC+H_BACK+H_DATA;
	constant TOTAL_ROWS    : integer := V_FRONT+V_SYNC+V_BACK+V_DATA;

	type fifo_entry_t is record
		row, column : integer;
		img_row, img_column : unsigned(9 downto 0);
	end record;

	type fifo_t is array(0 to PREFETCH_LENGTH) of fifo_entry_t;

	-- prefetch FIFO, moves toward PREFETCH_LENGTH
	signal fifo : fifo_t := (others => (
		0,
		0,
		(others => '0'),
		(others => '0'))
	);

	-- the current pixel, output to prefetch ports
	alias curr : fifo_entry_t is fifo(0);
	-- the pixel at the end of the FIFO, used for vsync/hsync and blank
	alias last : fifo_entry_t is fifo(PREFETCH_LENGTH);

	function is_blank(entry : fifo_entry_t) return boolean is
	begin
		return (entry.row < V_FRONT+V_SYNC+V_BACK) or (entry.column < H_FRONT+H_SYNC+H_BACK);
	end function;
begin
	counter: process(reset, pixel_clk)
		begin
			if rising_edge(pixel_clk) then
				-- advance FIFO by one toward PREFETCH_LENGTH
				fifo(1 to PREFETCH_LENGTH) <= fifo(0 to PREFETCH_LENGTH-1);

				if curr.column = TOTAL_COLUMNS - 1 then
					curr.column <= 0;
					if curr.row = TOTAL_ROWS - 1 then
						curr.row <= 0;
					else
						curr.row <= curr.row + 1;
					end if;
				else
					curr.column <= curr.column + 1;
				end if;
			end if;
		end process;

	curr.img_row <=
		(others => '0') when is_blank(curr)
		else to_unsigned(curr.row - (V_FRONT+V_SYNC+V_BACK), curr.img_row'length);
	curr.img_column <=
		(others => '0') when is_blank(curr)
		else to_unsigned(curr.column - (H_FRONT+H_SYNC+H_BACK), curr.img_column'length);

	pref_img_row <= curr.img_row;
	pref_img_col <= curr.img_column;

	next_img_row <= fifo(PREFETCH_LENGTH-1).img_row;
	next_img_col <= fifo(PREFETCH_LENGTH-1).img_column;


	vsync <= '1' when V_FRONT <= last.row    and last.row    < V_FRONT+V_SYNC else '0';
	hsync <= '1' when H_FRONT <= last.column and last.column < H_FRONT+H_SYNC else '0';

	blank <= '1' when is_blank(last) else '0';
end behaviour;
