library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity gfx_module is
	generic (
		-- horizontal timing (number of pixels)
		H_FRONT : integer;
		H_SYNC  : integer;
		H_BACK  : integer;
		H_DATA  : integer;
		-- vertical timing (number of pixels)
		V_FRONT : integer;
		V_SYNC  : integer;
		V_BACK  : integer;
		V_DATA  : integer;

		CHAR_WIDTH  : integer;
		CHAR_HEIGHT : integer
	);
	port (
		-- 25.175MHz for 640x480@60
		clk_pixel : in std_logic;
		-- 10x pixel clock
		clk_tmds  : in std_logic;
		reset     : in std_logic;

		-- connection to text buffer RAM
		-- 80 * 30 = 2400 => 2^12 = 4096
		tram_addr : out std_logic_vector(11 downto 0);
		tram_data : in std_logic_vector(15 downto 0);

		hsync : out std_logic;
		vsync : out std_logic;
		blank : out std_logic;

		analog_red   : out std_logic_vector(7 downto 0);
		analog_green : out std_logic_vector(7 downto 0);
		analog_blue  : out std_logic_vector(7 downto 0);

		dvi_red   : out std_logic;
		dvi_green : out std_logic;
		dvi_blue  : out std_logic;
		dvi_clk   : out std_logic
	);
end gfx_module;

architecture rtl of gfx_module is
	signal pref_img_row : unsigned(9 downto 0);
	signal pref_img_col : unsigned(9 downto 0);

	signal next_img_row : unsigned(9 downto 0);
	signal next_img_col : unsigned(9 downto 0);

	signal textr_red   : std_logic_vector(7 downto 0);
	signal textr_green : std_logic_vector(7 downto 0);
	signal textr_blue  : std_logic_vector(7 downto 0);
begin
	vga_gen: entity work.vga
		generic map (
			PREFETCH_LENGTH => CHAR_WIDTH,
			H_FRONT => H_FRONT,
			H_SYNC  => H_SYNC,
			H_BACK  => H_BACK,
			H_DATA  => H_DATA,
			V_FRONT => V_FRONT,
			V_SYNC  => V_SYNC,
			V_BACK  => V_BACK,
			V_DATA  => V_DATA
		)
		port map (
			pixel_clk => clk_pixel,
			reset     => reset,

			next_img_row => next_img_row,
			next_img_col => next_img_col,

			pref_img_row => pref_img_row,
			pref_img_col => pref_img_col,

			hsync => hsync,
			vsync => vsync,
			blank => blank
		);

	text_render: entity work.text_render
		generic map (
			SCREEN_WIDTH  => H_DATA,
			SCREEN_HEIGHT => V_DATA,
			CHAR_WIDTH    => CHAR_WIDTH,
			CHAR_HEIGHT   => CHAR_HEIGHT
		)
		port map (
			clk => clk_pixel,

			tram_addr => tram_addr,
			tram_data => tram_data,

			pref_row => pref_img_row,
			pref_col => pref_img_col,

			next_row => next_img_row,
			next_col => next_img_col,

			red   => textr_red,
			green => textr_green,
			blue  => textr_blue
		);

	dvi_inst: entity work.dvi_d
		port map (
			clk_pixel => clk_pixel,
			clk_tmds  => clk_tmds,

			px_red   => textr_red,
			px_green => textr_green,
			px_blue  => textr_blue,

			vsync => vsync,
			hsync => hsync,
			blank => blank,

			dvi_red   => dvi_red,
			dvi_green => dvi_green,
			dvi_blue  => dvi_blue,
			dvi_clk   => dvi_clk
		);

	analog_red   <= (others => '0') when blank else textr_red;
	analog_green <= (others => '0') when blank else textr_green;
	analog_blue  <= (others => '0') when blank else textr_blue;
end rtl;
