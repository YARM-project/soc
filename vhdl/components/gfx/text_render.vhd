library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity text_render is
	generic (
		SCREEN_WIDTH  : integer;
		SCREEN_HEIGHT : integer;
		CHAR_WIDTH    : integer;
		CHAR_HEIGHT   : integer
	);
	port (
		clk : in std_logic;

		-- connection to text buffer RAM
		-- 80 * 30 = 2400 => 2^12 = 4096
		tram_addr : out std_logic_vector(11 downto 0);
		tram_data : in std_logic_vector(15 downto 0);

		-- connection to VGA driver
		-- prefetch coordinates (8 ahead)
		pref_row, pref_col : in unsigned(9 downto 0);
		-- row and column of next pixel
		next_row, next_col : in unsigned(9 downto 0);
		red, green, blue   : out std_logic_vector(7 downto 0)
	);
end text_render;

architecture behaviour of text_render is
	constant TEXT_ROWS : integer := SCREEN_HEIGHT / CHAR_HEIGHT;
	constant TEXT_COLS : integer := SCREEN_WIDTH  / CHAR_WIDTH;

	type colors_t is array(0 to 15) of std_logic_vector(23 downto 0);
	constant COLORS : colors_t := (
		x"000000", -- 0 => black
		x"0000AA", -- 1 => blue
		x"00AA00", -- 2 => green
		x"00AAAA", -- 3 => cyan
		x"AA0000", -- 4 => red
		x"AA00AA", -- 5 => magenta
		x"AA5500", -- 6 => brown
		x"AAAAAA", -- 7 => gray
		x"555555", -- 8 => dark gray
		x"5555FF", -- 9 => bright blue
		x"55FF55", -- 10 => bright green
		x"55FFFF", -- 11 => bright cyan
		x"FF5555", -- 12 => bright red
		x"FF55FF", -- 13 => bright magenta
		x"FFFF55", -- 14 => yellow
		x"FFFFFF"  -- 15 => white
	);

	signal color_fg : std_logic_vector(23 downto 0);
	signal color_bg : std_logic_vector(23 downto 0);

	-- connection to font ROM
	-- 16 (char height) * 256 (char num) = 2^12
	signal font_addr : std_logic_vector(11 downto 0) := (others => '0');
	signal font_data : std_logic_vector(7 downto 0);

	-- the next pixel column in the current character (0-7)
	alias curr_char_next_col : unsigned(2 downto 0) is next_col(2 downto 0);

	-- the pixel column in the prefetched character (0-7)
	alias next_char_col : unsigned(2 downto 0) is pref_col(2 downto 0);
	-- the pixel row in the prefetched character (0-15)
	alias next_char_row : unsigned(3 downto 0) is pref_row(3 downto 0);
	-- the text column of the next character (0-79)
	alias next_text_col : unsigned(6 downto 0) is pref_col(9 downto 3);
	-- the text row of the next character (0-29)
	alias next_text_row : unsigned(5 downto 0) is pref_row(9 downto 4);
begin
	font_rom: entity work.font_rom
		generic map (
			CHAR_WIDTH  => CHAR_WIDTH,
			CHAR_HEIGHT => CHAR_HEIGHT
		)
		port map (
			clk => clk,
			address => font_addr,
			data => font_data
		);

	prefetch: process(clk)
		begin
			if rising_edge(clk) then
				if next_char_col = "000" then
					-- fetch next character
					-- TRAM address = text_row * COLUMNS + text_col

					-- need to expand text_row to tram_addr size, otherwise
					-- multiplication is truncated
					tram_addr <= std_logic_vector(resize(
							resize(next_text_row, tram_addr'length) * TEXT_COLS + next_text_col,
							tram_addr'length
						));
				elsif next_char_col = "101" then
					-- new font row needs to be available on last col (7) of current char
					-- thus font ROM must latch output on col 6
					-- thus ROM address has to be latched on col 5

					-- font ROM address = char[6:0] * CHAR_HEIGHT + char_row
					font_addr <= std_logic_vector(resize(
							resize(
								unsigned(tram_data), font_addr'length
							) * CHAR_HEIGHT + next_char_row,
							font_addr'length
						));
				elsif next_char_col = "110" then
					-- foreground color: low nibble of attribute byte
					-- background color: high nibble
					color_fg <= COLORS(to_integer(unsigned(tram_data(11 downto 8))));
					color_bg <= COLORS(to_integer(unsigned(tram_data(15 downto 12))));
				end if;
			end if;
		end process;

	render: process(clk)
	begin
		if rising_edge(clk) then
			-- font data is 7 downto 0, pixels go from 0 to 7, so needs to be inverted
			-- one column ahead due to latching
			if font_data(7 - to_integer(curr_char_next_col)) then
				red   <= color_fg(23 downto 16);
				green <= color_fg(15 downto 8);
				blue  <= color_fg(7 downto 0);
			else
				red   <= color_bg(23 downto 16);
				green <= color_bg(15 downto 8);
				blue  <= color_bg(7 downto 0);
			end if;
		end if;
	end process;
end behaviour;
