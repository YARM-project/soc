library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity dvi_d is
	port (
		clk_pixel : in std_logic;
		-- 10x pixel clock
		clk_tmds  : in std_logic;

		px_red   : in std_logic_vector(7 downto 0);
		px_green : in std_logic_vector(7 downto 0);
		px_blue  : in std_logic_vector(7 downto 0);

		vsync : in std_logic;
		hsync : in std_logic;
		blank : in std_logic;

		dvi_red   : out std_logic;
		dvi_green : out std_logic;
		dvi_blue  : out std_logic;
		dvi_clk   : out std_logic
	);
end dvi_d;

architecture behaviour of dvi_d is
	signal control : std_logic_vector(1 downto 0);

	signal tmds_red   : std_logic_vector(9 downto 0);
	signal tmds_green : std_logic_vector(9 downto 0);
	signal tmds_blue  : std_logic_vector(9 downto 0);

	signal latched_red   : std_logic_vector(9 downto 0);
	signal latched_green : std_logic_vector(9 downto 0);
	signal latched_blue  : std_logic_vector(9 downto 0);
	signal latched_clock : std_logic_vector(9 downto 0);

	signal tmds_count : natural range 0 to 9 := 0;
begin
	tmds_gen_red: entity work.tmds_gen
		port map (
			clk     => clk_pixel,

			blank   => blank,
			control => "00",

			data_in     => px_red,
			encoded_out => tmds_red
		);

	tmds_gen_green: entity work.tmds_gen
		port map (
			clk     => clk_pixel,

			blank   => blank,
			control => "00",

			data_in     => px_green,
			encoded_out => tmds_green
		);

	tmds_gen_blue: entity work.tmds_gen
		port map (
			clk     => clk_pixel,

			blank   => blank,
			control => control,

			data_in     => px_blue,
			encoded_out => tmds_blue
		);

	control <= vsync & hsync;

	output: process(clk_tmds)
	begin
		if rising_edge(clk_tmds) then
			dvi_red   <= latched_red(0);
			dvi_green <= latched_green(0);
			dvi_blue  <= latched_blue(0);
			dvi_clk   <= latched_clock(0);

			if tmds_count = 9 then
				latched_red   <= tmds_red;
				latched_green <= tmds_green;
				latched_blue  <= tmds_blue;
				latched_clock <= "0000011111";
				tmds_count <= 0;
			else
				latched_red   <= '0' & latched_red(9 downto 1);
				latched_green <= '0' & latched_green(9 downto 1);
				latched_blue  <= '0' & latched_blue(9 downto 1);
				latched_clock <= '0' & latched_clock(9 downto 1);
				tmds_count <= tmds_count + 1;
			end if;
		end if;
	end process;
end behaviour;
