-- wrapper for symmetric SDP RAM to allow write-wider-than-read access

library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

entity sdp_asymm_wrapper is
	generic (
		ADDR_WIDTH_READ  : positive := 11;
		DATA_WIDTH_READ  : positive := 16;

		ADDR_WIDTH_WRITE : positive := 10;
		DATA_WIDTH_WRITE : positive := 32;

		COL_WIDTH : positive := 8;

		READ_REGISTER : boolean := false
	);
	port (
		clk_read  : in  std_logic;
		read      : in  std_logic;
		addr_read : in  std_logic_vector(ADDR_WIDTH_READ - 1 downto 0);
		data_read : out std_logic_vector(DATA_WIDTH_READ - 1 downto 0);

		clk_write  : in  std_logic;
		write      : in  std_logic;
		byte_en    : in  std_logic_vector(DATA_WIDTH_WRITE/COL_WIDTH - 1 downto 0);
		addr_write : in  std_logic_vector(ADDR_WIDTH_WRITE - 1 downto 0);
		data_write : in  std_logic_vector(DATA_WIDTH_WRITE - 1 downto 0)
	);
end sdp_asymm_wrapper;

architecture behavioral of sdp_asymm_wrapper is
	function log2(val : INTEGER) return natural is
		variable res : natural;
	begin
		for i in 0 to 31 loop
			if (val <= (2 ** i)) then
				res := i;
				exit;
			end if;
		end loop;

		return res;
	end function log2;

	constant EXTRA_ADDR_BITS : positive := log2(DATA_WIDTH_READ / COL_WIDTH);

	signal read_offset : unsigned(EXTRA_ADDR_BITS-1 downto 0);

	signal data_read_full : std_logic_vector(DATA_WIDTH_WRITE-1 downto 0);
	signal read_reg : std_logic_vector(DATA_WIDTH_READ-1 downto 0);
begin
	assert DATA_WIDTH_READ mod COL_WIDTH = 0 and
	       DATA_WIDTH_WRITE mod DATA_WIDTH_READ = 0 and
	       DATA_WIDTH_READ * 2 ** (ADDR_WIDTH_READ - ADDR_WIDTH_WRITE) = DATA_WIDTH_WRITE
		report "DATA_WIDTH_WRITE has to be a power-of-two multiple of DATA_WIDTH_READ"
		severity failure;

	symm_ram: entity work.sdp_ram_symm
		generic map (
			ADDR_WIDTH => ADDR_WIDTH_WRITE,
			DATA_WIDTH => DATA_WIDTH_WRITE,
			COL_WIDTH  => COL_WIDTH,
			READ_REGISTER => false
		)
		port map (
			clk_read  => clk_read,
			read      => read,
			addr_read => addr_read(ADDR_WIDTH_READ-1 downto EXTRA_ADDR_BITS),
			data_read => data_read_full,

			clk_write  => clk_write,
			write      => write,
			byte_en    => byte_en,
			addr_write => addr_write,
			data_write => data_write
		);

	process(clk_read)
	begin
		if rising_edge(clk_read) then
			if read then
				read_offset <= unsigned(addr_read(EXTRA_ADDR_BITS-1 downto 0));
			end if;
		end if;
	end process;

	read_reg <= data_read_full(
		(to_integer(read_offset) + 1) * DATA_WIDTH_READ - 1 downto
		to_integer(read_offset) * DATA_WIDTH_READ
	);

	read_reg_gen: if READ_REGISTER generate
		process(clk_read)
		begin
			if rising_edge(clk_read) then
				data_read <= read_reg;
			end if;
		end process;
	else generate
		data_read <= read_reg;
	end generate;
end behavioral;
