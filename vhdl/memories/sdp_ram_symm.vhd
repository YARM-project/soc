-- strictly symmetric simple dual port RAM to allow inferrence in yosys

library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

use work.util_pkg.all;

entity sdp_ram_symm is
	generic (
		ADDR_WIDTH : positive := 12;
		DATA_WIDTH : positive := 32;

		COL_WIDTH : positive := 8;

		READ_REGISTER : boolean := true;

		INIT_FILE : string := ""
	);
	port (
		clk_read  : in  std_logic;
		read      : in  std_logic;
		addr_read : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
		data_read : out std_logic_vector(DATA_WIDTH - 1 downto 0);

		clk_write  : in  std_logic;
		write      : in  std_logic;
		byte_en    : in  std_logic_vector(DATA_WIDTH/COL_WIDTH - 1 downto 0);
		addr_write : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
		data_write : in  std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end sdp_ram_symm;

architecture behavioral of sdp_ram_symm is
	function log2(val : INTEGER) return natural is
		variable res : natural;
	begin
		for i in 0 to 31 loop
			if (val <= (2 ** i)) then
				res := i;
				exit;
			end if;
		end loop;

		return res;
	end function log2;

	function eq_assert(x : integer; y : integer) return integer is
	begin
		assert x = y;
		return x;
	end function eq_assert;

	constant COLS_PER_PORT   : positive := DATA_WIDTH / COL_WIDTH;
	constant DEPTH           : positive := 2 ** ADDR_WIDTH;

	signal read_reg : std_logic_vector(DATA_WIDTH - 1 downto 0);
begin
	assert DATA_WIDTH mod COL_WIDTH = 0
		report "DATA_WIDTH must be a multiple of COL_WIDTH"
		severity failure;

	process(clk_read, clk_write)
		variable store : slv_mem_t(0 to DEPTH - 1)(DATA_WIDTH - 1 downto 0) :=
			init_memory_from_file(INIT_FILE, DEPTH, DATA_WIDTH);
	begin
		if rising_edge(clk_read) then
			if read then
				read_reg <= store(to_integer(unsigned(addr_read)));
			end if;
		end if;

		if rising_edge(clk_write) then
			for i in 0 to COLS_PER_PORT - 1 loop
				if write and byte_en(i) then
					store(to_integer(unsigned(addr_write)))((i+1) * COL_WIDTH - 1 downto i * COL_WIDTH) :=
						data_write((i+1) * COL_WIDTH - 1 downto i * COL_WIDTH);
				end if;
			end loop;
		end if;
	end process;

	read_reg_gen: if READ_REGISTER generate
		process(clk_read)
		begin
			if rising_edge(clk_read) then
				data_read <= read_reg;
			end if;
		end process;
	else generate
		data_read <= read_reg;
	end generate;
end behavioral;
