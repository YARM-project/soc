-- wrapper around SDP RAM that combines both ports into one clock domain

library ieee;
use ieee.std_logic_1164.all,
	ieee.numeric_std.all;

entity single_port_ram is
	generic (
		ADDR_WIDTH : positive;
		DATA_WIDTH : positive;

		COL_WIDTH : positive;

		READ_REGISTER : boolean := false;

		INIT_FILE : string := ""
	);
	port (
		clk        : in  std_logic;
		read       : in  std_logic;
		write      : in  std_logic;
		byte_en    : in  std_logic_vector(DATA_WIDTH/COL_WIDTH - 1 downto 0);

		addr       : in  std_logic_vector(ADDR_WIDTH - 1 downto 0);
		data_read  : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		data_write : in  std_logic_vector(DATA_WIDTH - 1 downto 0)
	);
end single_port_ram;

architecture rtl of single_port_ram is
begin
	--sdp: entity work.sdp_ram
	--	generic map (
	--		ADDR_WIDTH_READ  => ADDR_WIDTH,
	--		DATA_WIDTH_READ  => DATA_WIDTH,

	--		ADDR_WIDTH_WRITE => ADDR_WIDTH,
	--		DATA_WIDTH_WRITE => DATA_WIDTH,

	--		COL_WIDTH => COL_WIDTH,

	--		READ_REGISTER => READ_REGISTER
	--	)
	--	port map (
	--		clk_read  => clk,
	--		read      => read,
	--		addr_read => addr,
	--		data_read => data_read,

	--		clk_write  => clk,
	--		write      => write,
	--		byte_en    => byte_en,
	--		addr_write => addr,
	--		data_write => data_write
	--	);
	sdp: entity work.sdp_ram_symm
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,
			DATA_WIDTH => DATA_WIDTH,

			COL_WIDTH => COL_WIDTH,

			READ_REGISTER => READ_REGISTER,

			INIT_FILE => INIT_FILE
		)
		port map (
			clk_read  => clk,
			read      => read,
			addr_read => addr,
			data_read => data_read,

			clk_write  => clk,
			write      => write,
			byte_en    => byte_en,
			addr_write => addr,
			data_write => data_write
		);
end architecture;
