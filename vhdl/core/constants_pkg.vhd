library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all;

package constants_pkg is
	-- 0.0.1
	constant YARM_VERSION : yarm_word := x"0000_0001";

	-- ===========================
	--  INSTRUCTION FIELDS RANGES
	-- ===========================

	constant INST_OPCODE_START : integer := 6;
	constant INST_OPCODE_END   : integer := 2;

	constant INST_RD_START : integer := 11;
	constant INST_RD_END   : integer := 7;
	constant INST_RS1_START : integer := 19;
	constant INST_RS1_END   : integer := 15;
	constant INST_RS2_START : integer := 24;
	constant INST_RS2_END   : integer := 20;

	constant INST_FUNCT3_START : integer := 14;
	constant INST_FUNCT3_END   : integer := 12;
	constant INST_FUNCT7_START : integer := 31;
	constant INST_FUNCT7_END   : integer := 25;
	constant INST_FUNCT12_START : integer := 31;
	constant INST_FUNCT12_END   : integer := 20;

	constant INST_IMM_I_START : integer := 31;
	constant INST_IMM_I_END   : integer := 20;

	constant INST_IMM_S1_START : integer := 31;
	constant INST_IMM_S1_END   : integer := 25;
	constant INST_IMM_S2_START : integer := 11;
	constant INST_IMM_S2_END   : integer := 7;

	constant INST_IMM_U_START : integer := 31;
	constant INST_IMM_U_END   : integer := 12;

	-- =========
	--  OPCODES
	-- =========

	constant OPCODE_LOAD     : std_logic_vector(4 downto 0) := b"00_000";
	constant OPCODE_MISC_MEM : std_logic_vector(4 downto 0) := b"00_011";
	constant OPCODE_OP_IMM   : std_logic_vector(4 downto 0) := b"00_100";
	constant OPCODE_AUIPC    : std_logic_vector(4 downto 0) := b"00_101";
	constant OPCODE_STORE    : std_logic_vector(4 downto 0) := b"01_000";
	constant OPCODE_OP       : std_logic_vector(4 downto 0) := b"01_100";
	constant OPCODE_LUI      : std_logic_vector(4 downto 0) := b"01_101";
	constant OPCODE_BRANCH   : std_logic_vector(4 downto 0) := b"11_000";
	constant OPCODE_JALR     : std_logic_vector(4 downto 0) := b"11_001";
	constant OPCODE_JAL      : std_logic_vector(4 downto 0) := b"11_011";
	constant OPCODE_SYSTEM   : std_logic_vector(4 downto 0) := b"11_100";

	-- OP/OP_IMM

	constant F3_ADDSUB : std_logic_vector(2 downto 0) := "000";
	constant F3_SHIFTL : std_logic_vector(2 downto 0) := "001";
	constant F3_SLT    : std_logic_vector(2 downto 0) := "010";
	constant F3_SLTU   : std_logic_vector(2 downto 0) := "011";
	constant F3_XOR    : std_logic_vector(2 downto 0) := "100";
	constant F3_SHIFTR : std_logic_vector(2 downto 0) := "101";
	constant F3_OR     : std_logic_vector(2 downto 0) := "110";
	constant F3_AND    : std_logic_vector(2 downto 0) := "111";

	constant F3_BEQ  : std_logic_vector(2 downto 0) := "000";
	constant F3_BNE  : std_logic_vector(2 downto 0) := "001";
	constant F3_BLT  : std_logic_vector(2 downto 0) := "100";
	constant F3_BGE  : std_logic_vector(2 downto 0) := "101";
	constant F3_BLTU : std_logic_vector(2 downto 0) := "110";
	constant F3_BGEU : std_logic_vector(2 downto 0) := "111";

	constant F7_OP_DEFAULT : std_logic_vector(6 downto 0) := b"000_0000";
	constant F7_OP_SPECIAL : std_logic_vector(6 downto 0) := b"010_0000";
	constant F7_SRL : std_logic_vector(6 downto 0) := F7_OP_DEFAULT;
	constant F7_SRA : std_logic_vector(6 downto 0) := F7_OP_SPECIAL;
	constant F7_ADD : std_logic_vector(6 downto 0) := F7_OP_DEFAULT;
	constant F7_SUB : std_logic_vector(6 downto 0) := F7_OP_SPECIAL;

	-- M extension

	constant F3_MUL    : std_logic_vector(2 downto 0) := "000";
	constant F3_MULH   : std_logic_vector(2 downto 0) := "001";
	constant F3_MULHSU : std_logic_vector(2 downto 0) := "010";
	constant F3_MULHU  : std_logic_vector(2 downto 0) := "011";
	constant F3_DIV    : std_logic_vector(2 downto 0) := "100";
	constant F3_DIVU   : std_logic_vector(2 downto 0) := "101";
	constant F3_REM    : std_logic_vector(2 downto 0) := "110";
	constant F3_REMU   : std_logic_vector(2 downto 0) := "111";

	constant F7_MULDIV : std_logic_vector(6 downto 0) := b"000_0001";

	-- SYSTEM opcode

	constant F3_PRIV    : std_logic_vector(2 downto 0) := "000";
	constant F3_CSRRW   : std_logic_vector(2 downto 0) := "001";
	constant F3_CSRRS   : std_logic_vector(2 downto 0) := "010";
	constant F3_CSRRC   : std_logic_vector(2 downto 0) := "011";
	constant F3_CSRRWI  : std_logic_vector(2 downto 0) := "101";
	constant F3_CSRRSI  : std_logic_vector(2 downto 0) := "110";
	constant F3_CSRRCI  : std_logic_vector(2 downto 0) := "111";

	constant F12_ECALL  : std_logic_vector(11 downto 0) := x"000";
	constant F12_EBREAK : std_logic_vector(11 downto 0) := x"001";

	constant F12_URET   : std_logic_vector(11 downto 0) := x"002";
	constant F12_SRET   : std_logic_vector(11 downto 0) := x"102";
	constant F12_MRET   : std_logic_vector(11 downto 0) := x"302";
	constant F12_WFI    : std_logic_vector(11 downto 0) := x"105";

	-- MISC-MEM

	constant F3_FENCE   : std_logic_vector(2 downto 0) := "000";
	constant F3_FENCE_I : std_logic_vector(2 downto 0) := "001";

	-- ===============
	--  CSR ADDRESSES
	-- ===============

	-- machine CSRs
	-- info
	constant CSRADDR_MVENDORID : csr_addr_t := x"F11";
	constant CSRADDR_MARCHID   : csr_addr_t := x"F12";
	constant CSRADDR_MIMPID    : csr_addr_t := x"F13";
	constant CSRADDR_MHARTID   : csr_addr_t := x"F14";
	-- trap setup
	constant CSRADDR_MSTATUS    : csr_addr_t := x"300";
	constant CSRADDR_MISA       : csr_addr_t := x"301";
	constant CSRADDR_MEDELEG    : csr_addr_t := x"302";
	constant CSRADDR_MIDELEG    : csr_addr_t := x"303";
	constant CSRADDR_MIE        : csr_addr_t := x"304";
	constant CSRADDR_MTVEC      : csr_addr_t := x"305";
	constant CSRADDR_MCOUNTEREN : csr_addr_t := x"306";
	-- trap handling
	constant CSRADDR_MSCRATCH : csr_addr_t := x"340";
	constant CSRADDR_MEPC     : csr_addr_t := x"341";
	constant CSRADDR_MCAUSE   : csr_addr_t := x"342";
	constant CSRADDR_MTVAL    : csr_addr_t := x"343";
	constant CSRADDR_MIP      : csr_addr_t := x"344";
	-- counters/timers
	constant CSRADDR_MCYCLE    : csr_addr_t := x"B00";
	constant CSRADDR_MCYCLEH   : csr_addr_t := x"B80";
	constant CSRADDR_MINSTRET  : csr_addr_t := x"B02";
	constant CSRADDR_MINSTRETH : csr_addr_t := x"B82";
	constant CSRADDR_MHPMCOUNTER_START  : csr_addr_t := x"B00";
	constant CSRADDR_MHPMCOUNTER_END    : csr_addr_t := x"B1F";
	constant CSRADDR_MHPMCOUNTERH_START : csr_addr_t := x"B80";
	constant CSRADDR_MHPMCOUNTERH_END   : csr_addr_t := x"B9F";
	-- counter setup
	constant CSRADDR_MHPMEVENT_START : csr_addr_t := x"320";
	constant CSRADDR_MHPMEVENT_END   : csr_addr_t := x"33F";

	-- ===========================
	--  INTERRUPT/EXCEPTION CODES
	-- ===========================

	-- "interrupt" bit mcause(31)
	constant INTERRUPT_TRAP_CAUSE_MASK : yarm_trap_cause := x"8000_0000";

	constant INTERRUPT_SOFT_U  : yarm_trap_cause := x"0000_0000";
	constant INTERRUPT_SOFT_S  : yarm_trap_cause := x"0000_0001";
	constant INTERRUPT_SOFT_M  : yarm_trap_cause := x"0000_0003";

	constant INTERRUPT_TIMER_U : yarm_trap_cause := x"0000_0004";
	constant INTERRUPT_TIMER_S : yarm_trap_cause := x"0000_0005";
	constant INTERRUPT_TIMER_M : yarm_trap_cause := x"0000_0007";

	constant INTERRUPT_EXT_U   : yarm_trap_cause := x"0000_0008";
	constant INTERRUPT_EXT_S   : yarm_trap_cause := x"0000_0009";
	constant INTERRUPT_EXT_M   : yarm_trap_cause := x"0000_000b";

	type interrupt_prio_t is array(0 to 8) of yarm_trap_cause;
	constant INTERRUPT_PRIORITIES : interrupt_prio_t := (
		INTERRUPT_TIMER_U, INTERRUPT_SOFT_U, INTERRUPT_EXT_U,
		INTERRUPT_TIMER_S, INTERRUPT_SOFT_S, INTERRUPT_EXT_S,
		INTERRUPT_TIMER_M, INTERRUPT_SOFT_M, INTERRUPT_EXT_M
	);

	constant EXCEPTION_INST_MISALIGNED        : yarm_trap_cause := x"0000_0000";
	constant EXCEPTION_INST_ACCESS_FAULT      : yarm_trap_cause := x"0000_0001";
	constant EXCEPTION_ILLEGAL_INST           : yarm_trap_cause := x"0000_0002";
	constant EXCEPTION_BREAKPOINT             : yarm_trap_cause := x"0000_0003";
	constant EXCEPTION_LOAD_MISALIGNED        : yarm_trap_cause := x"0000_0004";
	constant EXCEPTION_LOAD_ACCESS_FAULT      : yarm_trap_cause := x"0000_0005";
	constant EXCEPTION_STORE_AMO_MISALIGNED   : yarm_trap_cause := x"0000_0006";
	constant EXCEPTION_STORE_AMO_ACCESS_FAULT : yarm_trap_cause := x"0000_0007";
	constant EXCEPTION_ENVCALL_U              : yarm_trap_cause := x"0000_0008";
	constant EXCEPTION_ENVCALL_S              : yarm_trap_cause := x"0000_0009";
	constant EXCEPTION_ENVCALL_M              : yarm_trap_cause := x"0000_000b";
	constant EXCEPTION_INST_PAGE_FAULT        : yarm_trap_cause := x"0000_000c";
	constant EXCEPTION_LOAD_PAGE_FAULT        : yarm_trap_cause := x"0000_000d";
	constant EXCEPTION_STORE_AMO_PAGE_FAULT   : yarm_trap_cause := x"0000_000f";
end constants_pkg;
