library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity program_counter is
	port (
		clk       : in std_logic;
		reset     : in std_logic;
		operation : in pc_operation_t;
		pc_in     : in yarm_word;
		pc_out    : out yarm_word
	);
end program_counter;

architecture behavior of program_counter is
	signal current_pc : yarm_word;
begin
	process(clk, reset)
	begin
		if reset = '1' then
			current_pc <= ADDRESS_INITIAL;
		elsif rising_edge(clk) then
			case operation is
				when PC_INCREMENT =>
					current_pc <= std_logic_vector(unsigned(current_pc) + 4);
				when PC_SET =>
					current_pc <= pc_in;
				when PC_FREEZE =>
			end case;
		end if;
	end process;

	pc_out <= current_pc;
end behavior;
