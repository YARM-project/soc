library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity rvc_uncompress is
	port (
		instruction_in : in std_logic_vector(15 downto 0);
		instruction_out : out std_logic_vector(31 downto 0);
		is_error : out std_logic
	);
end rvc_uncompress;

architecture rtl of rvc_uncompress is
	type instruction_format is (CR, CI, CSS, CIW, CL, CS, CA, CB, CJ);
	type instruction_t is (
		ILLEGAL,
		ADDI4SPN, LW, SW,
		ADDI, JAL, LI, ADDI16SP, LUI, SRLI, SRAI, ANDI, SUB, INS_XOR, INS_OR, INS_AND, J, BEQZ, BNEZ,
		SLLI, LWSP, JR, MV, EBREAK, JALR, ADD, SWSP
	);

	signal instruction : instruction_t;

	alias funct2 : std_logic_vector(1 downto 0) is instruction_in(6 downto 5);
	alias funct3 : std_logic_vector(2 downto 0) is instruction_in(15 downto 13);
	alias funct4 : std_logic_vector(3 downto 0) is instruction_in(15 downto 12);
	alias funct6 : std_logic_vector(5 downto 0) is instruction_in(15 downto 10);
	alias opcode : std_logic_vector(1 downto 0) is instruction_in(1 downto 0);

	alias ra : std_logic_vector(4 downto 0) is instruction_in(11 downto 7);
	alias rb : std_logic_vector(4 downto 0) is instruction_in(6 downto 2);

	signal format : instruction_format;
	signal immediate : std_logic_vector(31 downto 0);

	signal is_reserved, is_illegal : std_logic;
begin
	decode_ins: process(all)
		procedure HINT(pred : std_logic) is
		begin
		end procedure;

		procedure RESERVED(pred : std_logic) is
		begin
			if pred then
				is_reserved <= '1';
			end if;
		end procedure;

		variable immediate_zero : std_logic;
		variable ra_zero : std_logic;
	begin
		instruction <= ILLEGAL;
		immediate_zero := not (or immediate);
		ra_zero := not (or ra);

		is_reserved <= '0';

		case opcode is
			when "00" =>
				case funct3 is
					when "000" =>
						-- ADDI4SPN
						instruction <= ADDI4SPN;
						-- RES imm == 0
						RESERVED(immediate_zero);
					when "010" =>
						-- LW
						instruction <= LW;
					when "110" =>
						-- SW
						instruction <= SW;
					when others =>
				end case;
			when "01" =>
				case funct3 is
					when "000" =>
						instruction <= ADDI;
						if (or ra) then
							-- ADDI
							-- HINT imm == 0
							HINT(immediate_zero);
						else
							-- NOP
							-- HINT imm != 0
							HINT(not immediate_zero);
						end if;
					when "001" =>
						-- JAL
						instruction <= JAL;
					when "010" =>
						-- LI
						instruction <= LI;
						-- HINT ra == 0
						HINT(ra_zero);
					when "011" =>
						if ra = "00010" then
							-- ADDI16SP
							instruction <= ADDI16SP;
							-- RES imm == 0
							RESERVED(immediate_zero);
						else
							-- LUI
							instruction <= LUI;
							-- RES imm == 0
							RESERVED(immediate_zero);
							-- HINT ra == 0
							HINT(ra_zero);
						end if;
					when "100" =>
						case instruction_in(11 downto 10) is
							when "00" =>
								-- SRLI
								instruction <= SRLI;
								-- HINT imm == 0
								HINT(immediate_zero);
								-- RES imm[5] == 1
								RESERVED(immediate(5));
							when "01" =>
								-- SRAI
								instruction <= SRAI;
								-- HINT imm == 0
								HINT(immediate_zero);
								-- RES imm[5] == 1
								RESERVED(immediate(5));
							when "10" =>
								-- ANDI
								instruction <= ANDI;
							when "11" =>
								case instruction_in(12) & instruction_in(6 downto 5) is
									when "000" =>
										-- SUB
										instruction <= SUB;
									when "001" =>
										-- XOR
										instruction <= INS_XOR;
									when "010" =>
										-- OR
										instruction <= INS_OR;
									when "011" =>
										-- AND
										instruction <= INS_AND;
									when others =>
								end case;
							when others =>
						end case;
					when "101" =>
						-- J
						instruction <= J;
					when "110" =>
						-- BEQZ
						instruction <= BEQZ;
					when "111" =>
						-- BNEZ
						instruction <= BNEZ;
					when others =>
				end case;
			when "10" =>
				case funct3 is
					when "000" =>
						-- SLLI
						instruction <= SLLI;
						-- HINT ra == 0
						HINT(ra_zero);
						-- HINT imm == 0
						HINT(immediate_zero);
						-- RES imm[5] == 1
						RESERVED(immediate(5));
					when "010" =>
						-- LWSP
						instruction <= LWSP;
						-- RES ra == 0
						RESERVED(ra_zero);
					when "100" =>
						if instruction_in(12) = '0' then
							if (or rb) then
								-- MV
								instruction <= MV;
								-- HINT ra == 0
								HINT(ra_zero);
							else
								-- JR
								instruction <= JR;
								-- RES ra == 0
								RESERVED(ra_zero);
							end if;
						else
							if (or rb) then
								-- ADD
								instruction <= ADD;
								-- HINT ra == 0
								HINT(ra_zero);
							elsif (or ra) then
								-- JALR
								instruction <= JALR;
							else
								-- EBREAK
								instruction <= EBREAK;
							end if;
						end if;
					when "110" =>
						-- SWSP
						instruction <= SWSP;
					when others =>
				end case;
			when others =>
		end case;
	end process;

	is_illegal <= '1' when instruction = ILLEGAL else '0';
	is_error <= is_illegal or is_reserved;

	extract_imm: process(all)
		procedure uimm(x : std_logic_vector) is
		begin
			immediate <= std_logic_vector(resize(unsigned(x), immediate'length));
		end procedure;

		procedure simm(x : std_logic_vector) is
		begin
			immediate <= std_logic_vector(resize(signed(x), immediate'length));
		end procedure;
	begin
		immediate <= (others => 'U');

		case instruction is
			when ILLEGAL =>
			when ADDI4SPN =>
				uimm(
					instruction_in(10 downto 7) & instruction_in(12 downto 11) &
					instruction_in(5) & instruction_in(6) & "00"
				);
			when LW | SW =>
				uimm(
					instruction_in(5) & instruction_in(12 downto 10) &
					instruction_in(6) & "00"
				);
			when ADDI | LI | ANDI =>
				simm(instruction_in(12) & instruction_in(6 downto 2));
			when SRLI | SRAI | SLLI =>
				uimm(instruction_in(12) & instruction_in(6 downto 2));
			when LUI =>
				simm(
					instruction_in(12) & instruction_in(6 downto 2) &
					x"000"
				);
			when JAL | J =>
				simm(
					instruction_in(12) & instruction_in(8) &
					instruction_in(10 downto 9) & instruction_in(6) &
					instruction_in(7) & instruction_in(2) & instruction_in(11) &
					instruction_in(5 downto 3) & "0"
				);
			when ADDI16SP =>
				simm(
					instruction_in(12) & instruction_in(4 downto 3) &
					instruction_in(5) & instruction_in(2) &
					instruction_in(6) & "0000"
				);
			when BEQZ | BNEZ =>
				simm(
					instruction_in(12) & instruction_in(6 downto 5) &
					instruction_in(2) & instruction_in(11 downto 10) &
					instruction_in(4 downto 3) & "0"
				);
			when LWSP =>
				uimm(
					instruction_in(3 downto 2) & instruction_in(12) &
					instruction_in(6 downto 4) & "00"
				);
			when SWSP =>
				uimm(
					instruction_in(8 downto 7) &
					instruction_in(12 downto 9) & "00"
				);
			when JR | JALR =>
				immediate <= (others => '0');
			when EBREAK =>
				immediate <= (0 => '1', others => '0');
			when SUB | INS_XOR | INS_OR | INS_AND | MV | ADD =>
				-- NONE
		end case;
	end process;

	build_ins: process(all)
		procedure build(ins : std_logic_vector(31 downto 2)) is
		begin
			instruction_out <= ins & "11";
		end procedure;

		procedure build_R(
				funct7 : std_logic_vector(6 downto 0);
				rs2 : std_logic_vector(4 downto 0);
				rs1 : std_logic_vector(4 downto 0);
				funct3 : std_logic_vector(2 downto 0);
				rd : std_logic_vector(4 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(funct7 & rs2 & rs1 & funct3 & rd & opcode);
		end procedure;

		procedure build_I(
				rs1 : std_logic_vector(4 downto 0);
				funct3 : std_logic_vector(2 downto 0);
				rd : std_logic_vector(4 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(immediate(11 downto 0) & rs1 & funct3 & rd & opcode);
		end procedure;

		-- specialized I-type
		procedure build_shift(
				arithmetic : std_logic;
				rs1 : std_logic_vector(4 downto 0);
				funct3 : std_logic_vector(2 downto 0);
				rd : std_logic_vector(4 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(
				"0" & arithmetic & "00000" & immediate(4 downto 0) &
				rs1 & funct3 & rd & opcode
			);
		end procedure;

		procedure build_S(
				rs2 : std_logic_vector(4 downto 0);
				rs1 : std_logic_vector(4 downto 0);
				funct3 : std_logic_vector(2 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(immediate(11 downto 5) & rs2 & rs1 & funct3 & immediate(4 downto 0) & opcode);
		end procedure;

		procedure build_B(
				rs2 : std_logic_vector(4 downto 0);
				rs1 : std_logic_vector(4 downto 0);
				funct3 : std_logic_vector(2 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(
				immediate(12) & immediate(10 downto 5) &
				rs2 & rs1 & funct3 &
				immediate(4 downto 1) & immediate(11) &
				opcode
			);
		end procedure;

		procedure build_U(
				rd : std_logic_vector(4 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(immediate(31 downto 12) & rd & opcode);
		end procedure;

		procedure build_J(
				rd : std_logic_vector(4 downto 0);
				opcode : std_logic_vector(4 downto 0)
			) is
		begin
			build(
				immediate(20) & immediate(10 downto 1) &
				immediate(11) & immediate(19 downto 12) &
				rd & opcode
			);
		end procedure;

		variable ra_compressed : std_logic_vector(4 downto 0);
		variable rb_compressed : std_logic_vector(4 downto 0);
	begin
		instruction_out <= (others => 'U');
		ra_compressed := "01" & ra(2 downto 0);
		rb_compressed := "01" & rb(2 downto 0);

		case instruction is
			when ILLEGAL =>
			when ADDI4SPN =>
				build_I("00010", F3_ADDSUB, rb_compressed, OPCODE_OP_IMM);
			when LW =>
				build_I(ra_compressed, "010", rb_compressed, OPCODE_LOAD);
			when SW =>
				build_S(rb_compressed, ra_compressed, "010", OPCODE_STORE);
			when ADDI =>
				build_I(ra, F3_ADDSUB, ra, OPCODE_OP_IMM);
			when JAL =>
				build_J("00001", OPCODE_JAL);
			when LI =>
				build_I("00000", F3_ADDSUB, ra, OPCODE_OP_IMM);
			when ADDI16SP =>
				build_I("00010", F3_ADDSUB, "00010", OPCODE_OP_IMM);
			when LUI =>
				build_U(ra, OPCODE_LUI);
			when SRLI =>
				build_shift('0', ra_compressed, F3_SHIFTR, ra_compressed, OPCODE_OP_IMM);
			when SRAI =>
				build_shift('1', ra_compressed, F3_SHIFTR, ra_compressed, OPCODE_OP_IMM);
			when ANDI =>
				build_I(ra_compressed, F3_AND, ra_compressed, OPCODE_OP_IMM);
			when SUB =>
				build_R(F7_SUB, rb_compressed, ra_compressed, F3_ADDSUB, ra_compressed, OPCODE_OP);
			when INS_XOR =>
				build_R(F7_OP_DEFAULT, rb_compressed, ra_compressed, F3_XOR, ra_compressed, OPCODE_OP);
			when INS_OR =>
				build_R(F7_OP_DEFAULT, rb_compressed, ra_compressed, F3_OR, ra_compressed, OPCODE_OP);
			when INS_AND =>
				build_R(F7_OP_DEFAULT, rb_compressed, ra_compressed, F3_AND, ra_compressed, OPCODE_OP);
			when J =>
				build_J("00000", OPCODE_JAL);
			when BEQZ =>
				build_B("00000", ra_compressed, F3_BEQ, OPCODE_BRANCH);
			when BNEZ =>
				build_B("00000", ra_compressed, F3_BNE, OPCODE_BRANCH);
			when SLLI =>
				build_shift('0', ra, F3_SHIFTL, ra, OPCODE_OP_IMM);
			when LWSP =>
				build_I("00010", "010", ra, OPCODE_LOAD);
			when JR =>
				build_I(ra, "000", "00000", OPCODE_JALR);
			when MV =>
				build_R(F7_ADD, rb, "00000", F3_ADDSUB, ra, OPCODE_OP);
			when EBREAK =>
				build_I("00000", F3_PRIV, "00000", OPCODE_SYSTEM);
			when JALR =>
				build_I(ra, "000", "00001", OPCODE_JALR);
			when ADD =>
				build_R(F7_ADD, rb, ra, F3_ADDSUB, ra, OPCODE_OP);
			when SWSP =>
				build_S(rb, "00010", "010", OPCODE_STORE);
		end case;
	end process;
end rtl;
