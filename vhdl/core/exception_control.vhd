library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity exception_control is
	port (
		clk   : in std_logic;

		-- synchronous exceptions
		decoder_raise_exc : in std_logic;
		decoder_exc_data  : in exception_data_t;

		csr_raise_exc : in std_logic;
		csr_exc_data  : in exception_data_t;

		datamem_raise_exc : in std_logic;
		datamem_exc_data  : in exception_data_t;

		-- interrupts
		global_int_enabled : in std_logic;
		interrupts_enabled : in yarm_word;
		interrupts_pending : in yarm_word;

		-- stage inputs for return address + trap value (instruction)
		stage_inputs      : in pipeline_frames_t;
		interrupted_stage : in pipeline_stage_t;

		may_interrupt : in std_logic;
		do_trap       : out std_logic;
		trap_cause    : out yarm_trap_cause;
		trap_address  : out yarm_word;
		trap_value    : out yarm_word
	);
end exception_control;

architecture rtl of exception_control is
	signal any_interrupt_active : std_logic;
	signal interrupt_cause : yarm_trap_cause;
begin
	calc_interrupts: process(all)
		variable interrupts_active : yarm_word;
		variable int_num : yarm_trap_cause;
	begin
		if rising_edge(clk) then
			interrupts_active := interrupts_enabled and interrupts_pending;
			any_interrupt_active <= or interrupts_active;

			for prio in INTERRUPT_PRIORITIES'range loop
				int_num := INTERRUPT_PRIORITIES(prio);
				if interrupts_active(to_integer(int_num)) then
					interrupt_cause <= INTERRUPT_TRAP_CAUSE_MASK or int_num;
				end if;
			end loop;
		end if;
	end process;

	mux_controls: process(all)
	begin
		do_trap      <= '0';
		trap_cause   <= (others => 'U');
		trap_address <= (others => 'U');
		trap_value <= (others => 'U');

		if may_interrupt and global_int_enabled and any_interrupt_active then
			do_trap      <= '1';
			trap_cause   <= interrupt_cause;
			trap_address <= stage_inputs(interrupted_stage).address;
			trap_value   <= (others => '0');
		elsif csr_raise_exc then
			do_trap      <= '1';
			trap_cause   <= csr_exc_data.cause;
			trap_address <= csr_exc_data.ins_address;
			trap_value   <= csr_exc_data.trap_value;
		elsif decoder_raise_exc then
			do_trap      <= '1';
			trap_cause   <= decoder_exc_data.cause;
			trap_address <= decoder_exc_data.ins_address;
			trap_value   <= decoder_exc_data.trap_value;
		elsif datamem_raise_exc then
			do_trap      <= '1';
			trap_cause   <= datamem_exc_data.cause;
			trap_address <= datamem_exc_data.ins_address;
			trap_value   <= datamem_exc_data.trap_value;
		end if;
	end process;
end rtl;
