library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity decoder is
	port (
		clk    : in std_logic;
		enable : in std_logic;

		async_addr_rs1 : out register_addr_t;
		async_addr_rs2 : out register_addr_t;

		alu_muxsel_a    : out mux_selector_t;
		alu_muxsel_b    : out mux_selector_t;
		alu_muxsel_cmp2 : out mux_selector_t;

		csr_muxsel_in : out mux_selector_t;

		instr_info_in  : in instruction_info_t;
		instr_info_out : out instruction_info_t;

		raise_exc : out std_logic;
		exc_data  : out exception_data_t
	);
end decoder;

architecture rtl of decoder is
	type instruction_format is (
		TYPE_R, TYPE_I, TYPE_S, TYPE_B, TYPE_U, TYPE_J, CSR_IMM
	);

	-- extract immediate value from an instruction, given instruction format
	function imm_extract(
			instruction : yarm_word;
			format : instruction_format
		) return yarm_word is
	begin
		case format is
			when TYPE_R =>
				-- probably never called, but return 0 anyway
				return x"0000_0000";

			when TYPE_I =>
				return std_logic_vector(resize(signed(
						instruction(31 downto 20)
					), 32));

			when TYPE_S =>
				return std_logic_vector(resize(signed(
						instruction(31 downto 25) &
						instruction(11 downto 7)
					), 32));

			when TYPE_B =>
				return std_logic_vector(resize(signed(
						instruction(31) &
						instruction(7) &
						instruction(30 downto 25) &
						instruction(11 downto 8) &
						'0'
					), 32));

			when TYPE_U =>
				return instruction(31 downto 12) & (11 downto 0 => '0');

			when TYPE_J =>
				return std_logic_vector(resize(signed(
						instruction(31) &
						instruction(19 downto 12) &
						instruction(20) &
						instruction(30 downto 21) &
						'0'
					), 32));

			when CSR_IMM =>
				-- CSR immediate in rs1 field
				return std_logic_vector(resize(unsigned(
						instruction(19 downto 15)
					), 32));
		end case;
	end imm_extract;

	alias instruction_raw : yarm_word is instr_info_in.instruction;

	signal uncompress_error : std_logic;
	signal instruction_size : natural range 2 to 4;
	signal instruction_uncompressed : yarm_word;
	signal instruction_masked : yarm_word;
	signal instruction_in : yarm_word;

	alias a_opcode : std_logic_vector(4 downto 0) is
		instruction_in(INST_OPCODE_START downto INST_OPCODE_END);

	alias a_rd     : std_logic_vector(4 downto 0) is
		instruction_in(INST_RD_START downto INST_RD_END);

	alias a_rs1    : std_logic_vector(4 downto 0) is
		instruction_in(INST_RS1_START downto INST_RS1_END);

	alias a_rs2    : std_logic_vector(4 downto 0) is
		instruction_in(INST_RS2_START downto INST_RS2_END);

	alias a_funct3 : std_logic_vector(2 downto 0) is
		instruction_in(INST_FUNCT3_START downto INST_FUNCT3_END);

	alias a_funct7 : std_logic_vector(6 downto 0) is
		instruction_in(INST_FUNCT7_START downto INST_FUNCT7_END);

	-- for SYSTEM instructions
	alias a_funct12 : std_logic_vector(11 downto 0) is
		instruction_in(INST_FUNCT12_START downto INST_FUNCT12_END);
begin
	-- async register select
	async_addr_rs1 <= a_rs1;
	async_addr_rs2 <= a_rs2;

	uncompress: entity work.rvc_uncompress
		port map (
			instruction_in => instruction_raw(15 downto 0),
			instruction_out => instruction_uncompressed,
			is_error => uncompress_error
		);

	process(all)
	begin
		if instruction_raw(1 downto 0) = "11" then
			-- RV32
			instruction_in <= instruction_raw;
			instruction_masked <= instruction_raw;
			instruction_size <= 4;
		else
			-- RVC
			if uncompress_error then
				instruction_in <= (others => '0');
			else
				instruction_in <= instruction_uncompressed;
			end if;
			instruction_masked <= x"0000" & instruction_raw(15 downto 0);
			instruction_size <= 2;
		end if;
	end process;

	process(clk, enable)
		procedure ILLEGAL is
		begin
			raise_exc <= '1';
			exc_data.cause <= EXCEPTION_ILLEGAL_INST;
			exc_data.trap_value <= instruction_masked;
		end procedure;
	begin
		if rising_edge(clk) then
			raise_exc <= '0';
		if enable = '1' then
			instr_info_out <= instr_info_in;

			instr_info_out.next_address <= std_logic_vector(
				unsigned(instr_info_in.address) + instruction_size
			);

			instr_info_out.needs_rs1 <= '0';
			instr_info_out.needs_rs2 <= '0';
			instr_info_out.register_we <= '0';

			instr_info_out.rs1_addr <= a_rs1;
			instr_info_out.rs2_addr <= a_rs2;
			instr_info_out.rd_addr <= a_rd;

			instr_info_out.uses_alu_cmp <= '0';
			instr_info_out.uses_alu_math <= '0';
			instr_info_out.alu_cmp_signed <= '1';

			instr_info_out.uses_csr <= '0';

			instr_info_out.uses_memory <= '0';
			instr_info_out.mem_read <= '0';
			instr_info_out.mem_write <= '0';

			instr_info_out.jump_condition <= JUMP_NEVER;
			instr_info_out.mask_jump_destination_lsb <= '0';

			instr_info_out.return_trap <= '0';
			instr_info_out.enter_wfi <= '0';

			exc_data.ins_address <= instr_info_in.address;

			-- all-zeroes illegal (all-ones covered by opcode switch)
			if instruction_in = (instruction_in'range => '0') then
				ILLEGAL;
			end if;

			case a_opcode is
				when OPCODE_OP_IMM | OPCODE_OP =>
					-- OP_IMM and OP are almost the same, just need to change ALU input ports
					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_ALU_OUT;

					instr_info_out.needs_rs1 <= '1';
					alu_muxsel_a <= PORT_RS1;

					if a_opcode = OPCODE_OP_IMM then
						-- use rs1 and IMM for OP_IMM
						alu_muxsel_b <= PORT_IMM;
						alu_muxsel_cmp2 <= PORT_IMM;

						case a_funct3 is
							when F3_SHIFTL | F3_SHIFTR =>
								-- shifting is actually an R-type,
								-- with the shift amount encoded in the rs2 field
								instr_info_out.immediate <= std_logic_vector(resize(unsigned(a_rs2), 32));
							when others =>
								-- for all others, use I-type
								instr_info_out.immediate <= imm_extract(instruction_in, TYPE_I);
						end case;
					else
						-- use rs1 and rs2 for OP
						instr_info_out.needs_rs2 <= '1';

						alu_muxsel_b <= PORT_RS2;
						alu_muxsel_cmp2 <= PORT_RS2;
					end if;

					if a_opcode = OPCODE_OP and a_funct7 = F7_MULDIV then
						instr_info_out.uses_alu_math <= '1';
						case a_funct3 is
							when F3_MUL    => instr_info_out.alu_op <= ALU_MULL;
							when F3_MULH   => instr_info_out.alu_op <= ALU_MULHSS;
							when F3_MULHSU => instr_info_out.alu_op <= ALU_MULHSU;
							when F3_MULHU  => instr_info_out.alu_op <= ALU_MULHUU;
							when F3_DIV    => instr_info_out.alu_op <= ALU_DIV;
							when F3_DIVU   => instr_info_out.alu_op <= ALU_DIVU;
							when F3_REM    => instr_info_out.alu_op <= ALU_REM;
							when F3_REMU   => instr_info_out.alu_op <= ALU_REMU;
							when others => ILLEGAL;
						end case;
					-- OP is always funct7 = 0 except special-cased SUB (special ADD) and SRA (special SRL).
					-- OP_IMM is funct7 = 0 for SRL; 0 or special for SRA; anything (immediate) for others.
					elsif a_funct7 = F7_OP_DEFAULT or (
					          a_funct7 = F7_OP_SPECIAL and (
					              a_funct3 = F3_SHIFTR or
					              (a_funct3 = F3_ADDSUB and a_opcode = OPCODE_OP)
					          )
					      ) or (
					          a_opcode = OPCODE_OP_IMM and
					          a_funct3 /= F3_SHIFTL and
					          a_funct3 /= F3_SHIFTR
					      ) then
						case a_funct3 is
							when F3_ADDSUB =>
								instr_info_out.uses_alu_math <= '1';

								-- OP has subtraction as well
								if a_opcode = OPCODE_OP and a_funct7 = F7_SUB then
									instr_info_out.alu_op <= ALU_SUB;
								else
									instr_info_out.alu_op <= ALU_ADD;
								end if;
							when F3_SHIFTL =>
								instr_info_out.uses_alu_math <= '1';
								instr_info_out.alu_op <= ALU_SHIFTL;
							when F3_SLT =>
								-- use ALU's compare function
								instr_info_out.uses_alu_cmp <= '1';
								instr_info_out.muxsel_rd <= PORT_ALU_LESSTHAN;
							when F3_SLTU =>
								-- use ALU's compare function
								instr_info_out.uses_alu_cmp <= '1';
								instr_info_out.alu_cmp_signed <= '0';
								instr_info_out.muxsel_rd <= PORT_ALU_LESSTHAN;
							when F3_XOR =>
								instr_info_out.uses_alu_math <= '1';
								instr_info_out.alu_op <= ALU_XOR;
							when F3_SHIFTR =>
								instr_info_out.uses_alu_math <= '1';
								case a_funct7 is
									when F7_SRL =>
										instr_info_out.alu_op <= ALU_SHIFTR_L;
									when F7_SRA =>
										instr_info_out.alu_op <= ALU_SHIFTR_A;
									when others =>
										ILLEGAL;
								end case;
							when F3_OR =>
								instr_info_out.uses_alu_math <= '1';
								instr_info_out.alu_op <= ALU_OR;
							when F3_AND =>
								instr_info_out.uses_alu_math <= '1';
								instr_info_out.alu_op <= ALU_AND;
							when others =>
								ILLEGAL;
						end case;
					else
						ILLEGAL;
					end if;

				when OPCODE_AUIPC =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_U);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					alu_muxsel_a <= PORT_PC_OUT;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_ALU_OUT;

				when OPCODE_LUI =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_U);

					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_IMM;

				when OPCODE_JAL =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_J);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					alu_muxsel_a <= PORT_PC_OUT;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_PC_OUT;

					instr_info_out.jump_condition <= JUMP_ALWAYS;

				when OPCODE_JALR =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_I);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					instr_info_out.needs_rs1 <= '1';
					alu_muxsel_a <= PORT_RS1;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_PC_OUT;

					instr_info_out.jump_condition <= JUMP_ALWAYS;
					instr_info_out.mask_jump_destination_lsb <= '1';

				when OPCODE_LOAD =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_I);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					instr_info_out.needs_rs1 <= '1';
					alu_muxsel_a <= PORT_RS1;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.register_we <= '1';
					instr_info_out.muxsel_rd <= PORT_MEM_OUT;

					instr_info_out.uses_memory <= '1';
					instr_info_out.mem_read <= '1';
					instr_info_out.mem_sign_extend <= not a_funct3(2);

				when OPCODE_STORE =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_S);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					instr_info_out.needs_rs1 <= '1';
					alu_muxsel_a <= PORT_RS1;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.uses_memory <= '1';
					instr_info_out.mem_write <= '1';
					instr_info_out.mem_sign_extend <= '0';

				when OPCODE_BRANCH =>
					instr_info_out.immediate <= imm_extract(instruction_in, TYPE_B);

					instr_info_out.uses_alu_math <= '1';
					instr_info_out.alu_op <= ALU_ADD;
					alu_muxsel_a <= PORT_PC_OUT;
					alu_muxsel_b <= PORT_IMM;

					instr_info_out.uses_alu_cmp <= '1';
					instr_info_out.needs_rs2 <= '1';
					alu_muxsel_cmp2 <= PORT_RS2;

					case a_funct3 is
						when F3_BEQ =>
							instr_info_out.jump_condition <= JUMP_BEQ;
						when F3_BNE =>
							instr_info_out.jump_condition <= JUMP_BNE;
						when F3_BLT =>
							instr_info_out.jump_condition <= JUMP_BLT;
						when F3_BGE =>
							instr_info_out.jump_condition <= JUMP_BGE;
						when F3_BLTU =>
							instr_info_out.alu_cmp_signed <= '0';
							instr_info_out.jump_condition <= JUMP_BLT;
						when F3_BGEU =>
							instr_info_out.alu_cmp_signed <= '0';
							instr_info_out.jump_condition <= JUMP_BGE;
						when others =>
							ILLEGAL;
					end case;

				when OPCODE_SYSTEM =>
					if a_funct3 = F3_PRIV then
						-- TODO: more privileged instructions
						case a_funct12 is
							when F12_MRET =>
								instr_info_out.return_trap <= '1';
							when F12_WFI =>
								instr_info_out.enter_wfi <= '1';
							when F12_ECALL =>
								-- TODO: S-mode
								raise_exc <= '1';
								exc_data.cause <= EXCEPTION_ENVCALL_M;
								exc_data.trap_value <= (others => '0');
							when F12_EBREAK =>
								raise_exc <= '1';
								exc_data.cause <= EXCEPTION_BREAKPOINT;
								exc_data.trap_value <= instr_info_in.address;
							--when F12_URET =>
							--when F12_SRET =>
							when others =>
								ILLEGAL;
						end case;
					else
						-- CSR instructions
						instr_info_out.immediate <= imm_extract(instruction_in, CSR_IMM);

						instr_info_out.uses_csr <= '1';
						instr_info_out.csr_addr <= a_funct12;

						instr_info_out.register_we <= '1';
						instr_info_out.muxsel_rd <= PORT_CSR_OUT;

						case a_funct3 is
							when F3_CSRRW | F3_CSRRS | F3_CSRRC =>
								instr_info_out.needs_rs1 <= '1';
								csr_muxsel_in <= PORT_RS1;
							when F3_CSRRWI | F3_CSRRSI | F3_CSRRCI =>
								csr_muxsel_in <= PORT_IMM;
							when others =>
								ILLEGAL;
						end case;

						case a_funct3 is
							when F3_CSRRW | F3_CSRRWI =>
								if a_rd = "00000" then
									instr_info_out.csr_op <= CSR_WRITEONLY;
								else
									instr_info_out.csr_op <= CSR_EXCHANGE;
								end if;

							when F3_CSRRS | F3_CSRRSI =>
								if a_rs1 = "00000" then
									instr_info_out.csr_op <= CSR_READONLY;
								else
									instr_info_out.csr_op <= CSR_SET;
								end if;

							when F3_CSRRC | F3_CSRRCI =>
								if a_rs1 = "00000" then
									instr_info_out.csr_op <= CSR_READONLY;
								else
									instr_info_out.csr_op <= CSR_CLEAR;
								end if;

							when others =>
								ILLEGAL;
						end case;
					end if;

				when OPCODE_MISC_MEM =>
					-- NOP, since we don't reorder or cache anything
					--instr_info_out.immediate <= imm_extract(instruction_in, TYPE_I);

					case a_funct3 is
						when F3_FENCE =>
						when F3_FENCE_I =>
						when others =>
							ILLEGAL;
					end case;

				when others =>
					ILLEGAL;
			end case;

			case a_funct3(1 downto 0) is
				when "00" =>
					instr_info_out.mem_width <= BYTE;
				when "01" =>
					instr_info_out.mem_width <= HALFWORD;
				when "10" =>
					instr_info_out.mem_width <= WORD;
				when others =>
					if a_opcode = OPCODE_LOAD or a_opcode = OPCODE_STORE then
						ILLEGAL;
					end if;
			end case;
		end if;
		end if;
	end process;
end rtl;
