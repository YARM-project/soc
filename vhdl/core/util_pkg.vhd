library ieee;
use ieee.std_logic_1164.all;

use std.textio.all;

use work.yarm_types_pkg.all;

package util_pkg is
	type slv_mem_t is array(natural range <>) of std_logic_vector;

	function endian_switch(data : yarm_word) return yarm_word;
	function addr_bits_req(num_elements : integer) return integer;

	function misaligned(addr : yarm_word; data_width : datum_width_t) return std_logic;

	impure function init_memory_from_file(
		file_name : string;
		depth : positive;
		word_size : positive
	) return slv_mem_t;
end package util_pkg;

package body util_pkg is
	function endian_switch(data : yarm_word) return yarm_word is
	begin
		return data(7 downto 0) & data(15 downto 8) & data(23 downto 16) & data(31 downto 24);
	end endian_switch;

	-- returns the number of address bits required to store a given number of elements - ceil(log2(num))
	function addr_bits_req(num_elements : integer) return integer is
		variable bits_req : integer;
		variable remaining : integer;
	begin
		bits_req := 0;
		remaining := num_elements - 1;

		while remaining > 0 loop
			bits_req := bits_req + 1;
			remaining := remaining / 2;
		end loop;

		return bits_req;
	end addr_bits_req;

	function misaligned(addr : yarm_word; data_width : datum_width_t) return std_logic is
	begin
		case data_width is
			when BYTE =>
				return '0';
			when HALFWORD =>
				return addr(0);
			when WORD =>
				return (or addr(1 downto 0));
		end case;
	end function;

	impure function read_memory_file(
		file_name : string;
		depth : positive;
		word_size : positive;
		skip_first_line : boolean
	) return slv_mem_t is
		file file_handle : text open read_mode is file_name;
		variable current_line : line;
		variable good         : boolean;
		variable result       : slv_mem_t(0 to depth-1)(word_size-1 downto 0) :=
			(others => (others => '0'));
	begin
		for i in 0 to depth-1 loop
			exit when endfile(file_handle);
			readline(file_handle, current_line);

			hread(current_line, result(i), good);
			if not good then
				report "Not a hex literal in memory file '" & file_name & "': " & current_line.all
				severity failure;
			end if;
		end loop;

		assert endfile(file_handle)
			report "Could not fit image '" & file_name & "' into memory!"
			severity failure;

		return result;
	end read_memory_file;

	impure function init_memory_from_file(
		file_name : string;
		depth : positive;
		word_size : positive
	) return slv_mem_t is
		constant uninitialized : slv_mem_t(0 to depth-1)(word_size-1 downto 0) :=
			(others => (others => 'U'));
	begin
		if file_name'length = 0 then
			return uninitialized;
		else
			return read_memory_file(file_name, depth, word_size, true);
		end if;
	end init_memory_from_file;
end package body util_pkg;
