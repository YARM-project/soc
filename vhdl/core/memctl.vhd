-- asynchronously transforms memory addresses and data
-- does endianness conversion (big-e core, little-e mem), sign extension,
-- address alignment and datum width -> byte enable conversion

library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all,
    work.util_pkg.all;

entity memctl is
	port (
		addr        : in yarm_word;
		-- data width
		data_width  : in datum_width_t;
		-- perfom sign extension when reading short data
		sign_extend : in std_logic;
		data_read   : out yarm_word;
		data_write  : in yarm_word;

		MEM_addr        : out yarm_word;
		MEM_byte_enable : out std_logic_vector(3 downto 0);
		MEM_data_read   : in yarm_word;
		MEM_data_write  : out yarm_word
	);
end memctl;

architecture behaviour of memctl is
begin
	-- move the address to the output synchronously, blanking the
	-- lowest 2 bits (32 bit alignment)
	MEM_addr <= addr(addr'high downto 2) & "00";

	-- process incoming data - endianness conversion, byte extraction, sign extension
	read_process: process(all)
		variable shifted_read : yarm_word;
	begin
		-- convert to BE, then shift the read value right so it's aligned
		shifted_read := std_logic_vector(shift_right(
				unsigned(endian_switch(MEM_data_read)),
				8 * to_integer(unsigned(addr(1 downto 0)))
			));

		case data_width is
			when BYTE =>
				if sign_extend = '1' then
					data_read <= std_logic_vector(resize(signed(
							shifted_read(7 downto 0)
						), 32));
				else
					data_read <= std_logic_vector(resize(unsigned(
							shifted_read(7 downto 0)
						), 32));
				end if;

			when HALFWORD =>
				if sign_extend = '1' then
					data_read <= std_logic_vector(resize(signed(
							shifted_read(15 downto 0)
						), 32));
				else
					data_read <= std_logic_vector(resize(unsigned(
							shifted_read(15 downto 0)
						), 32));
				end if;

			when WORD =>
				data_read <= shifted_read;
		end case;
	end process;

	-- process outgoing data - endianness conversion, byte shifting
	write_process: process(all)
	begin
		MEM_data_write <= std_logic_vector(shift_right(
				unsigned(endian_switch(data_write)),
				8 * to_integer(unsigned(addr(1 downto 0)))
			));
	end process;

	-- process byte enable
	byte_en: process(all)
		variable temp_byte_en : std_logic_vector(3 downto 0);
	begin
		-- create mask first, shift later
		case data_width is
			when BYTE =>
				temp_byte_en := "1000";
			when HALFWORD =>
				temp_byte_en := "1100";
			when WORD =>
				temp_byte_en := "1111";
		end case;

		MEM_byte_enable <= std_logic_vector(shift_right(
				unsigned(temp_byte_en),
				to_integer(unsigned(addr(1 downto 0)))
			));
	end process;
end behaviour;
