library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.yarm_types_pkg.all;

entity registers is
	port (
		clk   : in std_logic;

		read_enable  : in std_logic;
		write_enable : in std_logic;

		addr_a : in register_addr_t;
		addr_b : in register_addr_t;
		addr_d : in register_addr_t;

		data_a : out yarm_word;
		data_b : out yarm_word;
		data_d : in yarm_word
	);
end registers;

architecture behaviour of registers is
	type store_t is array(1 to 31) of yarm_word;
	signal store : store_t;
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if read_enable = '1' then
				if unsigned(addr_a) = 0 then
					data_a <= (others => '0');
				else
					data_a <= store(to_integer(unsigned(addr_a)));
				end if;

				if unsigned(addr_b) = 0 then
					data_b <= (others => '0');
				else
					data_b <= store(to_integer(unsigned(addr_b)));
				end if;
			end if;

			if write_enable = '1' and unsigned(addr_d) /= 0 then
				store(to_integer(unsigned(addr_d))) <= data_d;
			end if;
		end if;
	end process;
end behaviour;
