library ieee;
use ieee.std_logic_1164.all;

use work.yarm_types_pkg.all,
	work.rom.all;

entity core_tb is
end core_tb;

architecture rtl of core_tb is
	signal clk, reset, stop : std_logic;

	signal MEM_addr  : yarm_word;
	signal MEM_read  : std_logic;
	signal MEM_write : std_logic;
	signal MEM_ready : std_logic;
	signal MEM_byte_enable : std_logic_vector(3 downto 0);
	signal MEM_data_read   : yarm_word;
	signal MEM_data_write  : yarm_word;
begin
	uut: entity work.core
		generic map (
			HART_ID => 0
		)
		port map (
			clk => clk,
			reset => reset,

			MEM_addr        => MEM_addr,
			MEM_read        => MEM_read,
			MEM_write       => MEM_write,
			MEM_ready       => MEM_ready,
			MEM_byte_enable => MEM_byte_enable,
			MEM_data_read   => MEM_data_read,
			MEM_data_write  => MEM_data_write
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	mem: entity work.memory
		generic map (
			DATA_WIDTH => 32,
			ADDR_WIDTH => 13,
			INIT_DATA  => ROM_IMAGE
		)
		port map (
			clk_a        => clk,
			enable_a     => MEM_read or MEM_write,
			write_en_a   => MEM_write,
			byte_en_a    => MEM_byte_enable,
			address_a    => MEM_addr(14 downto 2),
			data_in_a    => MEM_data_write,
			data_out_a   => MEM_data_read,

			clk_b        => '0',
			enable_b     => '0',
			write_en_b   => '0',
			byte_en_b    => (others => '0'),
			address_b    => (others => '0'),
			data_in_b    => (others => '0'),
			data_out_b   => open
		);

	stimulus: process
	begin
		MEM_ready <= '1';
		wait for 5 us;
		stop <= '1';
		wait;
	end process;
end rtl;
