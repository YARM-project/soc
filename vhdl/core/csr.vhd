-- Control and Status registers
-- operated during EXECUTE stage instead of ALU
library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity csr is
	generic (
		HART_ID : integer
	);
	port (
		clk    : in std_logic;
		reset  : in std_logic;
		enable : in std_logic;
		ready  : out std_logic;

		instr_info_in : in instruction_info_t;
		data_write    : in yarm_word;
		data_read     : out yarm_word;

		increase_instret : in std_logic;

		external_int : in std_logic;
		timer_int    : in std_logic;
		software_int : in std_logic;

		interrupts_pending : out yarm_word;
		interrupts_enabled : out yarm_word;
		global_int_enabled : out std_logic;
		mtvec_out : out yarm_word;
		mepc_out  : out yarm_word;

		do_trap     : in std_logic;
		return_m_trap : in std_logic;
		mepc_in       : in yarm_word;
		mcause_in     : in yarm_trap_cause;
		mtval_in      : in yarm_word;

		raise_exc : out std_logic;
		exc_data  : out exception_data_t
	);
end csr;

architecture behaviour of csr is
	type csr_state_t is (READ, CALC, WRITEBACK);
	signal write_state : csr_state_t := READ;

	signal access_allowed : std_logic;

	-- latched variables for writing state machine
	signal write_op       : csr_operation_t;
	signal write_addr     : csr_addr_t;
	signal write_in_data  : yarm_word;
	-- data that was read in the first cycle, needed for writeback later
	signal previous_read_data : yarm_word;

	signal csr_mstatus : yarm_word := x"0000_0000";
	alias flag_MPP : std_logic_vector(1 downto 0) is csr_mstatus(12 downto 11);
	alias flag_SPP : std_logic_vector(0 downto 0) is csr_mstatus(8 downto 8);

	alias flag_MPIE : std_logic is csr_mstatus(7);
	alias flag_SPIE : std_logic is csr_mstatus(5);
	alias flag_UPIE : std_logic is csr_mstatus(4);

	alias flag_MIE : std_logic is csr_mstatus(3);
	alias flag_SIE : std_logic is csr_mstatus(1);
	alias flag_UIE : std_logic is csr_mstatus(0);

	signal csr_mip      : yarm_word := x"0000_0000";
	signal csr_mie      : yarm_word := x"0000_0000";
	signal csr_mtvec    : yarm_word := x"0000_0000";
	signal csr_mscratch : yarm_word := x"0000_0000";
	signal csr_mepc     : yarm_word := x"0000_0000";
	signal csr_mcause   : yarm_word := x"0000_0000";
	signal csr_mtval    : yarm_word := x"0000_0000";

	signal counter_cycles  : unsigned(63 downto 0) := (others => '0');
	signal counter_instret : unsigned(63 downto 0) := (others => '0');
begin
	merge_pending_ints: process(all)
	begin
		interrupts_pending <= csr_mip;
		interrupts_pending(11) <= external_int;
		interrupts_pending(7)  <= timer_int;
		interrupts_pending(3)  <= software_int;
	end process;

	interrupts_enabled <= csr_mie;
	global_int_enabled <= flag_MIE;
	mtvec_out <= csr_mtvec;
	mepc_out  <= csr_mepc;

	cycle_count: process(clk)
	begin
		if rising_edge(clk) then
			counter_cycles <= counter_cycles + 1;
		end if;
	end process;

	instret_count: process(clk)
	begin
		if rising_edge(clk) and increase_instret = '1' then
			counter_instret <= counter_instret + 1;
		end if;
	end process;

	permission: process(all)
		-- access permissions
		type csr_perm_t is (NONE, READONLY, READWRITE);
		variable csr_perm : csr_perm_t := NONE;
	begin
		access_allowed <= '0';

		-- writability
		if instr_info_in.csr_addr(11 downto 10) = "11" then
			csr_perm := READONLY;
		else
			csr_perm := READWRITE;
		end if;

		-- only allow machine mode CSRs
		if instr_info_in.csr_addr(9 downto 8) /= "11" then
			csr_perm := NONE;
		end if;

		if csr_perm = READWRITE or
		  (csr_perm = READONLY and instr_info_in.csr_op = CSR_READONLY) then
			access_allowed <= '1';
		end if;
	end process;

	regs_proc: process(clk)
		variable read_data : yarm_word;
	begin
		if rising_edge(clk) then
			raise_exc <= '0';

			exc_data <= (
				cause => EXCEPTION_ILLEGAL_INST,
				ins_address => instr_info_in.address,
				trap_value => instr_info_in.instruction
			);

			if reset = '1' then
				csr_mstatus <= (others => '0');
				csr_mcause  <= (others => '0');
				write_state <= READ;
			else
				if do_trap = '1' then
					csr_mepc   <= mepc_in;
					csr_mcause <= std_logic_vector(mcause_in);
					csr_mtval  <= mtval_in;
					flag_MPIE  <= flag_MIE;
					flag_MIE   <= '0';
				elsif return_m_trap = '1' then
					flag_MIE   <= flag_MPIE;
					flag_MPIE  <= '1';
				end if;

				if enable and not access_allowed then
					raise_exc <= '1';
				-- continue writeback even if not enabled
				elsif enable = '1' or write_state /= READ then
					-- WRITING
					-- consists of up to 3 stages (latch, mask, writeback)
					-- shortcut for CSRRW operations (only needs latch and writeback)

					case write_state is
						when READ =>
							if instr_info_in.csr_op /= CSR_WRITEONLY then
								case instr_info_in.csr_addr is
									when CSRADDR_MVENDORID =>
										-- non-commercial
										read_data := x"0000_0000";
									when CSRADDR_MARCHID =>
										-- "YARM"
										read_data := x"5941_524d";
									when CSRADDR_MIMPID =>
										read_data := YARM_VERSION;
									when CSRADDR_MHARTID =>
										read_data := std_logic_vector(to_unsigned(
											HART_ID,
											read_data'length
										));
									when CSRADDR_MSTATUS =>
										read_data := csr_mstatus;
									when CSRADDR_MISA =>
										-- XLEN=32, extensions=IM
										read_data := "01" & "0000" &
											b"00100_00010_00100_00000_00000_0";
									when CSRADDR_MIE =>
										read_data := csr_mie;
									when CSRADDR_MTVEC =>
										read_data := csr_mtvec;
									when CSRADDR_MSCRATCH =>
										read_data := csr_mscratch;
									when CSRADDR_MEPC =>
										read_data := csr_mepc;
									when CSRADDR_MCAUSE =>
										read_data := csr_mcause;
									when CSRADDR_MTVAL =>
										read_data := csr_mtval;
									when CSRADDR_MIP =>
										read_data := interrupts_pending;
									when CSRADDR_MCYCLE =>
										read_data := std_logic_vector(counter_cycles(31 downto 0));
									when CSRADDR_MCYCLEH =>
										read_data := std_logic_vector(counter_cycles(63 downto 32));
									when CSRADDR_MINSTRET =>
										read_data := std_logic_vector(counter_instret(31 downto 0));
									when CSRADDR_MINSTRETH =>
										read_data := std_logic_vector(counter_instret(63 downto 32));
									-- TODO: S-mode
									-- when CSRADDR_MCOUNTEREN =>
									-- when CSRADDR_MEDELEG =>
									-- when CSRADDR_MIDELEG =>
									when others =>
								end case;
							end if;

							previous_read_data <= read_data;

							data_read <= read_data;

							-- don't start write operation for CSRR[S/C] rs1=0 special case
							if instr_info_in.csr_op /= CSR_READONLY then
								-- latch values
								write_op      <= instr_info_in.csr_op;
								write_addr    <= instr_info_in.csr_addr;
								write_in_data <= data_write;

								-- skip masking for straight writes
								if instr_info_in.csr_op = CSR_WRITEONLY
								  or instr_info_in.csr_op = CSR_EXCHANGE then
									write_state <= WRITEBACK;
								else
									write_state <= CALC;
								end if;
							end if;

						when CALC =>
							case write_op is
								when CSR_SET =>
									write_in_data <= previous_read_data or write_in_data;
								when CSR_CLEAR =>
									write_in_data <= previous_read_data and (not write_in_data);
								when others =>
							end case;

							write_state <= WRITEBACK;

						when WRITEBACK =>
							case write_addr is
								when CSRADDR_MSTATUS =>
									csr_mstatus <= write_in_data;
								when CSRADDR_MISA =>
									-- do nothing
								when CSRADDR_MIE =>
									csr_mie <= write_in_data;
								when CSRADDR_MTVEC =>
									-- only allow direct mode
									csr_mtvec <= write_in_data(31 downto 2) & "00";
								when CSRADDR_MSCRATCH =>
									csr_mscratch <= write_in_data;
								when CSRADDR_MEPC =>
									csr_mepc <= write_in_data(31 downto 1) & "0";
								when CSRADDR_MCAUSE =>
									csr_mcause <= write_in_data;
								when CSRADDR_MTVAL =>
									csr_mtval <= write_in_data;
								when CSRADDR_MIP =>
									-- only allow writing S and U interrupts
									csr_mip <= write_in_data and x"0000_0333";
								-- don't allow writes to mcycle/minstret ("multiple constant drivers" error)
								-- instead, do nothing
								when CSRADDR_MCYCLE =>
								when CSRADDR_MCYCLEH =>
								when CSRADDR_MINSTRET =>
								when CSRADDR_MINSTRETH =>
								--when CSRADDR_MCYCLE =>
								--	counter_cycles(31 downto 0) <= unsigned(write_in_data);
								--when CSRADDR_MCYCLEH =>
								--	counter_cycles(63 downto 32) <= unsigned(write_in_data);
								--when CSRADDR_MINSTRET =>
								--	counter_instret(31 downto 0) <= unsigned(write_in_data);
								--when CSRADDR_MINSTRETH =>
								--	counter_instret(63 downto 32) <= unsigned(write_in_data);
								when others =>
							end case;

							write_state <= READ;
					end case;
				end if;
			end if;
		end if;
	end process;

	ready <= '1' when write_state = READ else '0';
end behaviour;
