library ieee;
use ieee.std_logic_1164.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all;

entity core is
	generic (
		HART_ID : natural;
		RESET_VECTOR : yarm_word := (others => '0')
	);
	port (
		clk   : in std_logic;
		reset : in std_logic;

		-- little-endian memory interface, 4 byte address alignment
		MEM_addr        : out yarm_word;
		MEM_read        : out std_logic;
		MEM_write       : out std_logic;
		MEM_ready       : in std_logic;
		MEM_byte_enable : out std_logic_vector(3 downto 0);
		MEM_data_read   : in yarm_word;
		MEM_data_write  : out yarm_word;

		external_int : in std_logic;
		timer_int    : in std_logic;
		software_int : in std_logic
	);
end core;

architecture rtl of core is
	signal control_may_interrupt : std_logic;
	signal stage_inputs          : pipeline_frames_t;

	signal interrupted_stage : pipeline_stage_t;

	signal fetch_enable    : std_logic;
	signal fetch_ready     : std_logic;
	signal fetch_instr_out : yarm_word;

	signal datamem_enable    : std_logic;
	signal datamem_ready     : std_logic;
	signal datamem_read_data : yarm_word;

	signal datamem_raise_exc : std_logic;
	signal datamem_exc_data  : exception_data_t;

	signal decoder_enable    : std_logic;
	signal decoder_instr_info_out : instruction_info_t;

	signal decoder_raise_exc : std_logic;
	signal decoder_exc_data  : exception_data_t;

	signal registers_read_enable  : std_logic;
	signal registers_write_enable : std_logic;
	signal registers_data_a       : yarm_word;
	signal registers_data_b       : yarm_word;
	signal registers_data_d       : yarm_word;
	signal registers_addr_a       : register_addr_t;
	signal registers_addr_b       : register_addr_t;

	signal alu_enable_math : std_logic;
	signal alu_enable_cmp  : std_logic;
	signal alu_valid       : std_logic;
	signal alu_muxsel_a    : mux_selector_t;
	signal alu_muxsel_b    : mux_selector_t;
	signal alu_muxsel_cmp2 : mux_selector_t;
	signal alu_a           : yarm_word;
	signal alu_b           : yarm_word;
	signal alu_math_result : yarm_word;
	signal alu_cmp1        : yarm_word;
	signal alu_cmp2        : yarm_word;
	signal alu_cmp_result  : compare_result_t;
	-- 1 when compare result is LESS, 0 otherwise
	signal writeback_lessthan_expanded : yarm_word;

	signal csr_enable     : std_logic;
	signal csr_ready      : std_logic;
	signal csr_muxsel_in  : mux_selector_t;
	signal csr_data_read  : yarm_word;
	signal csr_data_write : yarm_word;

	signal csr_increase_instret   : std_logic;
	signal csr_interrupts_pending : yarm_word;
	signal csr_interrupts_enabled : yarm_word;
	signal csr_global_int_enabled : std_logic;
	signal csr_mtvec_out          : yarm_word;
	signal csr_mepc_out           : yarm_word;

	signal csr_mepc_in       : yarm_word;
	signal csr_mcause_in     : yarm_trap_cause;
	signal csr_mtval_in      : yarm_word;

	signal csr_raise_exc : std_logic;
	signal csr_exc_data  : exception_data_t;

	signal do_trap       : std_logic;
	signal return_m_trap : std_logic;
begin
	memory_arbiter: entity work.memory_arbiter
		port map (
			clk   => clk,
			reset => reset,

			-- fetch port
			fetch_enable    => fetch_enable,
			fetch_ready     => fetch_ready,
			fetch_address   => stage_inputs(FETCH).address,
			fetch_data_width => WORD,
			fetch_instr_out => fetch_instr_out,

			-- data memory port
			datamem_enable        => datamem_enable,
			datamem_ready         => datamem_ready,
			datamem_instr_info_in => stage_inputs(MEMORY),
			datamem_read_data     => datamem_read_data,

			datamem_raise_exc => datamem_raise_exc,
			datamem_exc_data  => datamem_exc_data,

			-- external memory signals
			MEM_addr        => MEM_addr,
			MEM_read        => MEM_read,
			MEM_write       => MEM_write,
			MEM_ready       => MEM_ready,
			MEM_byte_enable => MEM_byte_enable,
			MEM_data_read   => MEM_data_read,
			MEM_data_write  => MEM_data_write
		);

	exception_control: entity work.exception_control
		port map (
			clk => clk,

			decoder_raise_exc => decoder_raise_exc,
			decoder_exc_data  => decoder_exc_data,

			csr_raise_exc => csr_raise_exc,
			csr_exc_data  => csr_exc_data,

			datamem_raise_exc => datamem_raise_exc,
			datamem_exc_data  => datamem_exc_data,

			global_int_enabled => csr_global_int_enabled,
			interrupts_enabled => csr_interrupts_enabled,
			interrupts_pending => csr_interrupts_pending,

			stage_inputs      => stage_inputs,
			interrupted_stage => interrupted_stage,

			may_interrupt => control_may_interrupt,
			do_trap       => do_trap,
			trap_cause    => csr_mcause_in,
			trap_address  => csr_mepc_in,
			trap_value    => csr_mtval_in
		);

	control: entity work.control
		generic map (
			RESET_VECTOR => RESET_VECTOR
		)
		port map (
			clk   => clk,
			reset => reset,

			-- FETCH
			fetch_enable    => fetch_enable,
			fetch_ready     => fetch_ready,
			fetch_instr_out => fetch_instr_out,

			-- DECODE
			decoder_enable         => decoder_enable,
			decoder_instr_info_out => decoder_instr_info_out,

			registers_data_a => registers_data_a,
			registers_data_b => registers_data_b,

			-- EXECUTE
			alu_enable_math    => alu_enable_math,
			alu_enable_cmp     => alu_enable_cmp,
			alu_valid          => alu_valid,
			alu_math_result    => alu_math_result,
			alu_cmp_result     => alu_cmp_result,

			csr_enable           => csr_enable,
			csr_ready            => csr_ready,
			csr_data_read        => csr_data_read,
			csr_increase_instret => csr_increase_instret,

			-- MEMORY
			datamem_enable         => datamem_enable,
			datamem_ready          => datamem_ready,

			registers_read_enable  => registers_read_enable,
			registers_write_enable => registers_write_enable,

			-- interrupt/trap control
			may_interrupt     => control_may_interrupt,
			interrupted_stage => interrupted_stage,

			do_trap     => do_trap,
			trap_vector => csr_mtvec_out,

			return_trap     => return_m_trap,
			trap_return_vec => csr_mepc_out,

			stage_inputs => stage_inputs
		);

	-- DECODE stage

	decoder: entity work.decoder
		port map (
			clk    => clk,
			enable => decoder_enable,

			async_addr_rs1 => registers_addr_a,
			async_addr_rs2 => registers_addr_b,

			alu_muxsel_a    => alu_muxsel_a,
			alu_muxsel_b    => alu_muxsel_b,
			alu_muxsel_cmp2 => alu_muxsel_cmp2,

			csr_muxsel_in => csr_muxsel_in,

			instr_info_in  => stage_inputs(DECODE),
			instr_info_out => decoder_instr_info_out,

			raise_exc => decoder_raise_exc,
			exc_data  => decoder_exc_data
		);

	registers: entity work.registers
		port map (
			clk => clk,

			read_enable  => registers_read_enable,
			write_enable => registers_write_enable,

			data_a => registers_data_a,
			data_b => registers_data_b,
			data_d => registers_data_d,

			addr_a => registers_addr_a,
			addr_b => registers_addr_b,
			addr_d => stage_inputs(WRITEBACK).rd_addr
		);

	-- EXECUTE stage

	alu: entity work.alu
		port map (
			clk => clk,

			enable_math => alu_enable_math,
			valid       => alu_valid,
			operation   => stage_inputs(EXECUTE).alu_op,
			a           => alu_a,
			b           => alu_b,
			math_result => alu_math_result,

			enable_cmp  => alu_enable_cmp,
			cmp_signed  => stage_inputs(EXECUTE).alu_cmp_signed,
			cmp1        => alu_cmp1,
			cmp2        => alu_cmp2,
			cmp_result  => alu_cmp_result
		);

	csr: entity work.csr
		generic map (
			HART_ID => HART_ID
		)
		port map (
			clk    => clk,
			reset  => reset,
			enable => csr_enable,
			ready  => csr_ready,

			instr_info_in => stage_inputs(EXECUTE),
			data_read     => csr_data_read,
			data_write    => csr_data_write,

			increase_instret => csr_increase_instret,

			external_int => external_int,
			timer_int    => timer_int,
			software_int => software_int,

			interrupts_pending => csr_interrupts_pending,
			interrupts_enabled => csr_interrupts_enabled,
			global_int_enabled => csr_global_int_enabled,
			mtvec_out          => csr_mtvec_out,
			mepc_out           => csr_mepc_out,

			do_trap       => do_trap,
			return_m_trap => return_m_trap,
			mepc_in       => csr_mepc_in,
			mcause_in     => csr_mcause_in,
			mtval_in      => csr_mtval_in,

			raise_exc => csr_raise_exc,
			exc_data  => csr_exc_data
		);

	-- MULTIPLEXERS

	with alu_muxsel_a select alu_a <=
		stage_inputs(EXECUTE).rs1_data when PORT_RS1,
		stage_inputs(EXECUTE).address  when PORT_PC_OUT,
		(others => 'U') when others;

	with alu_muxsel_b select alu_b <=
		stage_inputs(EXECUTE).rs2_data  when PORT_RS2,
		stage_inputs(EXECUTE).immediate when PORT_IMM,
		(others => 'U') when others;

	alu_cmp1 <= registers_data_a;
	with alu_muxsel_cmp2 select alu_cmp2 <=
		stage_inputs(EXECUTE).rs2_data  when PORT_RS2,
		stage_inputs(EXECUTE).immediate when PORT_IMM,
		(others => 'U') when others;

	with csr_muxsel_in select csr_data_write <=
		stage_inputs(EXECUTE).rs1_data  when PORT_RS1,
		stage_inputs(EXECUTE).immediate when PORT_IMM,
		(others => 'U') when others;

	writeback_lessthan_expanded <=
		x"0000_0001" when stage_inputs(WRITEBACK).alu_cmp_result = LESSTHAN else
		x"0000_0000";

	with stage_inputs(WRITEBACK).muxsel_rd select registers_data_d <=
		stage_inputs(WRITEBACK).immediate       when PORT_IMM,
		stage_inputs(WRITEBACK).alu_math_result when PORT_ALU_OUT,
		writeback_lessthan_expanded             when PORT_ALU_LESSTHAN,
		-- store address of next instruction (for JAL/JALR)
		stage_inputs(WRITEBACK).next_address    when PORT_PC_OUT,
		datamem_read_data                       when PORT_MEM_OUT,
		stage_inputs(WRITEBACK).csr_data_read   when PORT_CSR_OUT,
		(others => 'U') when others;
end rtl;
