vunit alu_verify(alu) {
	signal expected_result : std_logic_vector(MATH_DATA_WIDTH-1 downto 0);
	signal has_run : std_logic := '0';

	default clock is rising_edge(clk);

	process(clk)
	begin
		if rising_edge(clk) and enable_math = '1' and valid = '1' then
			has_run <= '1';

			expected_result <= (others => '0');
			case operation is
				when ALU_AND =>
					expected_result <= a and b;
				when ALU_OR =>
					expected_result <= a or b;
				when ALU_XOR =>
					expected_result <= a xor b;
				when ALU_SHIFTL =>
					expected_result <= std_logic_vector(shift_left(
						unsigned(a),
						to_integer(unsigned(b(4 downto 0)))
					));
				when ALU_SHIFTR_L =>
					expected_result <= std_logic_vector(shift_right(
						unsigned(a),
						to_integer(unsigned(b(4 downto 0)))
					));
				when ALU_SHIFTR_A =>
					expected_result <= std_logic_vector(shift_right(
						signed(a),
						to_integer(unsigned(b(4 downto 0)))
					));
				when ALU_ADD =>
					expected_result <= std_logic_vector(signed(a) + signed(b));
				when ALU_SUB =>
					expected_result <= std_logic_vector(signed(a) - signed(b));
				when ALU_MULL =>
					expected_result <= std_logic_vector(resize(unsigned(
						signed(a) * signed(b)
					), MATH_DATA_WIDTH));
				when ALU_MULHSS =>
					expected_result <= std_logic_vector(resize(shift_right(unsigned(
						resize(signed(a), 2 * MATH_DATA_WIDTH) * resize(signed(b), 2 * MATH_DATA_WIDTH)
					), MATH_DATA_WIDTH), MATH_DATA_WIDTH));
				when ALU_MULHUU =>
					expected_result <= std_logic_vector(resize(shift_right(unsigned(
						resize(unsigned(a), 2 * MATH_DATA_WIDTH) * resize(unsigned(b), 2 * MATH_DATA_WIDTH)
					), MATH_DATA_WIDTH), MATH_DATA_WIDTH));
				when ALU_MULHSU =>
					expected_result <= std_logic_vector(resize(shift_right(unsigned(
						unsigned(resize(signed(a), 2 * MATH_DATA_WIDTH)) * resize(unsigned(b), 2 * MATH_DATA_WIDTH)
					), MATH_DATA_WIDTH), MATH_DATA_WIDTH));
				when ALU_DIV =>
					if signed(b) = 0 then
						expected_result <= (others => '1');
					elsif signed(a) = '1' & (a'length-2 downto 0 => '0') and signed(b) = -1 then
						-- overflow
						expected_result <= a;
					else
						expected_result <= std_logic_vector(signed(a) / signed(b));
					end if;
				when ALU_DIVU =>
					if unsigned(b) = 0 then
						expected_result <= (others => '1');
					else
						expected_result <= std_logic_vector(unsigned(a) / unsigned(b));
					end if;
				when ALU_REM =>
					if signed(b) = 0 then
						expected_result <= a;
					elsif signed(a) = '1' & (a'length-2 downto 0 => '0') and signed(b) = -1 then
						-- overflow
						expected_result <= (others => '0');
					else
						expected_result <= std_logic_vector(signed(a) rem signed(b));
					end if;
				when ALU_REMU =>
					if unsigned(b) = 0 then
						expected_result <= a;
					else
						expected_result <= std_logic_vector(unsigned(a) rem unsigned(b));
					end if;
			end case;
		end if;
	end process;

	result_correct: assert always valid and has_run -> math_result = expected_result;
	live: assert always enable_math -> eventually! valid;
}
-- vim: syntax=vhdl:
