library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity div is
	generic (
		DATA_WIDTH : positive := 32
	);
	port (
		clk : in std_logic;

		enable : in std_logic;
		valid  : out std_logic;

		is_signed         : in std_logic;
		dividend, divisor : in std_logic_vector(DATA_WIDTH-1 downto 0);

		quotient, remainder : out std_logic_vector(DATA_WIDTH-1 downto 0)
	);
end entity;

architecture sub of div is
	signal running : std_logic := '0';

	signal negate_quotient, negate_remainder : std_logic;

	signal dividend_reg : unsigned(DATA_WIDTH-1 downto 0);
	signal divisor_reg  : unsigned(DATA_WIDTH*2-2 downto 0);
	signal quotient_reg : std_logic_vector(DATA_WIDTH-1 downto 0);
	signal quotient_active_bit : std_logic_vector(DATA_WIDTH-1 downto 0);
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if enable then
				if not running then
					running <= '1';
					quotient_reg <= (others => '0');
					quotient_active_bit <= ('1', others => '0');

					if is_signed then
						dividend_reg <= unsigned(abs signed(dividend));
						divisor_reg  <= shift_left(
							resize(
								unsigned(abs signed(divisor)),
								DATA_WIDTH*2-1
							),
							DATA_WIDTH-1
						);
						negate_quotient  <= dividend(dividend'left) ?/= divisor(divisor'left) and unsigned(divisor) ?/= 0;
						negate_remainder <= dividend(dividend'left);
					else
						dividend_reg <= unsigned(dividend);
						divisor_reg  <= shift_left(
							resize(
								unsigned(divisor),
								DATA_WIDTH*2-1
							),
							DATA_WIDTH-1
						);
						negate_quotient  <= '0';
						negate_remainder <= '0';
					end if;
				elsif (or quotient_active_bit) then
					if dividend_reg >= divisor_reg then
						dividend_reg <= dividend_reg - divisor_reg(DATA_WIDTH-1 downto 0);
						quotient_reg <= quotient_reg or quotient_active_bit;
					end if;

					divisor_reg <= divisor_reg srl 1;
					quotient_active_bit <= std_logic_vector(shift_right(unsigned(quotient_active_bit), 1));
				else
					running <= '0';
				end if;
			end if;
		end if;
	end process;

	valid <= not running;

	quotient <= std_logic_vector(-signed(quotient_reg)) when negate_quotient else quotient_reg;
	remainder <= std_logic_vector(-signed(dividend_reg)) when negate_remainder else std_logic_vector(dividend_reg);
end architecture;
