library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all;

entity alu is
	generic (
		MATH_DATA_WIDTH : positive := 32;
		CMP_DATA_WIDTH  : positive := 32
	);
	port (
		clk : in std_logic;

		enable_math : in std_logic;
		valid       : out std_logic := '1';
		operation   : in alu_operation_t;
		a, b        : in std_logic_vector(MATH_DATA_WIDTH-1 downto 0);
		math_result : out std_logic_vector(MATH_DATA_WIDTH-1 downto 0);

		-- compare inputs
		-- do signed comparisons
		enable_cmp : in std_logic;
		cmp_signed : in std_logic;
		cmp1, cmp2 : in std_logic_vector(CMP_DATA_WIDTH-1 downto 0);
		cmp_result : out compare_result_t
	);
end alu;

architecture behaviour of alu is
	subtype MATH_WORD is std_logic_vector(MATH_DATA_WIDTH-1 downto 0);
	subtype MATH_DWORD is std_logic_vector(MATH_DATA_WIDTH*2-1 downto 0);

	signal running : std_logic := '0';
	signal active_operation : alu_operation_t;
	type module_t is (INTERNAL, MULT, DIV);
	signal module : module_t;

	signal current_a, current_b : MATH_WORD;
	signal internal_result : MATH_WORD;

	signal mult_select, mult_valid : std_logic;
	signal mult_result : MATH_DWORD;

	signal div_select, div_valid : std_logic;
	signal div_quotient, div_remainder : MATH_WORD;

	signal a_signed, b_signed : std_logic;
begin
	cmp_proc: process(clk)
	begin
		if rising_edge(clk) and enable_cmp = '1' then
			-- comparison
			if cmp1 = cmp2 then
				cmp_result <= EQUALTO;
			elsif cmp_signed = '0' and unsigned(cmp1) < unsigned(cmp2) then
				cmp_result <= LESSTHAN;
			elsif cmp_signed = '1' and signed(cmp1) < signed(cmp2) then
				cmp_result <= LESSTHAN;
			else
				cmp_result <= GREATERTHAN;
			end if;
		end if;
	end process;

	mult_module: entity work.mult
		generic map (
			DATA_WIDTH => MATH_DATA_WIDTH
		)
		port map (
			clk    => clk,
			enable => enable_math and mult_select,
			valid  => mult_valid,

			a_signed => a_signed,
			b_signed => b_signed,
			a        => a,
			b        => b,
			result   => mult_result
		);

	div_module: entity work.div
		generic map (
			DATA_WIDTH => MATH_DATA_WIDTH
		)
		port map (
			clk    => clk,
			enable => enable_math and div_select,
			valid  => div_valid,

			is_signed => a_signed or b_signed,
			dividend  => a,
			divisor   => b,
			quotient  => div_quotient,
			remainder => div_remainder
		);

	calc_proc: process(clk)
	begin
		if rising_edge(clk) and enable_math = '1' and module = INTERNAL then
			-- setup in first cycle
			if not running then
				running <= '1';

				current_a <= a;
				case operation is
					when ALU_SHIFTL | ALU_SHIFTR_L | ALU_SHIFTR_A =>
						current_b <= std_logic_vector(resize(
							unsigned(b(4 downto 0)),
							MATH_DATA_WIDTH
						));
					when others =>
						current_b <= b;
				end case;
			end if;

			-- calculation
			case operation is
				when ALU_AND =>
					internal_result <= a and b;
					running <= '0';

				when ALU_OR =>
					internal_result <= a or b;
					running <= '0';

				when ALU_XOR =>
					internal_result <= a xor b;
					running <= '0';

				when ALU_SHIFTL | ALU_SHIFTR_L | ALU_SHIFTR_A =>
					if running then
						internal_result <= current_a;
						if unsigned(current_b) > 0 then
							case operation is
								when ALU_SHIFTL =>
									current_a <= std_logic_vector(shift_left(
										unsigned(current_a),
										1
									));
								when ALU_SHIFTR_L =>
									current_a <= std_logic_vector(shift_right(
										unsigned(current_a),
										1
									));
								when ALU_SHIFTR_A =>
									current_a <= std_logic_vector(shift_right(
										signed(current_a),
										1
									));
								when others => unreachable_non_shift: assert false;
							end case;

							current_b <= std_logic_vector(unsigned(current_b) - 1);
						else
							running <= '0';
						end if;
					end if;

				when ALU_ADD =>
					internal_result <= std_logic_vector(signed(a) + signed(b));
					running <= '0';

				when ALU_SUB =>
					internal_result <= std_logic_vector(signed(a) - signed(b));
					running <= '0';

				when others => unreachable_external_op: assert false;
			end case;
		end if;
	end process;

	with operation select a_signed <=
		'1' when ALU_MULHSS | ALU_MULHSU | ALU_DIV | ALU_REM,
		'0' when others;

	with operation select b_signed <=
		'1' when ALU_MULHSS | ALU_DIV | ALU_REM,
		'0' when others;

	with operation select module <=
		MULT when ALU_MULL | ALU_MULHSS | ALU_MULHUU | ALU_MULHSU,
		DIV when ALU_DIV | ALU_DIVU | ALU_REM | ALU_REMU,
		INTERNAL when others;
	mult_select <= '1' when module = MULT else '0';
	div_select <= '1' when module = DIV else '0';

	process(clk)
	begin
		if rising_edge(clk) and enable_math = '1' then
			active_operation <= operation;
		end if;
	end process;

	valid <= mult_valid and div_valid and not running;

	with active_operation select math_result <=
		mult_result(MATH_DATA_WIDTH-1 downto 0) when ALU_MULL,
		mult_result(MATH_DATA_WIDTH*2-1 downto MATH_DATA_WIDTH) when ALU_MULHSS | ALU_MULHUU | ALU_MULHSU,
		div_quotient when ALU_DIV | ALU_DIVU,
		div_remainder when ALU_REM | ALU_REMU,
		internal_result when others;

	-- induction hints
	process(clk)
	begin
		if rising_edge(clk) then
			if running = '1' and (
				active_operation = ALU_SHIFTL or
				active_operation = ALU_SHIFTR_L or
				active_operation = ALU_SHIFTR_A) then
				shift_b_hint: assert unsigned(current_b) <= MATH_DATA_WIDTH;
			end if;
		end if;
	end process;
end behaviour;
