library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity mult is
	generic (
		DATA_WIDTH : positive := 32
	);
	port (
		clk : in std_logic;

		enable : in std_logic;
		valid  : out std_logic;

		a_signed, b_signed : in std_logic;
		a, b               : in std_logic_vector(DATA_WIDTH-1 downto 0);

		result             : out std_logic_vector(DATA_WIDTH*2-1 downto 0)
	);
end entity;

architecture carry_save of mult is
	signal countdown : natural range 0 to DATA_WIDTH*2 := 0;
	signal running : std_logic := '0';

	signal a_reg, b_reg, carries, sums : std_logic_vector(DATA_WIDTH*2-1 downto 0);
	signal b_reg_masked : std_logic_vector(DATA_WIDTH*2-1 downto 0);
begin
	process(clk)
	begin
		if rising_edge(clk) then
			if enable then
				if not running then
					running <= '1';
					countdown <= DATA_WIDTH*2;

					if a_signed then
						a_reg <= std_logic_vector(resize(signed(a), a_reg'length));
					else
						a_reg <= std_logic_vector(resize(unsigned(a), a_reg'length));
					end if;

					if b_signed then
						b_reg <= std_logic_vector(resize(signed(b), b_reg'length));
					else
						b_reg <= std_logic_vector(resize(unsigned(b), b_reg'length));
					end if;

					sums <= (others => '0');
					carries <= (others => '0');
				elsif countdown > 0 then
					sums <= sums xor carries xor b_reg_masked;
					carries <= std_logic_vector(shift_left(
						unsigned(
							(sums and carries) or
							(carries and b_reg_masked) or
							(b_reg_masked and sums)
						),
						1
					));

					a_reg <= std_logic_vector(shift_right(
						unsigned(a_reg),
						1
					));
					b_reg <= std_logic_vector(shift_left(
						unsigned(b_reg),
						1
					));
					countdown <= countdown-1;
				else
					running <= '0';
				end if;
			end if;
		end if;
	end process;

	--b_reg_masked <= b_reg and a_reg(0);
	b_reg_masked <= b_reg when a_reg(0) else (others => '0');
	result <= sums;
	valid <= not running;

	counter_bounds: assert always countdown <= DATA_WIDTH*2 @ rising_edge(clk);
end architecture;
