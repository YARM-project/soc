library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

package yarm_types_pkg is
	subtype yarm_word is std_logic_vector(31 downto 0);
	subtype yarm_byte is std_logic_vector(7 downto 0);
	subtype register_addr_t is std_logic_vector(4 downto 0);
	subtype csr_addr_t is std_logic_vector(11 downto 0);

	subtype yarm_trap_cause is unsigned(31 downto 0);

	type datum_width_t is (BYTE, HALFWORD, WORD);

	type alu_operation_t is (
		ALU_AND, ALU_OR, ALU_XOR,
		ALU_SHIFTL, ALU_SHIFTR_L, ALU_SHIFTR_A,
		ALU_ADD, ALU_SUB,
		ALU_MULL, ALU_MULHSS, ALU_MULHUU, ALU_MULHSU,
		ALU_DIV, ALU_DIVU, ALU_REM, ALU_REMU
	);

	type pc_operation_t is (
		PC_FREEZE, PC_INCREMENT, PC_SET
	);

	type csr_operation_t is (
		CSR_READONLY, CSR_WRITEONLY, CSR_EXCHANGE, CSR_SET, CSR_CLEAR
	);

	type state_t is (
		STATE_INHIBIT, STATE_FETCH, STATE_DECODE, STATE_EXECUTE, STATE_MEM,
		STATE_WRITEBACK, STATE_WFI, STATE_INT_ENTER
	);

	type mux_selector_t is (
		PORT_RS1, PORT_RS2, PORT_IMM, PORT_ALU_OUT, PORT_ALU_LESSTHAN,
		PORT_PC_OUT, PORT_MEM_OUT, PORT_CSR_OUT
	);

	type jump_condition_t is (
		JUMP_NEVER, JUMP_ALWAYS, JUMP_BEQ, JUMP_BNE, JUMP_BLT, JUMP_BGE
	);

	type compare_result_t is (
		LESSTHAN, EQUALTO, GREATERTHAN
	);

	type pipeline_stage_t is (FETCH, DECODE, EXECUTE, MEMORY, WRITEBACK);

	type exception_data_t is record
		cause : yarm_trap_cause;
		ins_address : yarm_word;
		trap_value : yarm_word;
	end record;

	type instruction_info_t is record
		address     : yarm_word;
		instruction : yarm_word;
		committed   : std_logic;

		next_address : yarm_word;

		immediate : yarm_word;

		-- REGISTERS
		needs_rs1   : std_logic;
		needs_rs2   : std_logic;
		register_we : std_logic;

		-- register addresses
		rs1_addr    : register_addr_t;
		rs2_addr    : register_addr_t;
		rd_addr     : register_addr_t;

		rs1_data    : yarm_word;
		rs2_data    : yarm_word;

		-- ALU
		uses_alu_math   : std_logic;
		alu_op          : alu_operation_t;
		alu_math_result : yarm_word;

		uses_alu_cmp    : std_logic;
		alu_cmp_signed  : std_logic;
		alu_cmp_result  : compare_result_t;

		-- CSR
		uses_csr       : std_logic;
		csr_op         : csr_operation_t;
		csr_addr       : csr_addr_t;
		csr_data_write : yarm_word;
		csr_data_read  : yarm_word;

		-- MEMORY
		uses_memory     : std_logic;
		mem_read        : std_logic;
		mem_write       : std_logic;
		mem_sign_extend : std_logic;
		mem_width       : datum_width_t;

		-- JUMP CONTROL
		jump_condition : jump_condition_t;
		-- for JALR
		mask_jump_destination_lsb : std_logic;

		-- TRAPS
		return_trap : std_logic;
		enter_wfi   : std_logic;

		-- MUX SELECTORS
		-- ALU and CSR input muxes are output directly by the decoder
		muxsel_rd       : mux_selector_t;
	end record;

	constant INSTRUCTION_FRAME_INITIAL : instruction_info_t := (
		address     => (others => 'U'),
		instruction => (others => 'U'),
		committed   => '0',

		next_address => (others => 'U'),

		immediate => (others => 'U'),

		-- REGISTERS
		needs_rs1 => 'U',
		needs_rs2 => 'U',
		register_we => 'U',

		-- register
		rs1_addr => (others => 'U'),
		rs2_addr => (others => 'U'),
		rd_addr => (others => 'U'),

		rs1_data => (others => 'U'),
		rs2_data => (others => 'U'),

		-- ALU
		uses_alu_math   => 'U',
		alu_op          => ALU_ADD,
		alu_math_result => (others => 'U'),

		uses_alu_cmp   => 'U',
		alu_cmp_signed => 'U',
		alu_cmp_result => LESSTHAN,

		-- CSR
		uses_csr       => 'U',
		csr_op         => CSR_EXCHANGE,
		csr_addr       => (others => 'U'),
		csr_data_write => (others => 'U'),
		csr_data_read  => (others => 'U'),

		-- MEMORY
		uses_memory => 'U',
		mem_read => 'U',
		mem_write => 'U',
		mem_sign_extend => 'U',
		mem_width => WORD,

		-- JUMP
		jump_condition => JUMP_ALWAYS,
		mask_jump_destination_lsb => 'U',

		-- EXCEPTIONS
		return_trap => 'U',
		enter_wfi => 'U',

		-- MUX
		muxsel_rd => PORT_IMM
	);

	type pipeline_frames_t is array(pipeline_stage_t) of instruction_info_t;

	type stage_interface_t is record
		frame_in  : instruction_info_t;
		frame_out : instruction_info_t;

		start : std_logic;
		ready : std_logic;
	end record;
end yarm_types_pkg;

package body yarm_types_pkg is
end package body;
