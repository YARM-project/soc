library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all,
    work.util_pkg.all;

entity memory_arbiter is
	port (
		clk   : in std_logic;
		reset : in std_logic;

		fetch_enable    : in std_logic;
		fetch_ready     : out std_logic;
		fetch_address   : in yarm_word;
		fetch_data_width : in datum_width_t;
		fetch_instr_out : out yarm_word;

		datamem_enable        : in std_logic;
		datamem_ready         : out std_logic;
		datamem_instr_info_in : in instruction_info_t;
		datamem_read_data     : out yarm_word;

		datamem_raise_exc : out std_logic;
		datamem_exc_data  : out exception_data_t;

		-- little-endian memory interface, 4 byte address alignment
		MEM_addr        : out yarm_word;
		MEM_read        : out std_logic;
		MEM_write       : out std_logic;
		MEM_ready       : in std_logic;
		MEM_byte_enable : out std_logic_vector(3 downto 0);
		MEM_data_read   : in yarm_word;
		MEM_data_write  : out yarm_word
	);
end memory_arbiter;

architecture rtl of memory_arbiter is
	signal selected_addr        : yarm_word;
	signal selected_data_width  : datum_width_t;
	signal selected_sign_extend : std_logic;
	signal selected_data_write  : yarm_word;

	signal intmem_data_read : yarm_word;

	signal unmasked_MEM_read        : std_logic;
	signal unmasked_MEM_write       : std_logic;
	signal unmasked_MEM_addr        : yarm_word;
	signal unmasked_MEM_byte_enable : std_logic_vector(3 downto 0);
	signal unmasked_MEM_data_write  : yarm_word;

	type mem_state_t is (IDLE, FETCH_FIRST_HALFWORD, FETCH_SECOND_HALFWORD, FETCH_ALIGNED, DATAMEM);
	signal mem_state : mem_state_t;
	signal next_mem_state : mem_state_t;

	signal datamem_exc_cause : yarm_trap_cause;

	signal datamem_misaligned : std_logic;
begin
	memctl: entity work.memctl
		port map (
			addr        => selected_addr,
			data_width  => selected_data_width,
			sign_extend => selected_sign_extend,
			data_read   => intmem_data_read,
			data_write  => selected_data_write,

			MEM_addr        => unmasked_MEM_addr,
			MEM_byte_enable => unmasked_MEM_byte_enable,
			MEM_data_read   => MEM_data_read,
			MEM_data_write  => unmasked_MEM_data_write
		);

	process(all)
	begin
		next_mem_state <= mem_state;
		fetch_ready   <= '0';
		datamem_ready <= '0';

		if reset then
			next_mem_state <= IDLE;
		else
			case mem_state is
				when IDLE =>
					fetch_ready   <= '1';
					datamem_ready <= '1';

					if fetch_enable then
						if fetch_data_width = WORD and fetch_address(1) = '1' then
							next_mem_state <= FETCH_FIRST_HALFWORD;
						else
							next_mem_state <= FETCH_ALIGNED;
						end if;
					elsif datamem_enable then
						next_mem_state <= DATAMEM;
					end if;

				when FETCH_FIRST_HALFWORD =>
					if MEM_ready then
						next_mem_state <= FETCH_SECOND_HALFWORD;
					end if;

				when FETCH_SECOND_HALFWORD | FETCH_ALIGNED | DATAMEM =>
					if MEM_ready then
						next_mem_state <= IDLE;
					end if;
			end case;
		end if;
	end process;

	datamem_exc_cause <=
		EXCEPTION_STORE_AMO_MISALIGNED when datamem_instr_info_in.mem_write
		else EXCEPTION_LOAD_MISALIGNED;

	process(clk)
	begin
		if rising_edge(clk) then
			datamem_exc_data <= (
				cause => datamem_exc_cause,
				ins_address => datamem_instr_info_in.address,
				trap_value => datamem_instr_info_in.alu_math_result
			);

			if datamem_misaligned then
				datamem_raise_exc <= '1';
				mem_state <= IDLE;
			else
				datamem_raise_exc <= '0';
				mem_state <= next_mem_state;
			end if;

			case mem_state is
				when FETCH_ALIGNED =>
					fetch_instr_out <= intmem_data_read;
				when FETCH_FIRST_HALFWORD =>
					fetch_instr_out(15 downto 0) <= intmem_data_read(31 downto 16);
				when FETCH_SECOND_HALFWORD =>
					fetch_instr_out(31 downto 16) <= intmem_data_read(15 downto 0);
				when DATAMEM =>
					datamem_read_data <= intmem_data_read;
				when IDLE =>
			end case;
		end if;
	end process;

	mux_input: process(all)
		variable select_state : mem_state_t;
	begin
		if next_mem_state /= IDLE then
			select_state := next_mem_state;
		else
			select_state := mem_state;
		end if;

		if select_state = DATAMEM then
			datamem_misaligned <= misaligned(datamem_instr_info_in.alu_math_result, datamem_instr_info_in.mem_width);
		else
			datamem_misaligned <= '0';
		end if;

		case select_state is
			when FETCH_FIRST_HALFWORD | FETCH_SECOND_HALFWORD | FETCH_ALIGNED =>
				unmasked_MEM_read  <= '1';
				unmasked_MEM_write <= '0';

				if select_state = FETCH_SECOND_HALFWORD then
					selected_addr <= std_logic_vector(unsigned(fetch_address) + 2);
				else
					selected_addr <= fetch_address;
				end if;

				if select_state = FETCH_FIRST_HALFWORD or select_state = FETCH_SECOND_HALFWORD then
					selected_data_width <= WORD;
				else
					selected_data_width <= fetch_data_width;
				end if;

				selected_sign_extend <= '0';
				selected_data_write  <= (others => '0');
			when DATAMEM =>
				unmasked_MEM_read    <= datamem_instr_info_in.mem_read;
				unmasked_MEM_write   <= datamem_instr_info_in.mem_write;

				selected_addr        <= datamem_instr_info_in.alu_math_result;
				selected_data_width  <= datamem_instr_info_in.mem_width;
				selected_sign_extend <= datamem_instr_info_in.mem_sign_extend;
				selected_data_write  <= datamem_instr_info_in.rs2_data;
			when others =>
				unmasked_MEM_read    <= '0';
				unmasked_MEM_write   <= '0';

				selected_addr        <= (others => '0');
				selected_data_width  <= WORD;
				selected_sign_extend <= '0';
				selected_data_write  <= (others => '0');
		end case;
	end process;

	mask_output: process(all)
	begin
		if datamem_misaligned = '1' or next_mem_state = IDLE then
			MEM_read        <= '0';
			MEM_write       <= '0';

			MEM_addr        <= (others => '0');
			MEM_byte_enable <= (others => '0');
			MEM_data_write  <= (others => '0');
		else
			MEM_read        <= unmasked_MEM_read;
			MEM_write       <= unmasked_MEM_write;

			MEM_addr        <= unmasked_MEM_addr;
			MEM_byte_enable <= unmasked_MEM_byte_enable;
			MEM_data_write  <= unmasked_MEM_data_write;
		end if;
	end process;
end rtl;
