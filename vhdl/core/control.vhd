library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.constants_pkg.all,
    work.util_pkg.all;

entity control is
	generic (
		RESET_VECTOR : yarm_word
	);
	port (
		clk   : in std_logic;
		reset : in std_logic;

		fetch_enable    : out std_logic;
		fetch_ready     : in std_logic;
		fetch_instr_out : in yarm_word;

		decoder_enable         : out std_logic;
		decoder_instr_info_out : in instruction_info_t;

		registers_data_a : in yarm_word;
		registers_data_b : in yarm_word;

		alu_enable_math : out std_logic;
		alu_math_result : in yarm_word;
		alu_valid       : in std_logic;
		alu_enable_cmp  : out std_logic;
		alu_cmp_result  : in compare_result_t;

		csr_enable           : out std_logic;
		csr_ready            : in std_logic;
		csr_data_read        : in yarm_word;
		csr_increase_instret : out std_logic;

		datamem_enable         : out std_logic;
		datamem_ready          : in std_logic;

		registers_read_enable  : out std_logic;
		registers_write_enable : out std_logic;

		-- TRAP CONTROL

		may_interrupt     : out std_logic;
		-- the stage that will receive an interrupt exception
		interrupted_stage : out pipeline_stage_t;

		do_trap     : in std_logic;
		trap_vector : in yarm_word;

		trap_return_vec : in yarm_word;
		return_trap     : out std_logic;

		-- instruction info records used as input for the respective stages
		stage_inputs : out pipeline_frames_t
	);
end control;

architecture rtl of control is
	-- the stage that was started by the most recent clock cycle (currently running)
	signal running_stage : pipeline_stage_t;
	-- the stage that will be run by the next clock cycle (async)
	signal next_stage    : pipeline_stage_t;

	type stage_enables_t is array(pipeline_stage_t) of std_logic;
	signal stage_enabled : stage_enables_t;

	signal was_reset : std_logic;
	signal halt      : std_logic;

	signal stage_frame_latches : pipeline_frames_t;
	signal stage_outputs : pipeline_frames_t;
	signal curr_output   : instruction_info_t;

	signal force_fetch : std_logic;
	signal fetch_addr_in_forced  : yarm_word;
	signal fetch_addr_in_latched : yarm_word;

	signal execute_done : std_logic;
	signal execute_stage_used_csr : std_logic;

	signal writeback_out_next_address : yarm_word;
begin
	curr_output <= stage_outputs(running_stage);

	fsm_sync: process(clk)
	begin
		if rising_edge(clk) then
			running_stage <= next_stage;
			was_reset <= reset;

			if reset then
				force_fetch <= '1';
				fetch_addr_in_forced <= RESET_VECTOR;
			elsif do_trap then
				force_fetch <= '1';
				fetch_addr_in_forced <= trap_vector;
			elsif fetch_ready then
				force_fetch <= '0';
			end if;
		end if;
	end process;

	fsm: process(all)
	begin
		next_stage <= running_stage;
		halt <= '0';

		if reset or do_trap then
			halt <= '1';
		end if;

		if force_fetch then
			next_stage <= FETCH;
		else
			case running_stage is
				when FETCH =>
					if fetch_ready then
						next_stage <= DECODE;
					end if;
				when DECODE =>
					next_stage <= EXECUTE;
				when EXECUTE =>
					if execute_done then
						if datamem_ready then
							next_stage <= MEMORY;
						else
							halt <= '1';
						end if;
					end if;
				when MEMORY =>
					if datamem_ready then
						next_stage <= WRITEBACK;
					end if;
				when WRITEBACK =>
					if not stage_outputs(WRITEBACK).enter_wfi and fetch_ready then
						next_stage <= FETCH;
					else
						halt <= '1';
					end if;
			end case;
		end if;
	end process;

	execute_done <=
		(execute_stage_used_csr and csr_ready) or
		(not execute_stage_used_csr and alu_valid);

	writeback_decide_next_address: process(all)
		variable address : yarm_word;
		variable should_jump : boolean;
	begin
		case stage_inputs(WRITEBACK).jump_condition is
			when JUMP_NEVER =>
				should_jump := false;
			when JUMP_ALWAYS =>
				should_jump := true;
			when JUMP_BEQ =>
				should_jump := stage_inputs(WRITEBACK).alu_cmp_result = EQUALTO;
			when JUMP_BNE =>
				should_jump := stage_inputs(WRITEBACK).alu_cmp_result /= EQUALTO;
			when JUMP_BLT =>
				should_jump := stage_inputs(WRITEBACK).alu_cmp_result = LESSTHAN;
			when JUMP_BGE =>
				should_jump := stage_inputs(WRITEBACK).alu_cmp_result /= LESSTHAN;
		end case;

		if stage_inputs(WRITEBACK).return_trap then
			address := trap_return_vec;
		elsif should_jump then
			address := stage_inputs(WRITEBACK).alu_math_result;

			if stage_inputs(WRITEBACK).mask_jump_destination_lsb then
				address(0) := '0';
			end if;
		else
			address := stage_inputs(WRITEBACK).next_address;
		end if;

		writeback_out_next_address <= address;
	end process;

	remember_execute_split: process(clk)
	begin
		if rising_edge(clk) and next_stage = EXECUTE then
			-- remember what was ran in execute stage so the correct output is
			-- multiplexed
			execute_stage_used_csr <= stage_inputs(EXECUTE).uses_csr;
		end if;
	end process;

	latch_stage_frames: process(clk)
	begin
		if rising_edge(clk) then
			for stage in stage_inputs'range loop
				if next_stage = stage then
					stage_frame_latches(stage) <= stage_inputs(stage);
				end if;
			end loop;
		end if;
	end process;

	assign_stage_outputs: process(all)
	begin
		-- by default, pass the instruction frame unchanged
		for stage in stage_frame_latches'range loop
			stage_outputs(stage) <= stage_frame_latches(stage);
		end loop;

		-- FETCH
		stage_outputs(FETCH).instruction <= fetch_instr_out;

		-- overwrite entire frame from decoder
		stage_outputs(DECODE) <= decoder_instr_info_out;
		stage_outputs(DECODE).rs1_data <= registers_data_a;
		stage_outputs(DECODE).rs2_data <= registers_data_b;

		-- EXECUTE
		if execute_stage_used_csr then
			stage_outputs(EXECUTE).csr_data_read <= csr_data_read;

			if stage_frame_latches(EXECUTE).csr_op /= CSR_READONLY then
				stage_outputs(EXECUTE).committed <= '1';
			end if;
		else
			stage_outputs(EXECUTE).alu_math_result <= alu_math_result;
			stage_outputs(EXECUTE).alu_cmp_result  <= alu_cmp_result;
		end if;

		-- MEMORY
		if stage_frame_latches(MEMORY).mem_read or stage_frame_latches(MEMORY).mem_write then
			stage_outputs(MEMORY).committed <= '1';
		end if;

		-- WRITEBACK
		stage_outputs(WRITEBACK).next_address <= writeback_out_next_address;

		if stage_frame_latches(WRITEBACK).register_we then
			stage_outputs(WRITEBACK).committed <= '1';
		end if;
	end process;

	latch_fetch_address: process(clk)
	begin
		if rising_edge(clk) then
			fetch_addr_in_latched <= stage_inputs(FETCH).address;
		end if;
	end process;

	assign_stage_inputs: process(all)
	begin
		stage_inputs(FETCH)     <= INSTRUCTION_FRAME_INITIAL;

		if force_fetch then
			stage_inputs(FETCH).address <= fetch_addr_in_forced;
		elsif running_stage = FETCH then
			stage_inputs(FETCH).address <= fetch_addr_in_latched;
		else
			stage_inputs(FETCH).address <= stage_outputs(WRITEBACK).next_address;
		end if;

		stage_inputs(DECODE)    <= stage_outputs(FETCH);
		stage_inputs(EXECUTE)   <= stage_outputs(DECODE);
		stage_inputs(MEMORY)    <= stage_outputs(EXECUTE);
		stage_inputs(WRITEBACK) <= stage_outputs(MEMORY);
	end process;

	calc_stage_enables: process(all)
	begin
		for stage in stage_enabled'range loop
			stage_enabled(stage) <= '1' when next_stage = stage and halt = '0' else '0';
		end loop;
	end process;

	fetch_enable    <= stage_enabled(FETCH);

	decoder_enable  <= stage_enabled(DECODE);
	registers_read_enable <= stage_enabled(DECODE);

	csr_enable      <= stage_enabled(EXECUTE) and stage_inputs(EXECUTE).uses_csr;
	alu_enable_math <= stage_enabled(EXECUTE) and stage_inputs(EXECUTE).uses_alu_math;
	alu_enable_cmp  <= stage_enabled(EXECUTE) and stage_inputs(EXECUTE).uses_alu_cmp;

	datamem_enable  <= stage_enabled(MEMORY)  and stage_inputs(MEMORY).uses_memory;

	registers_write_enable <= stage_enabled(WRITEBACK) and stage_inputs(WRITEBACK).register_we;

	process(clk)
	begin
		if rising_edge(clk) then
			return_trap <= stage_enabled(WRITEBACK) and stage_inputs(WRITEBACK).return_trap;
		end if;
	end process;

	-- misc
	gate_interrupts: process(all)
	begin
		-- needs to become more complicated when we do proper pipelining
		interrupted_stage <= running_stage;

		if curr_output.committed then
			may_interrupt <= '0';
		else
			may_interrupt <= '1';
		end if;
	end process;

	csr_increase_instret <= '1' when running_stage = WRITEBACK and next_stage /= WRITEBACK else '0';
end rtl;
