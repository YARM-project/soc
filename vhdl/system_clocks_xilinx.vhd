library ieee;
use ieee.std_logic_1164.all;

entity system_clocks_xilinx is
	generic (
		CLK_IN_FREQ : natural
	);
	port (
		clk_in : in std_logic;

		clk_core   : out std_logic;
		clk_uart   : out std_logic;
		clk_ddr3   : out std_logic;
		clk_pixel  : out std_logic;
		clk_tmds   : out std_logic;
		clk_ethphy : out std_logic
	);
end system_clocks_xilinx;

architecture behaviour of system_clocks_xilinx is
	signal pll_feedback : std_logic;

	component PLLE2_BASE
		generic (
			CLKFBOUT_MULT : integer;
			DIVCLK_DIVIDE : integer;

			CLKIN1_PERIOD : real;

			CLKOUT0_DIVIDE     : integer := 0;
			CLKOUT0_DUTY_CYCLE : real := 0.5;
			CLKOUT0_PHASE      : real := 0.0;

			CLKOUT1_DIVIDE     : integer := 0;
			CLKOUT1_DUTY_CYCLE : real := 0.5;
			CLKOUT1_PHASE      : real := 0.0;

			CLKOUT2_DIVIDE     : integer := 0;
			CLKOUT2_DUTY_CYCLE : real := 0.5;
			CLKOUT2_PHASE      : real := 0.0;

			CLKOUT3_DIVIDE     : integer := 0;
			CLKOUT3_DUTY_CYCLE : real := 0.5;
			CLKOUT3_PHASE      : real := 0.0;

			CLKOUT4_DIVIDE     : integer := 0;
			CLKOUT4_DUTY_CYCLE : real := 0.5;
			CLKOUT4_PHASE      : real := 0.0;

			CLKOUT5_DIVIDE     : integer := 0;
			CLKOUT5_DUTY_CYCLE : real := 0.5;
			CLKOUT5_PHASE      : real := 0.0
		);
		port (
			RST    : in std_logic;
			PWRDWN : in std_logic;
			LOCKED : out std_logic;

			CLKIN1 : in std_logic;

			CLKFBIN  : in std_logic;
			CLKFBOUT : out std_logic;

			CLKOUT0 : out std_logic;
			CLKOUT1 : out std_logic;
			CLKOUT2 : out std_logic;
			CLKOUT3 : out std_logic;
			CLKOUT4 : out std_logic;
			CLKOUT5 : out std_logic
		);
	end component PLLE2_BASE;
begin
	assert CLK_IN_FREQ = 100_000_000
		report "Xilinx clock generator is currently only designed for 100MHz board clock"
		severity error;

	-- 25MHz pixel clock
	-- 250MHz TMDS clock
	-- 200MHz DDR3 clock
	-- 50MHz bus/core clock
	-- 25MHz ethernet PHY clock
	-- 18.432 MHz UART clock (actually 18.5185, which is within 0.4%)
	-- 640x480@60 technically needs 25.175MHz, but this should work fine
	pll_dvi: PLLE2_BASE
		generic map (
			CLKFBOUT_MULT => 10,
			DIVCLK_DIVIDE => 1,

			CLKIN1_PERIOD => 10.0,

			CLKOUT0_DIVIDE => 5,
			CLKOUT1_DIVIDE => 40,
			CLKOUT2_DIVIDE => 4,
			CLKOUT3_DIVIDE => 20,
			CLKOUT4_DIVIDE => 54,
			CLKOUT5_DIVIDE => 40
		)
		port map (
			RST    => '0',
			PWRDWN => '0',
			CLKIN1 => clk_in,

			CLKFBIN  => pll_feedback,
			CLKFBOUT => pll_feedback,

			CLKOUT0 => clk_ddr3,
			CLKOUT1 => clk_pixel,
			CLKOUT2 => clk_tmds,
			CLKOUT3 => clk_core,
			CLKOUT4 => clk_uart,
			CLKOUT5 => clk_ethphy
		);
end behaviour;
