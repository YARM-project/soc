library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.yarm_types_pkg.all,
    work.util_pkg.all;

entity soc is
	generic (
		IS_SIMULATION : std_logic := '0'
	);
	port (
		n_reset     : in std_logic;
		buttons     : in std_logic_vector(3 downto 0);
		switches    : in std_logic_vector(3 downto 0);
		leds_simple : out std_logic_vector(3 downto 0);
		led0, led1, led2, led3 : out std_logic_vector(2 downto 0);

		-- Pmod connectors - A+D standard, B+C high-speed.
		-- Defined as inputs by default for safety, change
		-- when necessary
		pmod_a : out std_logic_vector(7 downto 0);
		pmod_b : out std_logic_vector(7 downto 0);
		pmod_c : in std_logic_vector(7 downto 0);
		pmod_d : inout std_logic_vector(7 downto 0);

		clock_100mhz : in std_logic;

		uart_rx : in std_logic;
		uart_tx : out std_logic;

		mii_clk_25mhz : out std_logic;
		mii_n_reset   : out std_logic;
		mii_mdio      : inout std_logic;
		mii_mdc       : out std_logic;
		mii_rx_clk    : in std_logic;
		mii_rx_er     : in std_logic;
		mii_rx_dv     : in std_logic;
		mii_rx_data   : in std_logic_vector(3 downto 0);
		mii_tx_clk    : in std_logic;
		mii_tx_en     : out std_logic;
		mii_tx_data   : out std_logic_vector(3 downto 0);
		mii_col       : in std_logic;
		mii_crs       : in std_logic;

		ck_dig_l : inout std_logic_vector(13 downto 0);
		ck_dig_h : inout std_logic_vector(41 downto 26)
	);
end soc;

architecture behavior of soc is
	type mem_state_t is (
		MEMSTATE_IDLE,
		MEMSTATE_INPROGRESS,
		MEMSTATE_DONE
	);
	signal memory_state     : mem_state_t := MEMSTATE_IDLE;

	signal reset_async : std_logic;
	-- remember the component the current read data is from, so it can be
	-- multiplexed asynchronously
	type data_source_t is (SRC_RAM, SRC_TRAM, SRC_IO);
	signal current_data_source : data_source_t;

	-- memory chip select
	signal mcs_RAM  : std_logic;
	signal mcs_TRAM : std_logic;
	signal mcs_IO   : std_logic;

	signal reset           : std_logic;
	signal MEM_addr        : yarm_word;
	signal MEM_read        : std_logic;
	signal MEM_write       : std_logic;
	signal MEM_ready       : std_logic;
	signal MEM_byte_enable : std_logic_vector(3 downto 0);
	signal MEM_data_read   : yarm_word;
	signal MEM_data_write  : yarm_word;

	signal RAM_read  : std_logic;
	signal RAM_write : std_logic;

	signal IO_addr        : yarm_word;
	signal IO_read        : std_logic;
	signal IO_write       : std_logic;
	signal IO_done        : std_logic;
	signal IO_byte_enable : std_logic_vector(3 downto 0);
	signal IO_data_read   : yarm_word;
	signal IO_data_write  : yarm_word;

	signal dvi_red   : std_logic;
	signal dvi_green : std_logic;
	signal dvi_blue  : std_logic;
	signal dvi_clk   : std_logic;

	-- timer interrupts
	signal uart_interrupt : std_logic;
	signal external_int : std_logic;

	signal mtime        : unsigned(63 downto 0) := (others => '0');
	signal mtimecmp     : unsigned(63 downto 0);
	signal timer_int    : std_logic;

	signal software_int : std_logic;

	-- =================
	--  CLOCK GENERATOR
	-- =================
	component system_clocks is
		port (
			clk_in : in std_logic;

			clk_core   : out std_logic;
			clk_uart   : out std_logic;
			clk_ddr3   : out std_logic;
			clk_pixel  : out std_logic;
			clk_tmds   : out std_logic;
			clk_ethphy : out std_logic
		);
	end component;

	signal clk_core  : std_logic;
	signal clk_uart  : std_logic;
	signal clk_pixel : std_logic;
	signal clk_tmds  : std_logic;

	-- ===========
	--  BLOCK RAM
	-- ===========
	component block_ram is
		port (
			clk        : in std_logic;
			read       : in std_logic;
			write      : in std_logic;
			byte_en    : in std_logic_vector(3 downto 0);
			addr       : in std_logic_vector(12 downto 0);
			data_read  : out std_logic_vector(31 downto 0);
			data_write : in std_logic_vector(31 downto 0)
		);
	end component;

	signal RAM_data_read       : yarm_word;
	-- required to meet timing
	signal RAM_data_read_latch : yarm_word;

	-- ==========
	--  TEXT RAM
	-- ==========
	component text_ram is
		port (
			clk_write  : in std_logic;
			write      : in std_logic;
			byte_en    : in std_logic_vector(3 downto 0);
			addr_write : in std_logic_vector(10 downto 0);
			data_write : in std_logic_vector(31 downto 0);

			clk_read  : in std_logic;
			read      : in std_logic;
			addr_read : in std_logic_vector(11 downto 0);
			data_read : out std_logic_vector(15 downto 0)
		);
	end component;

	-- core-side TRAM interface
	signal TRAM_core_read       : std_logic;
	signal TRAM_core_write      : std_logic;
	signal TRAM_core_data_read  : yarm_word;
	-- needed to meet timing on 35T, TRAM core read performance doesn't matter as much
	signal TRAM_data_read_latch : yarm_word;

	-- VGA-side TRAM interface
	signal TRAM_vga_read      : std_logic;
	signal TRAM_vga_addr      : std_logic_vector(11 downto 0);
	signal TRAM_vga_data_read : std_logic_vector(15 downto 0);

	component io is
		generic (
			IS_SIMULATION : std_logic;
			WS2812_NUM_LEDS : natural range 0 to 1024
		);
		port (
			clk_bus   : in std_logic;
			clk_uart  : in std_logic;
			-- 25.175MHz for 640x480@60
			clk_pixel : in std_logic;
			-- 10x pixel clock
			clk_tmds  : in std_logic;
			clk_100mhz : in std_logic;
			reset     : in std_logic;

			IO_addr        : in yarm_word;
			IO_read        : in std_logic;
			IO_write       : in std_logic;
			IO_done        : out std_logic;
			IO_byte_enable : in std_logic_vector(3 downto 0);
			IO_data_read   : out yarm_word;
			IO_data_write  : in yarm_word;

			TRAM_vga_addr : out std_logic_vector(11 downto 0);
			TRAM_vga_data : in std_logic_vector(15 downto 0);

			buttons     : in std_logic_vector(3 downto 0);
			switches    : in std_logic_vector(3 downto 0);
			leds_simple : out std_logic_vector(3 downto 0);
			rgb_led0    : out std_logic_vector(2 downto 0);
			rgb_led1    : out std_logic_vector(2 downto 0);
			rgb_led2    : out std_logic_vector(2 downto 0);
			rgb_led3    : out std_logic_vector(2 downto 0);

			vga_r     : out std_logic_vector(7 downto 0);
			vga_g     : out std_logic_vector(7 downto 0);
			vga_b     : out std_logic_vector(7 downto 0);
			vga_hsync : out std_logic;
			vga_vsync : out std_logic;

			dvi_red   : out std_logic;
			dvi_green : out std_logic;
			dvi_blue  : out std_logic;
			dvi_clk   : out std_logic;

			uart_rx : in std_logic;
			uart_tx : out std_logic;
			uart_interrupt : out std_logic;

			ws2812_data : out std_logic;

			mii_n_reset : out std_logic;
			mii_mdio    : inout std_logic;
			mii_mdc     : out std_logic;
			mii_rx_clk  : in std_logic;
			mii_rx_er   : in std_logic;
			mii_rx_dv   : in std_logic;
			mii_rx_data : in std_logic_vector(3 downto 0);
			mii_tx_clk  : in std_logic;
			mii_tx_en   : out std_logic;
			mii_tx_data : out std_logic_vector(3 downto 0);
			mii_col     : in std_logic;
			mii_crs     : in std_logic;

			external_addr       : out std_logic_vector(7 downto 0);
			external_data_read  : in std_logic_vector(7 downto 0);
			external_data_write : out std_logic_vector(7 downto 0);
			external_read       : out std_logic;
			external_write      : out std_logic;
			external_cs_uart    : out std_logic;
			external_cs_dac     : out std_logic;

			mtime    : in unsigned(63 downto 0);
			mtimecmp : out unsigned(63 downto 0)
		);
	end component;

	signal external_addr       : std_logic_vector(7 downto 0);
	signal external_data_read  : std_logic_vector(7 downto 0);
	signal external_data_write : std_logic_vector(7 downto 0);
	signal external_reset      : std_logic;
	signal external_read       : std_logic;
	signal external_write      : std_logic;
	signal external_cs_uart    : std_logic;
	signal external_cs_dac     : std_logic;

	component OBUFDS is
		port (
			I  : in std_logic;
			O  : out std_logic;
			OB : out std_logic
		);
	end component;
begin
	system_clocks_inst: system_clocks
		port map (
			clk_in => clock_100mhz,

			clk_core   => clk_core,
			clk_uart   => clk_uart,
			clk_ddr3   => open,
			clk_pixel  => clk_pixel,
			clk_tmds   => clk_tmds,
			clk_ethphy => mii_clk_25mhz
		);

	mtime_counter: process(clock_100mhz)
	begin
		if rising_edge(clock_100mhz) then
			mtime <= mtime + 1;
		end if;
	end process;

	block_ram_inst: block_ram
		port map (
			clk        => clk_core,
			read       => RAM_read,
			write      => RAM_write,
			byte_en    => MEM_byte_enable,
			addr       => MEM_addr(14 downto 2),
			data_read  => RAM_data_read,
			data_write => MEM_data_write
		);

	text_ram_inst: text_ram
		port map (
			clk_write  => clk_core,
			write      => TRAM_core_write,
			addr_write => MEM_addr(12 downto 2),
			byte_en    => MEM_byte_enable,
			data_write => MEM_data_write,

			clk_read  => clk_pixel,
			read      => TRAM_vga_read,
			addr_read => TRAM_vga_addr,
			data_read => TRAM_vga_data_read
		);

	TRAM_core_data_read <= (others => '0');

	core_inst: entity work.core
		generic map (
			HART_ID => 0
		)
		port map (
			clk   => clk_core,
			reset => reset,

			MEM_addr        => MEM_addr,
			MEM_read        => MEM_read,
			MEM_write       => MEM_write,
			MEM_ready       => MEM_ready,
			MEM_byte_enable => MEM_byte_enable,
			MEM_data_read   => MEM_data_read,
			MEM_data_write  => MEM_data_write,

			external_int => external_int,
			timer_int    => timer_int,
			software_int => software_int
		);

	io_inst: io
		generic map (
			IS_SIMULATION => IS_SIMULATION,
			WS2812_NUM_LEDS => 100
		)
		port map (
			clk_bus   => clk_core,
			clk_uart  => clk_uart,
			clk_pixel => clk_pixel,
			clk_tmds  => clk_tmds,
			clk_100mhz => clock_100mhz,
			reset     => reset,

			IO_addr        => IO_addr,
			IO_read        => IO_read,
			IO_write       => IO_write,
			IO_done        => IO_done,
			IO_byte_enable => IO_byte_enable,
			IO_data_read   => IO_data_read,
			IO_data_write  => IO_data_write,

			TRAM_vga_addr => TRAM_vga_addr,
			TRAM_vga_data => TRAM_vga_data_read,

			buttons     => buttons,
			switches    => switches,
			leds_simple => leds_simple,
			rgb_led0    => led0,
			rgb_led1    => led1,
			rgb_led2    => led2,
			rgb_led3    => led3,

			vga_r     => open,
			vga_g     => open,
			vga_b     => open,
			vga_hsync => open,
			vga_vsync => open,

			dvi_red   => dvi_red,
			dvi_green => dvi_green,
			dvi_blue  => dvi_blue,
			dvi_clk   => dvi_clk,

			uart_rx => uart_rx,
			uart_tx => uart_tx,
			uart_interrupt => uart_interrupt,

			ws2812_data => open,

			mii_n_reset => mii_n_reset,
			mii_mdio    => mii_mdio,
			mii_mdc     => mii_mdc,
			mii_rx_clk  => mii_rx_clk,
			mii_rx_er   => mii_rx_er,
			mii_rx_dv   => mii_rx_dv,
			mii_rx_data => mii_rx_data,
			mii_tx_clk  => mii_tx_clk,
			mii_tx_en   => mii_tx_en,
			mii_tx_data => mii_tx_data,
			mii_col     => mii_col,
			mii_crs     => mii_crs,

			external_addr       => external_addr,
			external_data_read  => external_data_read,
			external_data_write => external_data_write,
			external_read       => external_read,
			external_write      => external_write,
			external_cs_uart    => external_cs_uart,
			external_cs_dac     => external_cs_dac,

			mtime    => mtime,
			mtimecmp => mtimecmp
		);

	orchestrate: process(clk_core)
	begin
		if rising_edge(clk_core) then
			if reset then
				memory_state <= MEMSTATE_IDLE;
			else
				if memory_state = MEMSTATE_DONE then
					-- automatically go from done back to idle after one cycle
					memory_state <= MEMSTATE_IDLE;
				end if;

				-- text RAM is too slow, give it an extra cycle for reads
				if (mcs_RAM or mcs_TRAM) then
					if MEM_read then
						case memory_state is
							when MEMSTATE_IDLE =>
								memory_state <= MEMSTATE_INPROGRESS;
							when MEMSTATE_INPROGRESS =>
								memory_state <= MEMSTATE_DONE;
							when MEMSTATE_DONE =>
						end case;

						TRAM_data_read_latch <= TRAM_core_data_read;
						RAM_data_read_latch  <= RAM_data_read;
					else
						memory_state <= MEMSTATE_DONE;
					end if;
				end if;

				IO_addr        <= MEM_addr;
				IO_byte_enable <= MEM_byte_enable;
				IO_data_write  <= MEM_data_write;

				if (MEM_read or MEM_write) and mcs_IO then
					if memory_state = MEMSTATE_IDLE then
						IO_read        <= MEM_read;
						IO_write       <= MEM_write;

						memory_state <= MEMSTATE_INPROGRESS;
					elsif IO_done then
						IO_read  <= '0';
						IO_write <= '0';

						memory_state <= MEMSTATE_DONE;
					end if;
				end if;
			end if;
		end if;
	end process;

	-- =================
	-- MEMORY DELEGATION
	-- =================
	mcs_RAM <= '1' when MEM_addr(31 downto 15) = x"0000" & "0" else '0';
	RAM_read  <= MEM_read and mcs_RAM;
	RAM_write <= MEM_write and mcs_RAM;

	mcs_TRAM <= '1' when MEM_addr(31 downto 13) = x"0001" & "000" else '0';
	TRAM_core_read  <= MEM_read and mcs_TRAM;
	TRAM_core_write <= MEM_write and mcs_TRAM;
	TRAM_vga_read <= n_reset;

	mcs_IO <= not (mcs_RAM or mcs_TRAM);

	MEM_ready <= '1' when memory_state = MEMSTATE_DONE else '0';

	process(clk_core)
	begin
		if rising_edge(clk_core) then
			if mcs_RAM then
				current_data_source <= SRC_RAM;
			elsif mcs_TRAM then
				current_data_source <= SRC_TRAM;
			else
				current_data_source <= SRC_IO;
			end if;
		end if;
	end process;

	with current_data_source select MEM_data_read <=
		RAM_data_read_latch when SRC_RAM,
		TRAM_data_read_latch when SRC_TRAM,
		IO_data_read when SRC_IO;

	-- ==========
	-- INTERRUPTS
	-- ==========

	-- extra register to meet timing
	timer_int_calc: process(clk_core)
	begin
		if rising_edge(clk_core) then
			if mtime >= mtimecmp then
				timer_int <= '1';
			else
				timer_int <= '0';
			end if;
		end if;
	end process;

	-- FIXME: we probably want more than one interrupt
	external_int <= uart_interrupt;
	software_int <= '0';

	-- =====================
	-- HARDWARE PORT MAPPING
	-- =====================
	--led(9 downto 4) <= DBG(31 downto 26);
	--led(3) <= RAM_enable;
	--led(2) <= DBG(25);
	--led(1) <= timer_int;

	reset_async <= not n_reset;

	reset_sync: entity work.synchronize_reset
		generic map (
			LEVELS => 5
		)
		port map (
			clk => clock_100mhz,
			data_in => reset_async,
			data_out => reset
		);

	external_reset <= reset;

	-- BOARD ERRATA: PMOD nibbles are reversed
	pmod_a <= external_addr(3 downto 0) & external_addr(7 downto 4);
	-- inout signals - fun for the whole family!
	pmod_d <=
		external_data_write(3 downto 0) & external_data_write(7 downto 4) when external_write
		else (others => 'Z');
	external_data_read <= pmod_d(3 downto 0) & pmod_d(7 downto 4);

	--ck_dig_l <= (others => '1');
	ck_dig_l(0) <= external_reset;
	ck_dig_l(1) <= not external_write;
	ck_dig_l(2) <= not external_read;
	ck_dig_l(3) <= not external_cs_uart;
	ck_dig_l(4) <= not external_cs_dac;

	OBUFDS_dvi_green: OBUFDS port map (I => dvi_green, O => pmod_b(0), OB => pmod_b(1));
	OBUFDS_dvi_blue:  OBUFDS port map (I => dvi_blue,  O => pmod_b(2), OB => pmod_b(3));
	OBUFDS_dvi_red:   OBUFDS port map (I => dvi_red,   O => pmod_b(4), OB => pmod_b(5));
	OBUFDS_dvi_clock: OBUFDS port map (I => dvi_clk,   O => pmod_b(6), OB => pmod_b(7));
end behavior;
