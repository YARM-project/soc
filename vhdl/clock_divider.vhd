-- divides an input clock by a variable factor of n

library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

use work.util_pkg.all;

entity clock_divider is
	generic (
		DIVIDE_FACTOR_WIDTH : positive := 16;
		AUTO_RESET : boolean := true
	);
	port (
		clk_in : in std_logic;
		reset  : in std_logic := '0';

		divisor : in unsigned(DIVIDE_FACTOR_WIDTH-1 downto 0);
		clk_out : out std_logic;
		-- tick for one clock cycle on every overflow, useful for clock enables
		tick    : out std_logic
	);
end clock_divider;

architecture rtl of clock_divider is
	signal counter : unsigned(DIVIDE_FACTOR_WIDTH-1 downto 0) := (others => '0');

	signal passthrough : std_logic;

	signal divided_imm : std_logic;
	signal divided : std_logic;

	signal last_divisor : unsigned(DIVIDE_FACTOR_WIDTH-1 downto 0) := (others => '0');
	signal divisor_changed : std_logic;
begin
	output: process(passthrough, clk_in, divided)
	begin
		if passthrough then
			clk_out <= clk_in;
		else
			clk_out <= divided;
		end if;
	end process;

	count: process(clk_in)
	begin
		if rising_edge(clk_in) then
			passthrough <= divisor ?<= 1;
			divided     <= divided_imm;

			if counter ?= 0 and not reset then
				tick <= '1';
			else
				tick <= '0';
			end if;

			if reset or divisor_changed or counter ?= 0 then
				counter <= divisor - 1;
			else
				counter <= counter - 1;
			end if;
		end if;
	end process;

	divided_imm <= counter ?< divisor / 2;

	change_check: if AUTO_RESET generate
		process(clk_in)
		begin
			if rising_edge(clk_in) then
				divisor_changed <= last_divisor ?/= divisor;
				last_divisor <= divisor;
			end if;
		end process;
	else generate
		divisor_changed <= '0';
	end generate;
end rtl;
