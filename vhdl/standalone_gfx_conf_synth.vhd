configuration standalone_gfx_conf_synth of standalone_gfx is
	for behaviour
		for system_clocks_inst : system_clocks
			use entity work.system_clocks_xilinx
				generic map (
					CLK_IN_FREQ => 100_000_000
				);
		end for;
	end for;
end standalone_gfx_conf_synth;
