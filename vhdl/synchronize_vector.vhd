library ieee;
use ieee.std_logic_1164.all;

entity synchronize_vector is
	generic (
		LEVELS     : integer range 2 to integer'high := 2;
		DATA_WIDTH : positive
	);
	port (
		clk_master : in std_logic;
		clk_slave  : in std_logic;

		data_in    : in std_logic_vector(DATA_WIDTH-1 downto 0);
		data_out   : out std_logic_vector(DATA_WIDTH-1 downto 0);

		busy       : out std_logic;
		changed    : out std_logic
	);
end synchronize_vector;

architecture rtl of synchronize_vector is
	signal data_latched : std_logic_vector(DATA_WIDTH-1 downto 0);

	signal data_changed : std_logic;

	signal marker_master : std_logic := '0';
	signal marker_return : std_logic := '0';

	signal marker_slave_in  : std_logic := '0';
	signal marker_slave_out : std_logic := '0';
begin
	process(clk_master)
	begin
		if rising_edge(clk_master) then
			if not busy then
				data_latched <= data_in;

				if data_changed then
					marker_master <= not marker_master;
				end if;
			end if;
		end if;
	end process;

	process(clk_slave)
	begin
		if rising_edge(clk_slave) then
			if marker_slave_in xor marker_slave_out then
				data_out <= data_latched;
				changed  <= '1';
			else
				changed  <= '0';
			end if;

			marker_slave_out <= marker_slave_in;
		end if;
	end process;

	data_changed <= '1' when data_latched /= data_in else '0';
	busy <= marker_master xor marker_return;

	sync_to_slave: entity work.synchronize_bit
		generic map (
			LEVELS => LEVELS
		)
		port map (
			clk => clk_slave,
			data_in  => marker_master,
			data_out => marker_slave_in
		);

	sync_to_master: entity work.synchronize_bit
		generic map (
			LEVELS => LEVELS
		)
		port map (
			clk => clk_master,
			data_in  => marker_slave_out,
			data_out => marker_return
		);
end rtl;
