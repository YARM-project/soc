#ifndef _IO_H_INCLUDED
#define _IO_H_INCLUDED

#include <stdint.h>

#define FG_COLOR_MASK  0x0F
#define FG_COLOR_SHIFT 0
#define BG_COLOR_MASK  0xF0
#define BG_COLOR_SHIFT 4

#define FLAG_COLOR_BRIGHT 0x8

#define COLOR_BLACK          0
#define COLOR_BLUE           1
#define COLOR_GREEN          2
#define COLOR_CYAN           3
#define COLOR_RED            4
#define COLOR_MAGENTA        5
#define COLOR_BROWN          6
#define COLOR_GRAY           7
#define COLOR_DARKGRAY       8
#define COLOR_BRIGHTBLUE     9
#define COLOR_BRIGHTGREEN   10
#define COLOR_BRIGHTCYAN    11
#define COLOR_BRIGHTRED     12
#define COLOR_BRIGHTMAGENTA 13
#define COLOR_YELLOW        14
#define COLOR_WHITE         15

typedef struct {
	uint8_t attrs;
	uint8_t character;
} screen_char_t;

typedef struct {
	const unsigned int num_rows, num_cols;
	unsigned int row, col;
	uint8_t active_fg, active_bg;
	screen_char_t *text_buf;
} screen_t;

void screen_scroll(screen_t *s);
void set_cursor_pos(screen_t *s, unsigned int row, unsigned int column);
void vga_putchar(screen_t *s, unsigned char c);
void clear_screen(screen_t *s);

#endif
