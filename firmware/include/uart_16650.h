#include "arty_a7.h"

#define UART_READ_REG(regnum) ({                                               \
	*(volatile uint8_t *)(ADDRESS_UART_REGS + 4 * regnum);                     \
})

#define UART_WRITE_REG(regnum, data) ({                                        \
	*(volatile uint8_t *)(ADDRESS_UART_REGS + 4 * regnum) = data;              \
})

#define UART_REG(regnum) (*(volatile uint8_t *)(ADDRESS_UART_REGS + 4 * regnum))

#define UART_REG_RBR 0
#define UART_REG_THR 0
#define UART_REG_DLL 0

#define UART_REG_IER 1
#define UART_REG_DLM 1

#define UART_REG_IIR 2
#define UART_REG_FCR 2
#define UART_REG_LCR 3
#define UART_REG_MCR 4
#define UART_REG_LSR 5
#define UART_REG_MSR 6
#define UART_REG_SCR 7

#define UART_INT_RX_AVAIL    0x01
#define UART_INT_TX_EMPTY    0x02
#define UART_INT_LINE_STATE  0x04
#define UART_INT_MODEM_STATE 0x08

// UART interrupt identification
#define UART_II_NONE        0x01
#define UART_II_LINE_STATE  0x06
#define UART_II_RX_AVAIL    0x04
#define UART_II_TIMEOUT     0x0C
#define UART_II_TX_EMPTY    0x02
#define UART_II_MODEM_STATE 0x00
