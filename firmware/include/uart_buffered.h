#include <stdint.h>
#include <stdlib.h>
#include "common.h"

typedef struct {
	uint8_t *buffer;
	unsigned int len;
	unsigned int p_start;
	unsigned int p_end;
} uart_fifo_s;

uart_fifo_s *uart_fifo_alloc(unsigned int len_init);
unsigned int uart_fifo_elems(uart_fifo_s *fifo);
int uart_fifo_push(uart_fifo_s *fifo, uint8_t c);
int uart_fifo_pop(uart_fifo_s *fifo);
