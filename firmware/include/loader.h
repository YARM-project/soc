#ifndef _LOADER_H_INCLUDED
#define _LOADER_H_INCLUDED
#include <stdint.h>

extern const volatile uint32_t * const IS_SIMULATION;

void uart_write_char(char c);
uint8_t uart_read_char();

int loader_putchar(int c);
int loader_puts(const char *str);

int uart_read_line(char *buf, int *len, int maxsize);

#endif
