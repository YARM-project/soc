#ifndef _COMMON_H_INCLUDED
#define _COMMON_H_INCLUDED

#include "csr.h"

#define SR_MIE	_AC(0x00000008, UL) /* Machine Interrupt Enable */
#define SR_MPIE	_AC(0x00000080, UL) /* Previous Machine IE */
#define SR_MPP	_AC(0x00000C00, UL) /* Machine Previous Privilege */

/* Interrupt Enable and Interrupt Pending flags */
#define MIE_MSIE _AC(0x00000008, UL) /* Software Interrupt Enable */
#define MIE_MTIE _AC(0x00000080, UL) /* Timer Interrupt Enable */
#define MIE_MEIE _AC(0x00000800, UL) /* External Interrupt Enable */

#define GLOBAL_INT_ENABLE  csr_set(mstatus, SR_MIE)
#define GLOBAL_INT_DISABLE csr_clear(mstatus, SR_MIE)

#define GLOBAL_INT_MASK unsigned long __prev_mstatus = csr_read_clear(mstatus, SR_MIE);
#define GLOBAL_INT_UNMASK csr_set(mstatus, __prev_mstatus & SR_MIE)

#define BREAKPOINT ({__asm__("ebreak");})

#define SET_ISR(isr) csr_write(mtvec, &isr)

#define UNUSED(x) (void)(x)

#define wfi() {__asm__ ("wfi");}
#define invalid() {__asm__ (".word 0x00000000");}

#define IS_SIMULATION (*(const volatile uint32_t *)ADDRESS_IS_SIMULATION)

#endif
