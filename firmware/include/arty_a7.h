#include <stddef.h>
#include <stdint.h>

#define ADDRESS_SCREEN_BASE 0x00010000
#define SCREEN_ROWS 30
#define SCREEN_COLUMNS 80

#define ADDRESS_SWITCHES  0xf0000004
#define ADDRESS_BUTTONS   0xf0000008

#define ADDRESS_UART_REGS 0xf0000100
#define UART_BASE_CLK 18432000

#define ADDRESS_WS2812    0xf0001000
#define ADDRESS_RGB_LEDS  0xf0000010

#define ADDRESS_TIME    0x80000000
#define ADDRESS_TIMECMP 0x80000010

#define ADDRESS_IS_SIMULATION 0x80000020

#define LITEETH_FIFOS_BASE 0xf0100000
#define LITEETH_PHY_BASE   0xf0180800
#define LITEETH_MAC_BASE   0xf0181000

#define PAYLOAD_ENTRY 0x00001000

int get_button(int num);
int get_switch(int num);
void set_ws2812(size_t num, uint32_t color);
void set_rgb_led(size_t num, uint32_t color);
