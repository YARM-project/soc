#!/usr/bin/python
import argparse
import os
import struct
import sys

# takes a binary input file and prepends the string "BOOT\n" and the file length to it

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', metavar='outfile', dest='outfile', type=argparse.FileType('wb'), default=sys.stdout.buffer, help='the destination file')
    parser.add_argument('infile', help='the binary source file')

    args = parser.parse_args()

    filesize = os.path.getsize(args.infile)

    args.outfile.write('BOOT\n'.encode())
    # big-endian 16-bit unsigned int
    args.outfile.write(struct.pack('>H', filesize))

    with open(args.infile, 'rb') as infile:
        args.outfile.write(infile.read())
