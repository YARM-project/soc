# UART

`stty -F /dev/ttyUSB0 sane 9600 erase '^H' -imaxbel -opost -icrnl -onlcr -isig -icanon -iexten -echo -echoe -echok -echoke`

# Bugs
## [fixed] Stack clash/trap return fail

reproducible in `09ba096`

fixed in `f101986` - root cause was mret returning to wrong address when interrupt was triggered right after another returned

```
cat firmware/payload_uart_data.bin > /dev/ttyUSB0
echo -en "many\nnew\nlines\n" > /dev/ttyUSB0
```

some failure modes:

1. prints `payload returned` without ever actually returning (i.e. returns with too high stack frame)
2. resets, i.e. returns to `0x00000000`
3. prints ~second half of Arch logo - some call to `puts()` with corrupted data address (probably popped from stack)?
