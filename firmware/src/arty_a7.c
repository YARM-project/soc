#include "arty_a7.h"
#include <stdint.h>
#include <stddef.h>

// get state of a button
int get_button(int num) {
	if (num >= 4) {
		return 0;
	}

	return *((volatile int *)ADDRESS_BUTTONS) & (1 << num);
}

// get state of a switch
int get_switch(int num) {
	if (num >= 4) {
		return 0;
	}

	return *((volatile int *)ADDRESS_SWITCHES) & (1 << num);
}

void set_ws2812(size_t num, uint32_t color) {
	((volatile uint32_t *)ADDRESS_WS2812)[num] = color;
}

void set_rgb_led(size_t num, uint32_t color) {
	((volatile uint32_t*)ADDRESS_RGB_LEDS)[num] = color;
}
