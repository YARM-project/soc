#include "io.h"

// set a specified screen character, automatically changing colors
static void set_char(screen_t *s, screen_char_t *screen_c, unsigned char c) {
	screen_c->attrs = (
		(s->active_fg << FG_COLOR_SHIFT & FG_COLOR_MASK) |
		(s->active_bg << BG_COLOR_SHIFT & BG_COLOR_MASK)
	);
	screen_c->character = c;
}

static void set_curr_char(screen_t *s, unsigned char c) {
	set_char(s, &s->text_buf[s->row * s->num_cols + s->col], c);
}

// scrolls the screen by one line, filling the new bottom row with spaces
void screen_scroll(screen_t *s) {
	// move content up by one line
	for (int i=0; i < (int)((s->num_rows - 1) * s->num_cols); i++) {
		s->text_buf[i] = s->text_buf[i + s->num_cols];
	}

	// fill last line with spaces
	for (int i=0; i < (int)s->num_cols; i++) {
		s->text_buf[(s->num_rows - 1) * s->num_cols + i].character = ' ';
	}
}

void set_cursor_pos(screen_t *s, unsigned int row, unsigned int column) {
	s->row = row;
	s->col = column;

	if (s->col >= s->num_cols) {
		s->col = 0;
		s->row++;
	}

	if (s->row >= s->num_rows) {
		screen_scroll(s);
		s->row = s->num_rows - 1;
	}
}

void vga_putchar(screen_t *s, unsigned char c) {
	switch(c) {
		case '\n':
			set_cursor_pos(s, s->row + 1, 0);
			break;

		case '\b':
		// DEL
		case 0x7F:
			if (s->col > 0) {
				set_cursor_pos(s, s->row, s->col - 1);
			}
			if (c == 0x7F) {
				set_curr_char(s, ' ');
			}
			break;

		default:
			set_curr_char(s, c);
			set_cursor_pos(s, s->row, s->col + 1);
	}
}

void clear_screen(screen_t *s) {
	screen_char_t pattern;
	set_char(s, &pattern, ' ');

	for (unsigned int i = 0; i < s->num_rows * s->num_cols; i++) {
		s->text_buf[i] = pattern;
	}

	set_cursor_pos(s, 0, 0);
}
