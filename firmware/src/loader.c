#include "uart_16650.h"
#include "arty_a7.h"
#include "loader.h"
#include "io.h"
#include "common.h"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

// #define UART_BAUD 1000000
#define UART_BAUD 115200
#define UART_DIVISOR (UART_BASE_CLK / UART_BAUD / 16)

#define SCHLAUCHBOOT_LOGO "\
 ____       _     _                  _     _                 _   \n\
/ ___|  ___| |__ | | __ _ _   _  ___| |__ | |__   ___   ___ | |_ \n\
\\___ \\ / __| '_ \\| |/ _` | | | |/ __| '_ \\| '_ \\ / _ \\ / _ \\| __|\n\
 ___) | (__| | | | | (_| | |_| | (__| | | | |_) | (_) | (_) | |_ \n\
|____/ \\___|_| |_|_|\\__,_|\\__,_|\\___|_| |_|_.__/ \\___/ \\___/ \\__|\n\
"

screen_t vga_screen = {
	.num_rows=SCREEN_ROWS,
	.num_cols=SCREEN_COLUMNS,
	.row=0,
	.col=0,
	.active_fg=COLOR_WHITE,
	.active_bg=COLOR_BLUE,
	.text_buf=(screen_char_t *)ADDRESS_SCREEN_BASE
};

// writes a character to UART, blocking
void uart_write_char(char c) {
	// replace NL by CRNL
	if (c == '\n') {
		uart_write_char('\r');
	}
	// wait until FIFO is empty
	// LSR5 = Transmit Holding Register Empty
	while (!(UART_READ_REG(UART_REG_LSR) & (1 << 5)));

	UART_WRITE_REG(UART_REG_THR, c);
}

// reads a character from UART, blocking
uint8_t uart_read_char() {
	// wait until data is available
	// LSR0 = Data Ready
	while (!(UART_READ_REG(UART_REG_LSR) & 1));

	return UART_READ_REG(UART_REG_RBR);
}

int loader_putchar(int c) {
	unsigned char real_c = c;

	// write to screen
	vga_putchar(&vga_screen, real_c);

	uart_write_char(real_c);

	return (int)c;
}

int loader_puts(const char *str) {
	for (; *str; str++) {
		loader_putchar(*str);
	}
	loader_putchar('\n');

	return 1;
}

// attempts to read a line of text from UART
// appends any characters read to `buf` and null-terminates,
// sets `len` to the current length of the string (excluding null)
// if no more data available, return 0
// if line was successfully read, return 1
int uart_read_line(char *buf, int *len, int maxsize) {
	// ensure we always have a valid string
	if (*len == 0) {
		buf[0] = 0;
	}

	while (1) {
		// LSR0 = data ready
		if (UART_READ_REG(UART_REG_LSR) & 1) {
			char read_char = UART_READ_REG(UART_REG_RBR);

			switch (read_char) {
				case '\n':
				case '\r':
					loader_putchar('\n');
					// line is done, return
					return 1;

				// backspace or DEL
				case '\b':
				case 0x7F:
					if (*len > 0) {
						// echo
						loader_putchar(read_char);

						// set last buffer element to NULL, decrease length
						buf[*len - 1] = '\0';
						(*len)--;
					} else {
						// BEL when already empty
						loader_putchar('\a');
					}
					break;

				default:
					// if there's still room
					if (*len + 1 < maxsize) {
						// echo
						loader_putchar(read_char);

						// replace previous null byte with read character and append null
						buf[*len] = read_char;
						buf[*len + 1] = '\0';
						(*len)++;
					} else {
						// BEL on full buffer
						loader_putchar('\a');
					}
			}
		} else {
			// no more data available
			return 0;
		}
	}

	return 2;
}

// gets a payload from UART and saves it to the payload vector
void receive_payload() {
	uint16_t payload_size;

	payload_size = uart_read_char() << 8;
	payload_size += uart_read_char();

	for (int i = 0; i < payload_size; i++) {
		((uint8_t *)PAYLOAD_ENTRY)[i] = uart_read_char();
	}
}

__attribute__((interrupt("machine")))
void loader_trap() {
	uint16_t mepc = csr_read(mepc) & 0xFFFF;
	uint32_t cause = csr_read(mcause);

	loader_puts("Loader trapped!");
	loader_putchar(mepc >> 24);
	loader_putchar(mepc >> 16);
	loader_putchar(mepc >> 8);
	loader_putchar(mepc);

	while (!get_button(1));

	if (cause == EXC_BREAKPOINT) {
		csr_write(mepc, csr_read(mepc) + 4);
	}
}

int main() {
	// set divisor latch
	UART_WRITE_REG(UART_REG_LCR, 0x80);

	// set divisor
	UART_WRITE_REG(UART_REG_DLM, (UART_DIVISOR >> 8) & 0xFF);
	UART_WRITE_REG(UART_REG_DLL, UART_DIVISOR & 0xFF);

	// clear divisor latch, set number of bits
	UART_WRITE_REG(UART_REG_LCR, 0b00000011);

	// enable loopback
	//UART_WRITE_REG(UART_REG_MCR, 0x10);

	// enable FIFO mode
	UART_WRITE_REG(UART_REG_FCR, 0x01);

	// enable parity
	//UART_REG(UART_REG_LCR) |= 0b01000;

	SET_ISR(loader_trap);

	vga_screen.active_bg = COLOR_BLUE;
	vga_screen.active_fg = COLOR_WHITE;

	if (IS_SIMULATION) {
		// when in simulation, call payload immediately
		((void (*)())PAYLOAD_ENTRY)();
	}

	clear_screen(&vga_screen);

	loader_puts("Schlauchboot loading...");

	loader_puts(SCHLAUCHBOOT_LOGO);

	int btn_state = 0;
	char buffer[10];
	int buf_len = 0;

	loader_putchar('>');
	loader_putchar(' ');

	while (1) {
		int ret = uart_read_line(buffer, &buf_len, sizeof(buffer)/sizeof(buffer[0]));
		if (ret) {
			// reset buffer
			buf_len = 0;

			loader_putchar(':');
			loader_puts(buffer);

			if (strcmp(buffer, "BOOT") == 0) {
				receive_payload();

				loader_puts("booting");
				((void (*)())PAYLOAD_ENTRY)();

				GLOBAL_INT_DISABLE;
				SET_ISR(loader_trap);
				loader_puts("payload returned");
			} else if (strcmp(buffer, "ext") == 0) {
				for (int i = 0; i < 8; i++) {
					*(volatile uint32_t*)(0xf0002000 + 4*i) = 1 << i;
					//printf("%#02x: %#02x\n", i, *(volatile uint32_t*)(0xf0002000 + i));
				}
			}

			loader_putchar('>');
			loader_putchar(' ');
		}

		if (get_button(1)) {
			if (btn_state == 0) {
				btn_state = 1;

				int input_char = *(volatile int *)ADDRESS_SWITCHES;

				loader_putchar(input_char);
			}
		} else {
			btn_state = 0;
		}
	}
}
