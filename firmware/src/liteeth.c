#include "arty_a7.h"
#include "liteeth.h"
#include <stdio.h>

void eth_init(void)
{
	printf("Ethernet init...\n");

	*(uint8_t*)(LITEETH_PHY_BASE + LITEETH_PHY_CRG_RESET) = 1;
	for (int i = 0; i < 100000; i++) {asm ("nop");}
	*(uint8_t*)(LITEETH_PHY_BASE + LITEETH_PHY_CRG_RESET) = 0;
	for (int i = 0; i < 100000; i++) {asm ("nop");}
}
