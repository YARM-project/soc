#include "common.h"
#include <sys/stat.h>
#include <errno.h>

int _write(int file, const char *buf, int len) {
	return payload_write(file, buf, len);
}

void abort() {
	while (1);
}

void *_sbrk(int incr) {
	// Defined by the linker
	extern char _start_heap;
	static void *heap_end;
	void *prev_heap_end;

	if (heap_end == 0) {
		heap_end = &_start_heap;
	}
	prev_heap_end = heap_end;

	void *stack_ptr;
	asm ("mv %0, sp" : "=r"(stack_ptr));
	if (heap_end + incr > stack_ptr) {
		errno = ENOMEM;
		return (void *)-1;
	}

	heap_end += incr;
	return prev_heap_end;
}

int _fstat(int file, struct stat *st) {
	UNUSED(file);
	UNUSED(*st);
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(int file) {
	UNUSED(file);
	return 1;
}

int _close(int file) {
	UNUSED(file);
	return -1;
}

int _lseek(int file, int ptr, int dir) {
	UNUSED(file);
	UNUSED(ptr);
	UNUSED(dir);
	return 0;
}

int _read(int file, char *ptr, int len) {
	UNUSED(file);
	UNUSED(*ptr);
	UNUSED(len);
	return 0;
}
