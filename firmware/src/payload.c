#include "arty_a7.h"
#include "loader.h"
#include "uart_16650.h"
#include "io.h"
#include "common.h"
#include "uart_buffered.h"
#include "liteeth.h"
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define UART_FIFO_LEN_INIT 64

extern screen_t vga_screen;

uart_fifo_s *uart_buf_rx;
uart_fifo_s *uart_buf_tx;

int payload_write(int file, const char *buf, int len) {
	UNUSED(file);

	uint8_t c;
	for (int i = 0; i < len; i++) {
		c = buf[i];
		if (c == '\n') {
			while (uart_fifo_push(uart_buf_tx, '\r')) {
				wfi();
			}
		}

		while (uart_fifo_push(uart_buf_tx, c)) {
			wfi();
		}

		vga_putchar(&vga_screen, c);
	}

	return len;
}

__attribute__((interrupt("machine")))
void trap_handler() {
	uint32_t cause = csr_read(mcause);
	uint8_t uart_int_ident = UART_READ_REG(UART_REG_IIR);
	uint32_t ret_pc = csr_read(mepc);
	UART_WRITE_REG(UART_REG_MCR, 0x00);

	if (cause & 0x80000000) {
		// interrupt
		switch (uart_int_ident & 0x0F) {
			case UART_II_LINE_STATE:
				putchar('L');
				UART_READ_REG(UART_REG_LSR);
				break;

			case UART_II_MODEM_STATE:
				putchar('M');
				UART_READ_REG(UART_REG_MSR);
				break;

			case UART_II_TX_EMPTY:
				if (uart_buf_tx->p_end != uart_buf_tx->p_start) {
					UART_WRITE_REG(UART_REG_THR, uart_fifo_pop(uart_buf_tx));
				} else {
					UART_REG(UART_REG_IER) &= ~UART_INT_TX_EMPTY;
				}
				break;

			case UART_II_RX_AVAIL:
			case UART_II_TIMEOUT:
				uart_fifo_push(uart_buf_rx, UART_READ_REG(UART_REG_RBR));
				break;

			default:
				putchar(uart_int_ident);
		}
	} else {
		// synchronous exception
		puts("exc");
		while (!get_button(1));
		if (cause == EXC_BREAKPOINT) {
			csr_write(mepc, ret_pc + 4);
		}
	}
}

uint32_t rgb(int red, int green, int blue) {
	return (red & 0xFF) << 16 |
	       (green & 0xFF) << 8 |
	       (blue & 0xFF);
}

void rainbow(int offset) {
	int steps = 5;
	int levels[] = {0x01, 0x04, 0x08, 0x0a, 0x10};

	for (int i = 0; i < 60; i++) {
		int inRange = (i + offset) % (3 * steps);
		int step    = (i + offset) % steps;

		int incr = levels[step];
		int decr = levels[steps - step];

		if (inRange < steps) {
			set_ws2812(i, rgb(decr, incr,    0));
		} else if (inRange < 2*steps) {
			set_ws2812(i, rgb(   0, decr, incr));
		} else {
			set_ws2812(i, rgb(incr,    0, decr));
		}
	}
}

void zero_bss() {
	extern char _start_bss, _end_bss;
	memset(&_start_bss, 0, &_end_bss - &_start_bss);
}

void hexdump(volatile uint8_t *data, size_t len, bool print_ascii) {
	for (size_t i = 0; i < len; i += 16) {
		printf("%08x:", i);

		for (size_t j = 0; j < 16; j++) {
			if (j % 2 == 0) {
				printf(" ");
			}

			if (i + j >= len) {
				printf("  ");
			} else {
				printf("%02x", data[i+j]);
			}
		}

		if (print_ascii) {
			printf(" |");
			for (size_t k = 0; k < 16; k++) {
				if (i + k >= len) {
					printf(" ");
				} else if (isprint(data[i+k])) {
					printf("%c", data[i+k]);
				} else {
					printf(".");
				}
			}
		}

		printf("\n");
	}
}

__attribute__((section(".text.payload_entry")))
void payload_entry() {
	zero_bss();

	vga_screen.active_bg = COLOR_BLACK;
	vga_screen.active_fg = COLOR_WHITE;
	SET_ISR(trap_handler);

	setbuf(stdout, NULL);

	// enable UART interrupts
	UART_WRITE_REG(UART_REG_IER, UART_INT_RX_AVAIL | UART_INT_TX_EMPTY | UART_INT_LINE_STATE | UART_INT_MODEM_STATE);

	uart_buf_tx = uart_fifo_alloc(UART_FIFO_LEN_INIT);
	if (NULL == uart_buf_tx) {
		loader_puts("tx buf alloc failed");
		abort();
	}

	uart_buf_rx = uart_fifo_alloc(UART_FIFO_LEN_INIT);
	if (NULL == uart_buf_rx) {
		loader_puts("rx buf alloc failed");
		abort();
	}

	// enable FIFO, set trigger level = 8
	UART_WRITE_REG(UART_REG_FCR, 0b10000001);
	// set trigger level = 1
	//UART_WRITE_REG(UART_REG_FCR, 0b00000001);

	// enable software and external interrupts
	csr_write(mie, MIE_MSIE | MIE_MEIE);
	GLOBAL_INT_ENABLE;

	if (!IS_SIMULATION) {
		clear_screen(&vga_screen);
	}
	puts("inside payload");

	printf("printf works\n");

	//rainbow(0);

	char buffer[10];
	int buf_len = 0;

	while (1) {
		if (uart_read_line(buffer, &buf_len, sizeof(buffer)/sizeof(buffer[0]))) {
			buf_len = 0;
			if (strcmp(buffer, "exit") == 0) {
				puts("exiting payload...\n");

				free(uart_buf_rx);
				free(uart_buf_tx);
				GLOBAL_INT_DISABLE;
				return;
			} else if (strcmp(buffer, "vga") == 0) {
				clear_screen(&vga_screen);
				puts("");
				puts("test");
				//*(uint32_t*)0x10000 = 0x0F580659;
				//*(uint32_t*)0x10000 = 0x580F59F0;
				*(uint16_t*)0x10000 = 0x59F0;
				*(uint16_t*)0x10002 = 0x580F;
				*(uint32_t*)0x10004 = 0x5A065B60;
			} else if (strcmp(buffer, "readq") == 0) {
				puts("reading 32b from 0x20000000");
				uint32_t res = *(uint32_t*)(0x20000000);
				printf("%08lx\n", res);
			} else if (strcmp(buffer, "readd") == 0) {
				puts("reading 16b from 0x20000000");
				uint16_t res = *(uint16_t*)(0x20000000);
				printf("%04x\n", res);
			} else if (strcmp(buffer, "read") == 0) {
				puts("reading 8b from 0x20000000");
				uint8_t res = *(uint8_t*)(0x20000000);
				printf("%02x\n", res);
			} else if (strcmp(buffer, "write") == 0) {
				puts("writing 0x12345678 to 0x20000000");
				*(uint32_t*)(0x20000000) = 0x12345678;
			} else if (strcmp(buffer, "test") == 0) {
				puts("performing memtest:");
				puts("writing 32bit addresses...");
				for (volatile uint32_t *p = (uint32_t*)0x20000000; p < (uint32_t*)0x30000000; p++) {
					*p = (uint32_t) p;
				}

				puts("checking addresses...");
				for (uint32_t i = 0x20000000; i < 0x30000000; i += 4) {
					volatile uint32_t stored = *((uint32_t*)i);
					if (stored != i) {
						printf("%08lx: should be %08lx, is %08lx", i, i, stored);
					}
				}
				puts("done!");

				puts("checking byte-level accesses...");
				volatile uint8_t *byte_base = (uint8_t*)0x25000000;

				byte_base[0] = 0xEF;
				byte_base[1] = 0xCD;
				byte_base[2] = 0xAB;
				byte_base[3] = 0x89;

				volatile uint32_t stored = *((uint32_t*)byte_base);

				if (stored != 0x89ABCDEF) {
					printf("expected %08x, got %08lx", 0x89ABCDEF, stored);
				} else {
					puts("bytes okay");
				}

				uint32_t test_val = 0x56789abc;
				for (int i = 0; i < 4; i++) {
					uint8_t res = *((volatile uint8_t*)(&test_val) + i);
					printf("%02x: %02x\n", i, res);
				}

				puts("done!");
			} else if (strcmp(buffer, "count") == 0) {
				int sum = 0;
				for (int i = 0; i <= 100; i++) {
					sum += i;
					printf("%d\n", sum);
				}
			} else if (strcmp(buffer, "ethreset") == 0) {
				eth_init();
			} else if (strcmp(buffer, "ethdump") == 0) {
				printf("pending: %04lx\n",
				       *(volatile uint32_t*)(LITEETH_MAC_BASE + LITEETH_WRITER_EV_PENDING)
				);
				printf("slot: %04lx\n",
				       *(volatile uint32_t*)(LITEETH_MAC_BASE + LITEETH_WRITER_SLOT)
				);
				printf("length: %04lx\n",
				       *(volatile uint32_t*)(LITEETH_MAC_BASE + LITEETH_WRITER_LENGTH)
				);
				hexdump((volatile void*)(LITEETH_FIFOS_BASE), 1000, false);
				hexdump((volatile void*)(LITEETH_FIFOS_BASE+LITEETH_SLOT_SIZE), 1000, false);
				*(volatile uint32_t*)(LITEETH_MAC_BASE + LITEETH_WRITER_EV_PENDING) = 0xFFFF;
			} else if (strcmp(buffer, "external") == 0) {
#define UART_EXTERNAL ((volatile uint32_t*)0xf0002000)
				UART_EXTERNAL[UART_REG_LCR] = 0x80;
				UART_EXTERNAL[UART_REG_DLM] = 0;
				UART_EXTERNAL[UART_REG_DLL] = 3;
				UART_EXTERNAL[UART_REG_LCR] = 0x03;
				printf("LSR: %#02lx\n", UART_EXTERNAL[UART_REG_LSR]);

				for (char c = 'A'; c <= 'Z'; c++) {
					while (!(UART_EXTERNAL[UART_REG_LSR] & (1 << 5)));
					UART_EXTERNAL[UART_REG_THR] = c;
				}
			} else if (strlen(buffer) > 0) {
				printf("unknown command: %s\n", buffer);
			}
		}
	}
}
