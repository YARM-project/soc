#include "uart_buffered.h"
#include "uart_16650.h"

uart_fifo_s *uart_fifo_alloc(unsigned int len) {
	uart_fifo_s *fifo = malloc(sizeof(*fifo));
	if (NULL == fifo) {
		return NULL;
	}

	fifo->buffer = calloc(len, sizeof(*fifo->buffer));
	if (NULL == fifo->buffer) {
		return NULL;
	}

	fifo->len = len;
	fifo->p_start = 0;
	fifo->p_end = 0;

	return fifo;
}

unsigned int uart_fifo_elems(uart_fifo_s *fifo) {
	GLOBAL_INT_MASK;
	int res;

	if (fifo->p_start <= fifo->p_end) {
		res = fifo->p_end - fifo->p_start;
	} else {
		res = fifo->len - (fifo->p_start - fifo->p_end);
	}

	GLOBAL_INT_UNMASK;
	return res;
}

int uart_fifo_push(uart_fifo_s *fifo, uint8_t c) {
	GLOBAL_INT_MASK;
	if ((fifo->p_end + 1) % fifo->len == fifo->p_start) {
		// FIFO full
		GLOBAL_INT_UNMASK;
		return -1;
	}

	fifo->buffer[fifo->p_end] = c;
	fifo->p_end = (fifo->p_end + 1) % fifo->len;

	UART_REG(UART_REG_IER) |= UART_INT_TX_EMPTY;

	GLOBAL_INT_UNMASK;
	return 0;
}

int uart_fifo_pop(uart_fifo_s *fifo) {
	GLOBAL_INT_MASK;
	if (fifo->p_start == fifo->p_end) {
		// FIFO empty
		GLOBAL_INT_UNMASK;
		return -1;
	}

	uint8_t result = fifo->buffer[fifo->p_start];
	fifo->p_start = (fifo->p_start + 1) % fifo->len;

	return result;
}
