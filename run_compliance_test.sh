#!/bin/sh

set -eu

ELFFILE="$1"
SIGNATURE_FILE="$2"
LOG_OUTPUT="$3"

READELF="$4"

symbol_address() {
	"$READELF" --syms ${ELFFILE} | awk -v sym="$1" '$8 == sym {print $2}'
}

SIGNATURE_START=$(symbol_address 'begin_signature')
SIGNATURE_END=$(symbol_address 'end_signature')
TOHOST_ADDR=$(symbol_address 'tohost')

cd "$(dirname "$0")"

ghdl -m $(make --silent --no-print-directory echo-ghdl-flags) compliance_tb
ghdl -r $(make --silent --no-print-directory echo-ghdl-flags) compliance_tb \
	$(make --silent --no-print-directory echo-sim-run-flags) \
	-gIMAGE_SIZE="$(stat --printf='%s' "$ELFFILE.bin")" \
	-gTEST_BINARY_FILE="$ELFFILE.hex" \
	-gSIGNATURE_START_STR="${SIGNATURE_START}" \
	-gSIGNATURE_END_STR="${SIGNATURE_END}" \
	-gSIGNATURE_FILE_PATH="${SIGNATURE_FILE}" \
	-gTOHOST_ADDR_STR="${TOHOST_ADDR}" 2>&1 | tee "${LOG_OUTPUT}"
