.SECONDARY:

FIRMWARE_DIR = firmware

INCL_DIR = $(FIRMWARE_DIR)/include
SRCDIR   = $(FIRMWARE_DIR)/src
OBJDIR   = $(FIRMWARE_DIR)/obj
BINDIR   = $(FIRMWARE_DIR)/bin

WORKDIR            = work
GHDL_WORKDIR       = $(WORKDIR)/ghdl
SYMBIYOSYS_WORKDIR = $(WORKDIR)/symbiyosys
SYNTH_WORKDIR      = $(WORKDIR)/synth
LITEX_WORKDIR      = $(WORKDIR)/litex
WORKLIB_NAME = yarm_soc
SIM_DIR   = sim
VHDL_DIR  = vhdl

COMPLIANCE_TESTS = external/riscv-compliance

VHDL_FILES = $(VHDL_DIR)/*.vhd \
             $(VHDL_DIR)/components/*.vhd \
             $(VHDL_DIR)/components/gfx/*.vhd $(VHDL_DIR)/components/usart/*.vhd $(VHDL_DIR)/components/ws2812/*.vhd \
             $(VHDL_DIR)/simulation/*.vhd $(VHDL_DIR)/tests/*.vhd \
             $(VHDL_DIR)/core/*.vhd $(VHDL_DIR)/core/tests/*.vhd \
             $(VHDL_DIR)/core/alu/*.vhd \
             $(VHDL_DIR)/memories/*.vhd

VERILOG_FILES = $(LITEX_WORKDIR)/liteeth/gateware/liteeth_core.v

SIM_ENTITY        = soc_tb_conf
SYNTH_ENTITY      = soc_conf_synth
YOSYS_MODULE_NAME = soc

_LOADER_OBJ  = init.o loader.o io.o arty_a7.o
_PAYLOAD_OBJ = payload.o libc_stub.o uart_buffered.o liteeth.o

LOADER_LINK_SCRIPT = $(FIRMWARE_DIR)/loader.ld
PAYLOAD_LINK_SCRIPT = $(FIRMWARE_DIR)/payload.ld

BLOCKRAM_DATA_FREE   = $(BINDIR)/loader.hex
PAYLOAD_UART_DATA    = $(FIRMWARE_DIR)/payload_uart_data.bin

export RISCV_PREFIX ?= riscv64-unknown-elf-
CC           = $(RISCV_PREFIX)gcc
OBJCOPY      = $(RISCV_PREFIX)objcopy
CFLAGS       = -march=rv32imc -mabi=ilp32 -fno-pic -static -nostartfiles -Wall -Wextra -Os -specs=nano.specs -Wl,--build-id=none -g -I$(INCL_DIR)

GHDL = ghdl
GHDL_FLAGS    = --std=08 --work=$(WORKLIB_NAME) --workdir=$(GHDL_WORKDIR) -g
SIM_RUN_FLAGS = --max-stack-alloc=0 --assert-level=warning --ieee-asserts=disable-at-0

GTKWAVE = gtkwave

# synthesis

XDC = arty_a7_35.xdc

PART = xc7a35tcsg324-1

YOSYS = yosys

XRAY_DATABASE = /usr/share/xray/database/artix7

NEXTPNR = nextpnr-xilinx
BBASM   = bbasm
FASM2FRAMES = fasm2frames
FRAMES2BIT  = xc7frames2bit
OPENFPGALOADER = openFPGALoader

GHDL_YOSYS_PLUGIN = ghdl

SBY = sby -fd '$(SYMBIYOSYS_WORKDIR)' --yosys='$(YOSYS) -m $(GHDL_YOSYS_PLUGIN)'

# ====================
# END OF CONFIGURATION
# ====================

LOADER_OBJ  = $(patsubst %,$(OBJDIR)/%,$(_LOADER_OBJ))
PAYLOAD_OBJ = $(patsubst %,$(OBJDIR)/%,$(_PAYLOAD_OBJ))

SIM_WAVE = $(SIM_DIR)/$(SIM_ENTITY).ghw
# for only capturing select signals
SIM_WAVEOPTS = $(SIM_DIR)/$(SIM_ENTITY).waveopt
SIM_WAVE_SAVE = $(SIM_DIR)/$(SIM_ENTITY).gtkw

ifneq (,$(wildcard $(SIM_WAVEOPTS)))
SIM_RUN_FLAGS += --read-wave-opt=$(SIM_WAVEOPTS)
endif

# =======
# PHONIES
# =======

.PHONY: all payload firmware ghdl_import simonly wave

# default target
all: bitstream

payload: $(PAYLOAD_UART_DATA)
firmware: payload $(BLOCKRAM_DATA_FREE)

ghdl_import: | $(GHDL_WORKDIR)
	$(GHDL) import $(GHDL_FLAGS) $(VHDL_FILES)

simonly: $(VHDL_FILES) $(BINDIR)/simulation.hex
	$(GHDL) make $(GHDL_FLAGS) $(SIM_ENTITY)
	$(GHDL) run $(GHDL_FLAGS) $(SIM_ENTITY) $(SIM_RUN_FLAGS)

wave: $(VHDL_FILES) $(BINDIR)/simulation.hex
	$(GHDL) make $(GHDL_FLAGS) $(SIM_ENTITY)
	$(GHDL) run $(GHDL_FLAGS) $(SIM_ENTITY) $(SIM_RUN_FLAGS) --wave=$(SIM_WAVE)
	$(GTKWAVE) $(SIM_WAVE) $(SIM_WAVE_SAVE)

.PHONY: check check-compliance check-formal
check: check-compliance check-formal
check-compliance:
	$(MAKE) -C $(COMPLIANCE_TESTS) TARGETDIR="$(PWD)/tests/riscv-compliance-target" RISCV_TARGET=YARM YARM_DIRECTORY="$(PWD)" clean
	$(MAKE) -C $(COMPLIANCE_TESTS) TARGETDIR="$(PWD)/tests/riscv-compliance-target" RISCV_TARGET=YARM YARM_DIRECTORY="$(PWD)" all_variant

check-formal:
	$(SBY) tests/formal/alu.sby
	$(SBY) tests/formal/mult.sby
	$(SBY) tests/formal/div.sby

.PHONY: synth show bitstream flash
synth: $(SYNTH_WORKDIR)/$(SYNTH_ENTITY).json

show: $(SYNTH_WORKDIR)/$(SYNTH_ENTITY).json
	$(YOSYS) -p show $<

bitstream: $(SYNTH_WORKDIR)/$(SYNTH_ENTITY).bit

flash: $(SYNTH_WORKDIR)/$(SYNTH_ENTITY).bit
	$(OPENFPGALOADER) -b arty $<

.PHONY: clean
clean:
	rm -rf $(WORKDIR)
	rm -rf $(LITEX_WORKDIR)
	rm -rf $(OBJDIR) $(BINDIR)
	rm -f $(BLOCKRAM_DATA) $(PAYLOAD_UART_DATA)
	rm -f $(FIRMWARE_DIR)/simulation.hex
	$(MAKE) -C $(COMPLIANCE_TESTS) clean

$(OBJDIR) $(BINDIR) $(GHDL_WORKDIR) $(SYNTH_WORKDIR):
	mkdir -p $@

# =====
# LITEX
# =====

$(LITEX_WORKDIR)/liteeth/software/include: $(LITEX_WORKDIR)/liteeth.check
$(LITEX_WORKDIR)/liteeth/gateware/liteeth_core.v: $(LITEX_WORKDIR)/liteeth.check

$(LITEX_WORKDIR)/%.check: %.yaml
	$*_gen --output-dir=$(LITEX_WORKDIR)/$* $*.yaml
	touch $@

# ========
# FIRMWARE
# ========

$(OBJDIR)/%.o: $(SRCDIR)/%.[Sc] | $(OBJDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BINDIR)/loader.elf: $(LOADER_OBJ) $(LOADER_LINK_SCRIPT) | $(BINDIR)
	$(CC) $(CFLAGS) -T$(LOADER_LINK_SCRIPT) -o $@ $(LOADER_OBJ)

$(BINDIR)/payload.elf: $(PAYLOAD_OBJ) $(PAYLOAD_LINK_SCRIPT) $(BINDIR)/loader.elf | $(BINDIR)
	$(CC) $(CFLAGS) -T$(PAYLOAD_LINK_SCRIPT) -Wl,--just-symbols=$(BINDIR)/loader.elf -o $@ $(PAYLOAD_OBJ)

$(BINDIR)/%.bin: $(BINDIR)/%.elf
	$(OBJCOPY) -O binary $< $@

$(BINDIR)/simulation.bin: $(BINDIR)/loader.bin $(BINDIR)/payload.bin | $(BINDIR)
	dd conv=notrunc bs=1 if=$(BINDIR)/loader.bin of=$@ seek=0
	dd conv=notrunc bs=1 if=$(BINDIR)/payload.bin of=$@ seek=$(shell printf "%d" 0x1000)

$(BINDIR)/%.hex: $(BINDIR)/%.bin
	xxd -p -c4 $< > $@

$(PAYLOAD_UART_DATA): $(BINDIR)/payload.bin
	$(FIRMWARE_DIR)/build_uart_data.py -o $@ $<

# ======
#  VHDL
# ======

# chipdb

$(SYNTH_WORKDIR)/%.bba:
	pypy3 $(NEXTPNR_DIR)/xilinx/python/bbaexport.py --device $* --bba $@

$(SYNTH_WORKDIR)/%.bin: $(SYNTH_WORKDIR)/%.bba
	$(BBASM) --le $< $@

# synthesis

$(SYNTH_WORKDIR)/$(SYNTH_ENTITY).json: $(VHDL_FILES) $(VERILOG_FILES) $(BLOCKRAM_DATA_FREE) | $(SYNTH_WORKDIR) $(WORKDIR)
	$(GHDL) make $(GHDL_FLAGS) $(SYNTH_ENTITY)
	$(YOSYS) -m $(GHDL_YOSYS_PLUGIN) -p 'ghdl $(GHDL_FLAGS) $(SYNTH_ENTITY); read_verilog $(VERILOG_FILES); chformal -remove; synth_xilinx -nodsp -nosrl -flatten -top $(YOSYS_MODULE_NAME); write_json $@'

$(SYNTH_WORKDIR)/%.fasm: $(SYNTH_WORKDIR)/%.json $(SYNTH_WORKDIR)/$(PART).bin $(XDC)
	$(NEXTPNR) --timing-allow-fail --xdc $(XDC) --json $< --chipdb $(SYNTH_WORKDIR)/$(PART).bin --fasm $@

$(SYNTH_WORKDIR)/%.frames: $(SYNTH_WORKDIR)/%.fasm
	$(FASM2FRAMES) --db-root $(XRAY_DATABASE) --part $(PART) $< $@

$(SYNTH_WORKDIR)/%.bit: $(SYNTH_WORKDIR)/%.frames $(XRAY_DATABASE)/$(PART)/part.yaml
	$(FRAMES2BIT) --part-file $(XRAY_DATABASE)/$(PART)/part.yaml --part-name $(PART) --frm-file $< --output-file $@

.PHONY: echo-ghdl-flags echo-sim-run-flags
echo-ghdl-flags:
	@echo $(GHDL_FLAGS)

echo-sim-run-flags:
	@echo $(SIM_RUN_FLAGS)
