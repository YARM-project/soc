// RISC-V Compliance Test Header File
// Copyright (c) 2017, Codasip Ltd. All Rights Reserved.
// See LICENSE for license details.
//
// Description: Common header file for RV32I tests

#ifndef _COMPLIANCE_MODEL_H
#define _COMPLIANCE_MODEL_H

//-----------------------------------------------------------------------
// RV Compliance Macros
//-----------------------------------------------------------------------

#define RVMODEL_HALT                                                    \
  addi x1, x1, 4;                                                       \
  li x1, 1;                                                             \
  write_tohost:                                                         \
    sw x1, tohost, t5;                                                  \
  self_loop:  j self_loop;

#define RVMODEL_DATA_BEGIN                                              \
  .align 4; .global begin_signature; begin_signature:

#define RVMODEL_DATA_END                                                \
  .align 4; .global end_signature; end_signature:                       \
  .pushsection .tohost,"aw",@progbits;                                  \
  .align 8; .global tohost; tohost: .dword 0;                           \
  .align 8; .global fromhost; fromhost: .dword 0;                       \
  .popsection;                                                          \
  .align 8; .global begin_regstate; begin_regstate:                     \
  .word 128;                                                            \
  .align 8; .global end_regstate; end_regstate:                         \
  .word 4;


#define RVMODEL_BOOT
#define RVMODEL_IO_INIT
#define RVMODEL_IO_ASSERT_GPR_EQ(_SP, _R, _I)
#define RVMODEL_IO_WRITE_STR(_SP, _STR)
#define RVMODEL_SET_MSW_INT
#define RVMODEL_CLEAR_MSW_INT
#define RVMODEL_CLEAR_MTIMER_INT
#define RVMODEL_CLEAR_MEXT_INT

#endif // _COMPLIANCE_IO_H
